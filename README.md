# RevenueX Android SDK

[![CircleCI](https://circleci.com/bb/mobilexturkey/revenuex-android-sdk.svg?style=shield&circle-token=d5ae5cd7b0da4f9bcb0a523541cacfb78b05a3ee)](https://app.circleci.com/pipelines/bitbucket/mobilexturkey/revenuex-android-sdk)

Instructions for implementing RevenueX SDK in Android applications.

## Table of Contents
<pre>
1. [Installation](#markdown-header-installation)
2. [Configuring the SDK](#markdown-header-configuring-the-sdk)
3. [Using the SDK](#markdown-header-using-the-sdk)
  3.1. [Requesting products](#markdown-header-requesting-products)
  3.2. [Requesting offerings](#markdown-header-requesting-offerings)
  3.3. [Purchase product](#markdown-header-purchase-product)
  3.4. [Entitlements](#markdown-header-entitlements)
  3.5. [User attributes](#markdown-header-user-attributes)
  3.6. [Remote configs](#markdown-header-remote-configs)
</pre>

## Installation

RevenueX Android SDK can be included in projects via Gradle.

```gradle
dependencies {
    implementation 'tr.com.mobilex:revenuex-android-sdk:0.0.4'
}
```

After synching Gradle, you should be able to import RevenueX.

    import tr.com.mobilex.Revenuex
    import tr.com.mobilex.core.billing.RevXOffering
    import tr.com.mobilex.core.billing.RevXProduct

## Configuring the SDK

After installation, you should configure RevenueX SDK at the application startup.
For configuration, you will need Client Id. This can be optained from RevenueX portal.

    class Application {
        
        override fun onCreate() {
            Revenuex.init(context = applicationContext, clientId = [CLIENT_ID], observerMode = false)
        }
    }

If you want RevenueX to manage billing operations, you should pass 'observerMode' as false.
Or should not pass 'observerMode' parameter, which is false in default.
If you already manage Google Billing operations, you can use RevenueX in observer mode, by passing the 'observerMode' parameter as true.
In observer mode, RevenueX only listens for purchases and syncs them with RevenueX portal.

## Using the SDK

### Requesting products

To request products, first you need to define products in RevenueX portal.

    Revenuex.getProducts(type = ProductType.Subs) { products: List<RevXProduct> ->
        // Use product information
    }

### Requesting offerings

Instead of requesting products, you can request Offerings to show them on your paywall.

    Revenuex.getCurrentOffering { currentOffering: RevXOffering? ->
        // Use current offerings information
    }

### Purchase product

You can start a purchase flow with RevenueX SDK and let the SDK manage operations related to Google Billing.

    Revenuex.purchaseProduct(activity = this, product = revXProduct) { purchase: RevXPurchase ->
        // 'purchase' holds information about the new purchase (for example; purchaseToken, orderId, etc.)
    }

You can provide old subscription to upgrade or downgrade user's subscription.

    Revenuex.purchaseProduct(activity = this, product = revXProduct, upgradedProduct = oldSubscription) { . . . }

### Entitlements

Entitlements can be used to check if user has an access to premium content in the application.

    val isEntitled: Boolean = Revenuex.isEntitled(identifier = "Entitlement identifier")

### User attributes

User attributes can be used to store information about the application user.
This values synched with the RevenueX portal.

    Revenuex.setAttributes(attributes = mapOf(UserAttributes.Email to "revenuex@mobilex.com.tr"))

### Remote configs

Remote configs can be used to define configuration parameters to customize application.
You can change config values remotely, without the need of a new release.

Remote config definitions fetched automatically at the startup.
If you want to define local default values, first you need to create an xml file under the 'res/xml' folder.
Example content;

```xml
<?xml version="1.0" encoding="utf-8"?>
<defaultsMap>
    <entry>
        <key>app_name</key>
        <value>RevenueX</value>
    </entry>
    <entry>
        <key>package_name</key>
        <value>tr.com.mobilex</value>
    </entry>
    <entry>
        <key>version</key>
        <value>3</value>
    </entry>
    <entry>
        <key>isPublished</key>
        <value>false</value>
    </entry>
    <entry>
        <key>email</key>
        <value></value>
    </entry>
</defaultsMap>
```

Then you need to set defaults at the application startup. (File name in the example is 'revenuex_remote_config_defaults.xml')

    Revenuex.setConfigDefaultsAsync(R.xml.revenuex_remote_config_defaults)

In the application, you can access remote config values like below.

    val appName = Revenuex.getStringConfig(
        key = "app_name",
        defaultValue = "This nullable parameter is used in case of missing config or type mismatch!"
    )
    val version = Revenuex.getDoubleConfig(key = "version")
