package tr.com.mobilex.applicationuser.network

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockkObject
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import java.io.Serializable
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplApplicationUserApiTests {

    private lateinit var applicationUserApi: ApplicationUserApi
    private lateinit var lock: CountDownLatch

    @MockK
    lateinit var systemInfo: SystemInfo

    @MockK
    lateinit var httpClient: HttpClient

    @BeforeEach
    fun setUp() {
        mockLogger()
        mockSystemInfo()
        lock = CountDownLatch(1)
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockSystemInfo() {
        every { systemInfo.revenueXUserId } returns "UserId"
        every { systemInfo.rootUrl } returns "https://www.revenuex.com"
        every { systemInfo.platform } returns "android"
        every { systemInfo.platformVersion } returns "10"
        every { systemInfo.deviceName } returns "TestDevice"
        every { systemInfo.applicationVersion } returns "1.2.3"
        every { systemInfo.isSandbox } returns true
        every { systemInfo.region } returns "TR"
        every { systemInfo.language } returns "tr-TR"
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T : Serializable> mockHttpClientPost(
        clientId: String,
        responseFromClient: T?,
        exceptionMessageFromClient: String?
    ) {
        every {
            httpClient.post<T>(
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any()
            )
        } answers {
            val headers = it.invocation.args[3] as Map<String, String>
            assertEquals(clientId, headers["clientid"], "ClientId in HTTP headers does not match")

            if (exceptionMessageFromClient != null) {
                val onFailure = it.invocation.args[4] as (Exception) -> Unit
                onFailure.invoke(Exception(exceptionMessageFromClient))
            } else {
                val onResponse = it.invocation.args[5] as (T?) -> Unit
                onResponse.invoke(responseFromClient)
            }
        }
    }

    @ParameterizedTest
    @MethodSource("testDataUserDto")
    fun testRegisterApplicationUser(
        clientId: String,
        userDtoFromClient: CurrentRevenueXUserDto?,
        exceptionMessageFromClient: String?,
        isExceptionExpected: Boolean,
        expectedMessage: String,
        expectedUserDto: CurrentRevenueXUserDto?
    ) {
        var isFailed = isExceptionExpected.not()
        var exceptionMessage = "DefaultMessage"
        var userDto: CurrentRevenueXUserDto? = null

        // Mocks test state with parameters
        every { systemInfo.clientId } returns clientId
        mockHttpClientPost(clientId, userDtoFromClient, exceptionMessageFromClient)
        applicationUserApi = ImplApplicationUserApi(systemInfo, httpClient)

        applicationUserApi.registerApplicationUser({ exception ->
            isFailed = true
            exceptionMessage = exception.localizedMessage ?: exception.toString()
            userDto = null
            lock.countDown()
        }, { currentUserDto ->
            isFailed = false
            exceptionMessage = ""
            userDto = currentUserDto
            lock.countDown()
        })
        lock.await(5, TimeUnit.SECONDS)

        assertUserDto(
            isFailed,
            exceptionMessage,
            userDto,
            isExceptionExpected,
            expectedMessage,
            expectedUserDto
        )
    }

    @ParameterizedTest
    @MethodSource("testDataUserDto")
    fun testSetAttributes(
        clientId: String,
        userDtoFromClient: CurrentRevenueXUserDto?,
        exceptionMessageFromClient: String?,
        isExceptionExpected: Boolean,
        expectedMessage: String,
        expectedUserDto: CurrentRevenueXUserDto?
    ) {
        var isFailed = isExceptionExpected.not()
        var exceptionMessage = "DefaultMessage"
        var serverResult: CurrentRevenueXUserDto? = null

        // Mocks test state with parameters
        every { systemInfo.clientId } returns clientId
        mockHttpClientPost(clientId, userDtoFromClient, exceptionMessageFromClient)
        applicationUserApi = ImplApplicationUserApi(systemInfo, httpClient)

        val attributes = mapOf(UserAttributes.DisplayName to "UnitTest")
        applicationUserApi.setAttributes(attributes, { exception ->
            isFailed = true
            exceptionMessage = exception.localizedMessage ?: exception.toString()
            serverResult = null
            lock.countDown()
        }, { result ->
            isFailed = false
            exceptionMessage = ""
            serverResult = result
            lock.countDown()
        })
        lock.await(5, TimeUnit.SECONDS)

        assertUserDto(
            isFailed,
            exceptionMessage,
            serverResult,
            isExceptionExpected,
            expectedMessage,
            expectedUserDto
        )
    }

    @ParameterizedTest
    @MethodSource("testDataUserDto")
    fun testOpenEvent(
        clientId: String,
        userDtoFromClient: CurrentRevenueXUserDto?,
        exceptionMessageFromClient: String?,
        isExceptionExpected: Boolean,
        expectedMessage: String,
        expectedUserDto: CurrentRevenueXUserDto?
    ) {
        var isFailed = isExceptionExpected.not()
        var exceptionMessage = "DefaultMessage"
        var userDto: CurrentRevenueXUserDto? = null

        // Mocks test state with parameters
        every { systemInfo.clientId } returns clientId
        mockHttpClientPost(clientId, userDtoFromClient, exceptionMessageFromClient)
        applicationUserApi = ImplApplicationUserApi(systemInfo, httpClient)

        applicationUserApi.openEvent({ exception ->
            isFailed = true
            exceptionMessage = exception.localizedMessage ?: exception.toString()
            userDto = null
            lock.countDown()
        }, { currentUserDto ->
            isFailed = false
            exceptionMessage = ""
            userDto = currentUserDto
            lock.countDown()
        })
        lock.await(5, TimeUnit.SECONDS)

        assertUserDto(
            isFailed,
            exceptionMessage,
            userDto,
            isExceptionExpected,
            expectedMessage,
            expectedUserDto
        )
    }

    @ParameterizedTest
    @MethodSource("testDataUserDto")
    fun testPingEvent(
        clientId: String,
        userDtoFromClient: CurrentRevenueXUserDto?,
        exceptionMessageFromClient: String?,
        isExceptionExpected: Boolean,
        expectedMessage: String,
        expectedUserDto: CurrentRevenueXUserDto?
    ) {
        var isFailed = isExceptionExpected.not()
        var exceptionMessage = "DefaultMessage"
        var userDto: CurrentRevenueXUserDto? = null

        // Mocks test state with parameters
        every { systemInfo.clientId } returns clientId
        mockHttpClientPost(clientId, userDtoFromClient, exceptionMessageFromClient)
        applicationUserApi = ImplApplicationUserApi(systemInfo, httpClient)

        applicationUserApi.pingEvent({ exception ->
            isFailed = true
            exceptionMessage = exception.localizedMessage ?: exception.toString()
            userDto = null
            lock.countDown()
        }, { currentUserDto ->
            isFailed = false
            exceptionMessage = ""
            userDto = currentUserDto
            lock.countDown()
        })
        lock.await(5, TimeUnit.SECONDS)

        assertUserDto(
            isFailed,
            exceptionMessage,
            userDto,
            isExceptionExpected,
            expectedMessage,
            expectedUserDto
        )
    }

    @ParameterizedTest
    @MethodSource("testDataUserDto")
    fun testCreateAlias(
        oldUserId: String,
        userDtoFromClient: CurrentRevenueXUserDto?,
        exceptionMessageFromClient: String?,
        isExceptionExpected: Boolean,
        expectedMessage: String,
        expectedUserDto: CurrentRevenueXUserDto?
    ) {
        var isFailed = isExceptionExpected.not()
        var exceptionMessage = "DefaultMessage"
        var serverResult: CurrentRevenueXUserDto? = null

        // Mocks test state with parameters
        every { systemInfo.clientId } returns oldUserId
        mockHttpClientPost(oldUserId, userDtoFromClient, exceptionMessageFromClient)
        applicationUserApi = ImplApplicationUserApi(systemInfo, httpClient)

        val newUserId = "ClientId2"
        applicationUserApi.createAlias(newUserId, oldUserId, { exception ->
            isFailed = true
            exceptionMessage = exception.localizedMessage ?: exception.toString()
            serverResult = null
            lock.countDown()
        }, { result ->
            isFailed = false
            exceptionMessage = ""
            serverResult = result
            lock.countDown()
        })
        lock.await(5, TimeUnit.SECONDS)

        assertUserDto(
            isFailed,
            exceptionMessage,
            serverResult,
            isExceptionExpected,
            expectedMessage,
            expectedUserDto
        )
    }

    private fun assertUserDto(
        isFailed: Boolean,
        exceptionMessage: String,
        userDto: CurrentRevenueXUserDto?,
        isExceptionExpected: Boolean,
        expectedMessage: String,
        expectedUserDto: CurrentRevenueXUserDto?
    ) {
        assertEquals(isExceptionExpected, isFailed, "isFailed must be equal to expectation")
        assertEquals(
            expectedMessage,
            exceptionMessage,
            "Exception message must match with expectation"
        )

        if (isExceptionExpected) {
            assertNull(userDto, "UserDto must be null on exception")
        } else {
            assertEquals(
                expectedUserDto?.applicationId,
                userDto?.applicationId,
                "UserDto must match with expectation"
            )
            assertEquals(
                expectedUserDto?.revenueXId,
                userDto?.revenueXId,
                "UserDto must match with expectation"
            )
            assertEquals(
                expectedUserDto?.createdAt,
                userDto?.createdAt,
                "UserDto must match with expectation"
            )
            assertEquals(
                expectedUserDto?.updatedAt,
                userDto?.updatedAt,
                "UserDto must match with expectation"
            )

            userDto?.attributes?.forEach { attr ->
                val attributeValue = expectedUserDto?.attributes
                    ?.firstOrNull { it.name == attr.name }?.value
                assertEquals(attributeValue, attr.value, "UserDto must match with expectation")
            }
        }
    }

    /**
     * Input domain for registerApplicationUser method
     * @param clientId - "", "ClientId1", "ClientId2"
     * @param userDtoFromClient - null, User1, User2
     * @param exceptionMessageFromClient - null, "Exception thrown from http client"
     * @param isExceptionExpected - false, true
     * @param expectedMessage - If should success, empty string. Or changes according to expected exception
     * @param expectedUserDto - Changes according to parameters
     *
     * @sample Base1 - "ClientId1", user1, null, false, "", user1
     */
    private fun testDataUserDto() = Stream.of(
        Arguments.of("ClientId1", user1, null, false, "", user1),
        Arguments.of("", user1, null, false, "", user1),
        Arguments.of("ClientId2", user1, null, false, "", user1),
        Arguments.of("ClientId1", null, null, false, "", null),
        Arguments.of("ClientId1", user2, null, false, "", user2),
        Arguments.of("ClientId1", user1, "Exception thrown from http client", true, "Exception thrown from http client", user1)
    )

    /**
     * Test data as User1
     */
    private val user1 = CurrentRevenueXUserDto(
        "6197626f05537200257c2db9",
        "0d3b8fc66eeb45728f2a281fdb0e666",
        1637760985309,
        1637673118998,
        1637760985309,
        listOf(AttributeDto("Attr1", "Attr1 Value"))
    )

    /**
     * Test data as User2
     */
    private val user2 = CurrentRevenueXUserDto(
        "6197626f05537200257c2db9",
        "666b8fc66eeb54728f2d281acb0ec98",
        1637761430725,
        1637676886176,
        1637761430725
    )
}
