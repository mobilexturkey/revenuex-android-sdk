package tr.com.mobilex.applicationuser.manager

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.CoroutinesTestExtension
import tr.com.mobilex.InstantTaskExecutorExtension
import tr.com.mobilex.applicationuser.network.ApplicationUserApi
import tr.com.mobilex.applicationuser.network.AttributeDto
import tr.com.mobilex.applicationuser.network.CurrentRevenueXUserDto
import tr.com.mobilex.applicationuser.network.UserAttributes
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.coroutine.DispatcherProvider
import tr.com.mobilex.core.db.dao.AttributeDao
import tr.com.mobilex.core.db.dao.UserDao
import tr.com.mobilex.core.db.entity.UserEntity
import tr.com.mobilex.core.db.relational.UserWithAttributes
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.utils.toDate
import java.util.Date
import java.util.stream.Stream

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@ExtendWith(CoroutinesTestExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplApplicationUserManagerTests {

    private lateinit var applicationUserManager: ApplicationUserManager

    private lateinit var applicationUserApi: ApplicationUserApi

    private val testDispatcher = TestCoroutineDispatcher()

    @MockK
    lateinit var systemInfo: SystemInfo

    @MockK
    lateinit var applicationUserDao: UserDao

    @MockK
    lateinit var attributeDao: AttributeDao

    @BeforeEach
    fun setUp() {
        mockLogger()
        mockDispatcherProvider()
        mockSystemInfo()
        mockDAOs()
        applicationUserApi = mockk()
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockDispatcherProvider() {
        mockkObject(DispatcherProvider)
        every { DispatcherProvider.Default } returns testDispatcher
        every { DispatcherProvider.IO } returns testDispatcher
        every { DispatcherProvider.Main } returns testDispatcher
        every { DispatcherProvider.Unconfined } returns testDispatcher
    }

    private fun mockSystemInfo() {
        every { systemInfo.revenueXUserId } returns "UserId"
        every { systemInfo.rootUrl } returns "https://www.revenuex.com"
        every { systemInfo.platform } returns "android"
        every { systemInfo.platformVersion } returns "10"
        every { systemInfo.deviceName } returns "TestDevice"
        every { systemInfo.applicationVersion } returns "1.2.3"
        every { systemInfo.isSandbox } returns true
        every { systemInfo.region } returns "TR"
        every { systemInfo.language } returns "tr-TR"
    }

    private fun mockSystemInfoWithIds(
        clientId: String,
        response: CurrentRevenueXUserDto?
    ) {
        every { systemInfo.clientId } returns clientId
        every { systemInfo.revenueXUserId = any() } answers {
            every { systemInfo.revenueXUserId } returns (response?.revenueXId ?: "UserId")
        }
    }

    private fun mockDAOs() {
        val cachedUser = UserWithAttributes(UserEntity(id = 1, revenueXId = "UserId"))
        every { applicationUserDao.getUsersWithAttributes() } returns listOf(cachedUser)
        every { applicationUserDao.update(any()) } answers { }
        every { attributeDao.insert(any()) } returns 1
        every { attributeDao.clear() } answers { }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockOpenEvent(
        response: CurrentRevenueXUserDto?,
        throwException: Boolean
    ) {
        every { applicationUserApi.openEvent(any(), any()) } answers {
            if (throwException) {
                val onFailure = it.invocation.args[0] as (Exception) -> Unit
                onFailure.invoke(Exception("Mocked exception message thrown from api"))
            } else {
                val onResponse = it.invocation.args[1] as (CurrentRevenueXUserDto?) -> Unit
                onResponse.invoke(response)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockSetAttribute(
        response: CurrentRevenueXUserDto?,
        throwException: Boolean,
        attributes: Map<UserAttributes, String>
    ) {
        every { applicationUserApi.setAttributes(attributes, any(), any()) } answers {
            if (throwException) {
                val onFailure = it.invocation.args[1] as (Exception) -> Unit
                onFailure.invoke(Exception("Mocked exception message thrown from api"))
            } else {
                val onResponse = it.invocation.args[2] as (CurrentRevenueXUserDto?) -> Unit
                onResponse.invoke(response)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockCreateAlias(
        response: CurrentRevenueXUserDto?,
        throwException: Boolean,
        newUserId: String,
        oldUserId: String
    ) {
        every { applicationUserApi.createAlias(newUserId, oldUserId, any(), any()) } answers {
            if (throwException) {
                val onFailure = it.invocation.args[2] as (Exception) -> Unit
                onFailure.invoke(Exception("Mocked exception message thrown from api"))
            } else {
                val onResponse = it.invocation.args[3] as (CurrentRevenueXUserDto?) -> Unit
                onResponse.invoke(response)
            }
        }
    }

    @ParameterizedTest
    @MethodSource
    fun openEvent(
        clientId: String,
        userDtoFromApi: CurrentRevenueXUserDto?,
        throwException: Boolean,
        isExceptionExpected: Boolean,
        expectedUserDto: CurrentRevenueXUserDto?
    ) {
        // Mocks test state with parameters
        mockSystemInfoWithIds(clientId, userDtoFromApi)
        mockOpenEvent(userDtoFromApi, throwException)

        applicationUserManager = ImplApplicationUserManager(
            systemInfo,
            applicationUserApi,
            applicationUserDao,
            attributeDao
        )
        applicationUserManager.openEvent {
            verify { applicationUserApi.openEvent(any(), any()) }
        }

        // Assertions
        assertCurrentUserInformation(expectedUserDto)
    }

    @ParameterizedTest
    @MethodSource
    fun setAttributes(
        clientId: String,
        userDtoFromApi: CurrentRevenueXUserDto?,
        throwException: Boolean,
        attributes: Map<UserAttributes, String>,
        isExceptionExpected: Boolean,
        expectedUserDto: CurrentRevenueXUserDto?
    ) {
        // Mocks test state with parameters
        mockSystemInfoWithIds(clientId, userDtoFromApi)
        mockSetAttribute(userDtoFromApi, throwException, attributes)

        applicationUserManager = ImplApplicationUserManager(
            systemInfo,
            applicationUserApi,
            applicationUserDao,
            attributeDao
        )
        applicationUserManager.setAttributes(attributes)

        // Assertions
        verify { applicationUserApi.setAttributes(attributes, any(), any()) }
        assertCurrentUserInformation(expectedUserDto)
        if (isExceptionExpected || userDtoFromApi == null) {
            assertLocalAttributesOnFailure(attributes)
        }
    }

    @ParameterizedTest
    @MethodSource
    fun createAlias(
        oldUserId: String,
        userDtoFromApi: CurrentRevenueXUserDto?,
        throwException: Boolean,
        newUserId: String,
        isExceptionExpected: Boolean,
        expectedUserDto: CurrentRevenueXUserDto?
    ) {
        // Mocks test state with parameters
        mockSystemInfoWithIds(oldUserId, userDtoFromApi)
        mockCreateAlias(userDtoFromApi, throwException, newUserId, oldUserId)

        applicationUserManager = ImplApplicationUserManager(
            systemInfo,
            applicationUserApi,
            applicationUserDao,
            attributeDao
        )
        applicationUserManager.createAlias(newUserId, oldUserId)

        // Assertions
        val apiCallCount = if (newUserId == oldUserId) 0 else 1
        verify(exactly = apiCallCount) {
            applicationUserApi.createAlias(newUserId, oldUserId, any(), any())
        }
        assertCurrentUserInformation(expectedUserDto)
    }

    private fun assertCurrentUserInformation(expectedUserDto: CurrentRevenueXUserDto?) {
        val liveDataUser = applicationUserManager.revenueXUser.value
        assertEquals(expectedUserDto!!.revenueXId, systemInfo.revenueXUserId)
        assertEquals(expectedUserDto.applicationId, liveDataUser!!.user.applicationId)
        assertEquals(expectedUserDto.revenueXId, liveDataUser.user.revenueXId)
        assertEquals(expectedUserDto.lastSeenDate?.toDate(), liveDataUser.user.lastSeenDate)
        assertEquals(expectedUserDto.updatedAt?.toDate(), liveDataUser.user.updatedAt)
        if (expectedUserDto.createdAt == null) {
            assertEquals(Date().day, liveDataUser.user.createdAt!!.day)
        } else {
            assertEquals(expectedUserDto.createdAt?.toDate(), liveDataUser.user.createdAt)
        }
    }

    private fun assertLocalAttributesOnFailure(attributes: Map<UserAttributes, String>) {
        val liveDataUser = applicationUserManager.revenueXUser.value
        attributes.forEach { entry ->
            val userAttr = liveDataUser!!.attributes.firstOrNull { it.name == entry.key.key }
            assertEquals(entry.value, userAttr?.value)
        }
    }

    /**
     * Input domain for openEvent method
     * @param clientId - "", "ClientId1", "ClientId2"
     * @param userDtoFromApi - null, User1, User2
     * @param throwException - false, true
     * @param isExceptionExpected - false, true
     * @param expectedUserDto - Changes according to parameters
     *
     * @sample Base1 - "ClientId1", user1, false, false, user1
     */
    private fun openEvent() = Stream.of(
        Arguments.of("ClientId1", user1, false, false, user1),
        Arguments.of("", user1, false, false, user1),
        Arguments.of("ClientId2", user1, false, false, user1),
        Arguments.of("ClientId1", null, false, false, defaultUser),
        Arguments.of("ClientId1", user2, false, false, user2),
        Arguments.of("ClientId1", null, true, true, defaultUser)
    )

    /**
     * Input domain for setAttributes method
     * @param clientId - "", "ClientId1", "ClientId2"
     * @param userDtoFromApi - null, User1, User2
     * @param throwException - false, true
     * @param attributes - [], ["Email": "mail@gmail.com"], ["Email": "mail@gmail.com", "Phone": "5556667788"]
     * @param isExceptionExpected - false, true
     * @param expectedUserDto - Changes according to parameters
     *
     * @sample Base1 - "ClientId1", user1, false, ["Email": "mail@gmail.com"], false, user1
     */
    private fun setAttributes() = Stream.of(
        Arguments.of("ClientId1", user1, false, mailAttrMap, false, user1),
        Arguments.of("", user1, false, mailAttrMap, false, user1),
        Arguments.of("ClientId2", user1, false, mailAttrMap, false, user1),
        Arguments.of("ClientId1", null, false, mailAttrMap, false, defaultUser),
        Arguments.of("ClientId1", user2, false, mailAttrMap, false, user2),
        Arguments.of("ClientId1", null, true, mailAttrMap, true, defaultUser),
        Arguments.of("ClientId1", user1, false, emptyAttrMap, false, user1),
        Arguments.of("ClientId1", user1, false, phoneAttrMap, false, user1)
    )

    /**
     * Input domain for setAttributes method
     * @param oldUserId - "", "ClientId1", "ClientId2"
     * @param userDtoFromApi - null, User1, User2
     * @param throwException - false, true
     * @param newUserId - "", "ClientId1", "ClientId2"
     * @param isExceptionExpected - false, true
     * @param expectedUserDto - Changes according to parameters
     *
     * @sample Base1 - "ClientId1", user1, false, "ClientId2", false, user1
     */
    private fun createAlias() = Stream.of(
        Arguments.of("ClientId1", user1, false, "ClientId2", false, user1),
        Arguments.of("", user1, false, "ClientId2", false, user1),
        Arguments.of("ClientId2", user1, false, "ClientId2", false, defaultUser),
        Arguments.of("ClientId1", null, false, "ClientId2", false, defaultUser),
        Arguments.of("ClientId1", user2, false, "ClientId2", false, user2),
        Arguments.of("ClientId1", null, true, "ClientId2", true, defaultUser),
        Arguments.of("ClientId1", user1, false, "", false, user1),
        Arguments.of("ClientId1", user1, false, "ClientId1", false, defaultUser)
    )

    /**
     * Test data as attribute list
     */
    private val emptyAttrMap = mapOf<UserAttributes, String>()
    private val mailAttrMap = mapOf(UserAttributes.Email to "mail@gmail.com")
    private val phoneAttrMap = mapOf(
        UserAttributes.Email to "mail@gmail.com",
        UserAttributes.PhoneNumber to "5556667788"
    )

    /**
     * Test data as user managers default state
     */
    private val defaultUser = CurrentRevenueXUserDto(
        null,
        "UserId",
        null,
        null,
        null
    )

    /**
     * Test data as User1
     */
    private val user1 = CurrentRevenueXUserDto(
        "6197626f05537200257c2db9",
        "0d3b8fc66eeb45728f2a281fdb0e666",
        1637760985309,
        1637673118998,
        1637760985309,
        listOf(AttributeDto("Attr1", "Attr1 Value"))
    )

    /**
     * Test data as User2
     */
    private val user2 = CurrentRevenueXUserDto(
        "6197626f05537200257c2db9",
        "666b8fc66eeb54728f2d281acb0ec98",
        1637761430725,
        1637676886176,
        1637761430725
    )
}
