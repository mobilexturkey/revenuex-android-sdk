package tr.com.mobilex.core.identity

import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockkObject
import io.mockk.mockkStatic
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.sharedpref.EncryptedSharedPrefManager
import java.util.UUID
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplIdentityManagerTests {

    private lateinit var identityManager: IdentityManager

    @BeforeEach
    fun setUp() {
        mockLogger()
        mockUUIDGenerator()
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockUUIDGenerator() {
        mockkStatic(UUID::class)
        every {
            UUID.randomUUID().toString()
        } returns "06b15d4a-5d0f-11ec-bf63-0242ac130002"
    }

    private fun mockEncryptedSharedPref(cachedUserId: String?) {
        mockkObject(EncryptedSharedPrefManager)
        every {
            EncryptedSharedPrefManager.write(Constants.REVENUEX_USER_ID_KEY, any())
        } answers { }
        every {
            EncryptedSharedPrefManager.read(Constants.REVENUEX_USER_ID_KEY, String::class.java)
        } returns cachedUserId
    }

    @ParameterizedTest
    @MethodSource
    fun getRevenueXUserId(
        providedUserId: String?,
        cachedUserId: String?,
        expectedUserId: String,
        callbackShouldBeCalled: Boolean
    ) {
        var callbackIsCalled = false
        mockEncryptedSharedPref(cachedUserId)
        identityManager = ImplIdentityManager(EncryptedSharedPrefManager)

        val finalUserId = identityManager.getRevenueXUserId(providedUserId) { _, _ ->
            callbackIsCalled = true
        }

        assertEquals(callbackShouldBeCalled, callbackIsCalled, "Callback should be called")
        assertEquals(expectedUserId, finalUserId, "Expected id not found")
        verify(atMost = 1) {
            EncryptedSharedPrefManager.write(Constants.REVENUEX_USER_ID_KEY, expectedUserId)
        }
    }

    /**
     * Input domain for getRevenueXUserId method
     * @param providedUserId - null, "", "a4455e08-5d0f-11ec-bf63-0242ac130002"
     * @param cachedUserId - null, "", "b6a87666-5d0f-11ec-bf63-0242ac130002"
     * @param expectedUserId - Changes according to parameters
     * @param callbackShouldBeCalled - Changes according to parameters
     *
     * @sample Base1 - null, null, "06b15d4a5d0f11ecbf630242ac130002"
     */
    private fun getRevenueXUserId() = Stream.of(
        Arguments.of(null, null, "06b15d4a5d0f11ecbf630242ac130002", false),
        Arguments.of("", null, "06b15d4a5d0f11ecbf630242ac130002", false),
        Arguments.of("a4455e08-5d0f-11ec-bf63-0242ac130002", null, "a4455e08-5d0f-11ec-bf63-0242ac130002", false),
        Arguments.of(null, "", "06b15d4a5d0f11ecbf630242ac130002", false),
        Arguments.of(null, "b6a87666-5d0f-11ec-bf63-0242ac130002", "b6a87666-5d0f-11ec-bf63-0242ac130002", false),
        Arguments.of("a4455e08-5d0f-11ec-bf63-0242ac130002", "b6a87666-5d0f-11ec-bf63-0242ac130002", "a4455e08-5d0f-11ec-bf63-0242ac130002", true)
    )
}
