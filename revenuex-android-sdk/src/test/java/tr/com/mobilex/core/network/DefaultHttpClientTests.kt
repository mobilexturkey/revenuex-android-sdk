package tr.com.mobilex.core.network

import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.stream.Stream

@Suppress("KDocUnresolvedReference")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class DefaultHttpClientTests {

    private lateinit var client: OkHttpClient
    private lateinit var httpClient: HttpClient
    private lateinit var lock: CountDownLatch
    private lateinit var mockWebServer: MockWebServer

    @BeforeEach
    fun setUp() {
        client = OkHttpClient()
        httpClient = DefaultHttpClient(client)
        lock = CountDownLatch(1)
        mockWebServer = MockWebServer()
        mockWebServer.start(8080)
    }

    @AfterEach
    fun tearDown() {
        mockWebServer.shutdown()
    }

    private fun mockResponse() {
        val mockedResponse = MockResponse()
            .setBody("[{\"message\": \"Hello World!\"}]")
            .addHeader("Content-Type", "application/json")
        mockWebServer.enqueue(mockedResponse)
    }

    @ParameterizedTest
    @MethodSource
    fun testGet(
        url: String,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        shouldFail: Boolean,
        expectedExceptionMessage: String,
        expectedUrl: String
    ) {
        var isFailed = shouldFail.not()
        var exceptionMessage = "DefaultMessage"

        mockResponse()

        httpClient.get(url, queryParams, headers, {
            isFailed = true
            exceptionMessage = it.localizedMessage ?: it.toString()
            lock.countDown()
        }, {
            isFailed = false
            exceptionMessage = ""
            lock.countDown()
        }, String::class.java)
        lock.await(5, TimeUnit.SECONDS)

        assertTestResult(
            url,
            null,
            headers,
            shouldFail,
            expectedExceptionMessage,
            expectedUrl,
            isFailed,
            exceptionMessage
        )
    }

    @ParameterizedTest
    @MethodSource("testDataWithBody")
    fun testPost(
        url: String,
        jsonBody: String,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        shouldFail: Boolean,
        expectedExceptionMessage: String,
        expectedUrl: String
    ) {
        var isFailed = shouldFail.not()
        var exceptionMessage = "DefaultMessage"

        mockResponse()

        httpClient.post(url, jsonBody, queryParams, headers, {
            isFailed = true
            exceptionMessage = it.localizedMessage ?: it.toString()
            lock.countDown()
        }, {
            isFailed = false
            exceptionMessage = ""
            lock.countDown()
        }, String::class.java)
        lock.await(5, TimeUnit.SECONDS)

        assertTestResult(
            url,
            jsonBody,
            headers,
            shouldFail,
            expectedExceptionMessage,
            expectedUrl,
            isFailed,
            exceptionMessage
        )
    }

    @ParameterizedTest
    @MethodSource("testDataWithBody")
    fun testPut(
        url: String,
        jsonBody: String,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        shouldFail: Boolean,
        expectedExceptionMessage: String,
        expectedUrl: String
    ) {
        var isFailed = shouldFail.not()
        var exceptionMessage = "DefaultMessage"

        mockResponse()

        httpClient.put(url, jsonBody, queryParams, headers, {
            isFailed = true
            exceptionMessage = it.localizedMessage ?: it.toString()
            lock.countDown()
        }, {
            isFailed = false
            exceptionMessage = ""
            lock.countDown()
        }, String::class.java)
        lock.await(5, TimeUnit.SECONDS)

        assertTestResult(
            url,
            jsonBody,
            headers,
            shouldFail,
            expectedExceptionMessage,
            expectedUrl,
            isFailed,
            exceptionMessage
        )
    }

    @ParameterizedTest
    @MethodSource("testDataWithBody")
    fun testPatch(
        url: String,
        jsonBody: String,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        shouldFail: Boolean,
        expectedExceptionMessage: String,
        expectedUrl: String
    ) {
        var isFailed = shouldFail.not()
        var exceptionMessage = "DefaultMessage"

        mockResponse()

        httpClient.patch(url, jsonBody, queryParams, headers, {
            isFailed = true
            exceptionMessage = it.localizedMessage ?: it.toString()
            lock.countDown()
        }, {
            isFailed = false
            exceptionMessage = ""
            lock.countDown()
        }, String::class.java)
        lock.await(5, TimeUnit.SECONDS)

        assertTestResult(
            url,
            jsonBody,
            headers,
            shouldFail,
            expectedExceptionMessage,
            expectedUrl,
            isFailed,
            exceptionMessage
        )
    }

    @ParameterizedTest
    @MethodSource("testDataWithBody")
    fun testDelete(
        url: String,
        jsonBody: String?,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        shouldFail: Boolean,
        expectedExceptionMessage: String,
        expectedUrl: String
    ) {
        var isFailed = shouldFail.not()
        var exceptionMessage = "DefaultMessage"

        mockResponse()

        httpClient.delete(url, jsonBody, queryParams, headers, {
            isFailed = true
            exceptionMessage = it.localizedMessage ?: it.toString()
            lock.countDown()
        }, {
            isFailed = false
            exceptionMessage = ""
            lock.countDown()
        }, String::class.java)
        lock.await(5, TimeUnit.SECONDS)

        assertTestResult(
            url,
            jsonBody,
            headers,
            shouldFail,
            expectedExceptionMessage,
            expectedUrl,
            isFailed,
            exceptionMessage
        )
    }

    private fun assertTestResult(
        url: String,
        jsonBody: String?,
        headers: Map<String, String>?,
        shouldFail: Boolean,
        expectedExceptionMessage: String,
        expectedUrl: String,
        isFailed: Boolean,
        exceptionMessage: String
    ) {
        if (url == "http://localhost:8080/") {
            val request = mockWebServer.takeRequest(2, TimeUnit.SECONDS)

            assertEquals(expectedUrl, request!!.requestUrl.toString(), "URL does not match")
            assertTrue(request.body.toString().contains(jsonBody ?: ""), "Request body is missing")
            headers?.forEach {
                assertTrue(
                    request.headers.contains(Pair(it.key, it.value)),
                    "Header is not added to request"
                )
            }
        }

        assertEquals(shouldFail, isFailed, "isFailed must be equal to expectation")
        assertEquals(
            expectedExceptionMessage,
            exceptionMessage,
            "Exception message must match with expected exception message"
        )
    }

    /**
     * Input domain for get method
     * @param url - "", "http://localhost:8080/", "http://localhost:666/"
     * @param queryParams - null, [], ["textParam": "TextValue"], ["textParam": "TextValue", "emptyTextParam": ""], ["textParam": "TextValue", "nullTextParam": null], ["textParam": "TextValue", "numericParam": "123"]
     * @param headers - null, [], ["textParam": "TextValue"], ["textParam": "TextValue", "emptyTextParam": ""], ["textParam": "TextValue", "numericParam": "123"]
     * @param shouldFail - false, true
     * @param expectedExceptionMessage - If should success, empty string. Or changes according to expected exception
     * @param expectedUrl - Changes according to url and query params
     *
     * @sample Base1 - "http://localhost:8080/", null, null, false, ""
     */
    private fun testGet() = Stream.of(
        Arguments.of("http://localhost:8080/", null, null, false, "", "http://localhost:8080/"),
        Arguments.of("", null, null, true, "Expected URL scheme 'http' or 'https' but no colon was found", ""),
        //Arguments.of("http://localhost:666/", null, null, true, "Failed to connect to localhost/0:0:0:0:0:0:0:1:666", ""),
        Arguments.of("http://localhost:8080/", mapOf<String, String?>(), null, false, "", "http://localhost:8080/"),
        Arguments.of("http://localhost:8080/", mapOf("textParam" to "TextValue"), null, false, "", "http://localhost:8080/?textParam=TextValue"),
        Arguments.of("http://localhost:8080/", mapOf("textParam" to "TextValue", "emptyTextParam" to ""), null, false, "", "http://localhost:8080/?textParam=TextValue&emptyTextParam="),
        Arguments.of("http://localhost:8080/", mapOf("textParam" to "TextValue", "nullTextParam" to null), null, false, "", "http://localhost:8080/?textParam=TextValue&nullTextParam"),
        Arguments.of("http://localhost:8080/", mapOf("textParam" to "TextValue", "numericParam" to "123"), null, false, "", "http://localhost:8080/?textParam=TextValue&numericParam=123"),
        Arguments.of("http://localhost:8080/", null, mapOf<String, String>(), false, "", "http://localhost:8080/"),
        Arguments.of("http://localhost:8080/", null, mapOf("textParam" to "TextValue"), false, "", "http://localhost:8080/"),
        Arguments.of("http://localhost:8080/", null, mapOf("textParam" to "TextValue", "emptyTextParam" to ""), false, "", "http://localhost:8080/"),
        Arguments.of("http://localhost:8080/", null, mapOf("textParam" to "TextValue", "numericParam" to "123"), false, "", "http://localhost:8080/")
    )

    /**
     * Input domain for get method
     * @param url - "", "http://localhost:8080/", "http://localhost:666/"
     * @param jsonBody - "", "[{\"message\": \"Hello World!\"}]"
     * @param queryParams - null, [], ["textParam": "TextValue"], ["textParam": "TextValue", "emptyTextParam": ""], ["textParam": "TextValue", "nullTextParam": null], ["textParam": "TextValue", "numericParam": "123"]
     * @param headers - null, [], ["textParam": "TextValue"], ["textParam": "TextValue", "emptyTextParam": ""], ["textParam": "TextValue", "numericParam": "123"]
     * @param shouldFail - false, true
     * @param expectedExceptionMessage - If should success, empty string. Or changes according to expected exception
     * @param expectedUrl - Changes according to url and query params
     *
     * @sample Base1 - "http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", null, null, false, ""
     */
    private fun testDataWithBody() = Stream.of(
        Arguments.of("http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", null, null, false, "", "http://localhost:8080/"),
        Arguments.of("", "[{\"message\": \"Hello World!\"}]", null, null, true, "Expected URL scheme 'http' or 'https' but no colon was found", ""),
        //Arguments.of("http://localhost:666/", "[{\"message\": \"Hello World!\"}]", null, null, true, "Failed to connect to localhost/0:0:0:0:0:0:0:1:666", ""),
        Arguments.of("http://localhost:8080/", "", null, null, false, "", "http://localhost:8080/"),
        Arguments.of("http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", mapOf<String, String?>(), null, false, "", "http://localhost:8080/"),
        Arguments.of("http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", mapOf("textParam" to "TextValue"), null, false, "", "http://localhost:8080/?textParam=TextValue"),
        Arguments.of("http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", mapOf("textParam" to "TextValue", "emptyTextParam" to ""), null, false, "", "http://localhost:8080/?textParam=TextValue&emptyTextParam="),
        Arguments.of("http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", mapOf("textParam" to "TextValue", "nullTextParam" to null), null, false, "", "http://localhost:8080/?textParam=TextValue&nullTextParam"),
        Arguments.of("http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", mapOf("textParam" to "TextValue", "numericParam" to "123"), null, false, "", "http://localhost:8080/?textParam=TextValue&numericParam=123"),
        Arguments.of("http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", null, mapOf<String, String>(), false, "", "http://localhost:8080/"),
        Arguments.of("http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", null, mapOf("textParam" to "TextValue"), false, "", "http://localhost:8080/"),
        Arguments.of("http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", null, mapOf("textParam" to "TextValue", "emptyTextParam" to ""), false, "", "http://localhost:8080/"),
        Arguments.of("http://localhost:8080/", "[{\"message\": \"Hello World!\"}]", null, mapOf("textParam" to "TextValue", "numericParam" to "123"), false, "", "http://localhost:8080/")
    )
}
