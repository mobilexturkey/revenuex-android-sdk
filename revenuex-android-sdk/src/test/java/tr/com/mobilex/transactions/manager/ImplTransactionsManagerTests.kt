package tr.com.mobilex.transactions.manager

import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.InstantTaskExecutorExtension
import tr.com.mobilex.core.billing.BillingClientManager
import tr.com.mobilex.core.billing.ProductType
import tr.com.mobilex.core.billing.RevXProduct
import tr.com.mobilex.core.billing.RevXPurchase
import tr.com.mobilex.core.billing.RevXPurchaseHistory
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.sharedpref.EncryptedSharedPrefManager
import tr.com.mobilex.entitlements.manager.EntitlementsManager
import tr.com.mobilex.products.manager.ProductsManager
import tr.com.mobilex.transactions.network.TransactionsApi
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplTransactionsManagerTests {

    private lateinit var transactionsManager: TransactionsManager

    private lateinit var transactionsApi: TransactionsApi

    private lateinit var billingClient: BillingClientManager

    private lateinit var productsManager: ProductsManager

    private lateinit var entitlementsManager: EntitlementsManager

    private lateinit var secureSharedPref: EncryptedSharedPrefManager

    @BeforeEach
    fun setUp() {
        mockLogger()
        mockBillingClient()
        mockProductsManager()
        mockEntitlementsManager()
        transactionsApi = mockk()
        secureSharedPref = mockk()
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockBillingClient() {
        billingClient = mockk()
        every { billingClient.newPurchaseListener = any() } answers { }
        every { billingClient.purchaseProduct(any(), any(), any()) } answers { }
    }

    private fun mockProductsManager() {
        productsManager = mockk()
        every { productsManager.products } returns listOf(product1, product2)
    }

    private fun mockEntitlementsManager() {
        entitlementsManager = mockk()
        every { entitlementsManager.fetchEntitlements(any()) } answers { }
    }

    private fun mockSecureSharedPref(validatedTokens: Array<String>?) {
        every { secureSharedPref.write<Array<String>>(any(), any()) } answers { }
        every { secureSharedPref.read<Array<String>>(any(), any()) } returns validatedTokens
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockTransactionsApi(isValidateSuccessful: Boolean) {
        every { transactionsApi.validate(any(), any(), any()) } answers {
            if (isValidateSuccessful) {
                val onResponse = it.invocation.args[2] as (String?) -> Unit
                onResponse.invoke("{ \"subscriptions\": [], \"transactions\": [], \"eventLogs\": [] }")
            } else {
                val onFailure = it.invocation.args[1] as (Exception) -> Unit
                onFailure.invoke(Exception("Mocked exception message thrown from api"))
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockQueryPurchaseHistory(
        purchaseHistoryFromBilling: List<RevXPurchaseHistory>
    ) {
        val subList = purchaseHistoryFromBilling.filter { it.type == ProductType.Subs }
        val inAppList = purchaseHistoryFromBilling.filter { it.type == ProductType.InApp }

        every { billingClient.queryPurchaseHistory(ProductType.Subs, any()) } answers {
            val callback = it.invocation.args[1] as ((List<RevXPurchaseHistory>) -> Unit)?
            callback?.invoke(subList)
        }
        every { billingClient.queryPurchaseHistory(ProductType.InApp, any()) } answers {
            val callback = it.invocation.args[1] as ((List<RevXPurchaseHistory>) -> Unit)?
            callback?.invoke(inAppList)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockQueryPurchases(
        purchasesFromBilling: List<RevXPurchase>
    ) {
        val subList = purchasesFromBilling.filter { it.type == ProductType.Subs }
        val inAppList = purchasesFromBilling.filter { it.type == ProductType.InApp }

        every { billingClient.queryPurchases(ProductType.Subs, any()) } answers {
            val callback = it.invocation.args[1] as ((List<RevXPurchase>) -> Unit)?
            callback?.invoke(subList)
        }
        every { billingClient.queryPurchases(ProductType.InApp, any()) } answers {
            val callback = it.invocation.args[1] as ((List<RevXPurchase>) -> Unit)?
            callback?.invoke(inAppList)
        }
    }

    @ParameterizedTest
    @MethodSource
    fun sendPurchaseHistory(
        purchaseHistoryFromBilling: List<RevXPurchaseHistory>,
        validatedTokens: Array<String>?,
        isValidateSuccessful: Boolean,
        expectedHistorySize: Int,
        expectedValidateCalls: Int
    ) {
        mockSecureSharedPref(validatedTokens)
        mockTransactionsApi(isValidateSuccessful)
        mockQueryPurchaseHistory(purchaseHistoryFromBilling)

        transactionsManager = ImplTransactionsManager(
            transactionsApi,
            billingClient,
            productsManager,
            entitlementsManager,
            secureSharedPref,
            true
        )
        transactionsManager.sendPurchaseHistory()

        assertHistoryLiveData(expectedHistorySize)
        verify(exactly = expectedValidateCalls) { transactionsApi.validate(any(), any(), any()) }
        if (isValidateSuccessful) {
            verify { secureSharedPref.write<Array<String>>(any(), any()) }
        }
    }

    private fun assertHistoryLiveData(expectedHistorySize: Int) {
        assertNotNull(
            transactionsManager.purchaseHistory.value,
            "Purchase history should not be null"
        )
        assertEquals(
            expectedHistorySize,
            transactionsManager.purchaseHistory.value!!.size,
            "History size does not match"
        )
    }

    @ParameterizedTest
    @MethodSource
    fun sendActivePurchases(
        purchasesFromBilling: List<RevXPurchase>,
        validatedTokens: Array<String>?,
        isValidateSuccessful: Boolean,
        expectedPurchaseSize: Int,
        expectedValidateCalls: Int
    ) {
        mockSecureSharedPref(validatedTokens)
        mockTransactionsApi(isValidateSuccessful)
        mockQueryPurchases(purchasesFromBilling)

        transactionsManager = ImplTransactionsManager(
            transactionsApi,
            billingClient,
            productsManager,
            entitlementsManager,
            secureSharedPref,
            true
        )
        transactionsManager.sendActivePurchases()

        assertPurchaseLiveData(expectedPurchaseSize)
        verify(exactly = expectedValidateCalls) { transactionsApi.validate(any(), any(), any()) }
        if (isValidateSuccessful) {
            verify { secureSharedPref.write<Array<String>>(any(), any()) }
        }
    }

    private fun assertPurchaseLiveData(expectedPurchaseSize: Int) {
        assertNotNull(
            transactionsManager.purchases.value,
            "Purchases should not be null"
        )
        assertEquals(
            expectedPurchaseSize,
            transactionsManager.purchases.value!!.size,
            "Purchase size does not match"
        )
    }

    @ParameterizedTest
    @MethodSource
    fun purchaseProduct(
        observerMode: Boolean,
        expectedCalls: Int
    ) {
        transactionsManager = ImplTransactionsManager(
            transactionsApi,
            billingClient,
            productsManager,
            entitlementsManager,
            secureSharedPref,
            observerMode
        )
        transactionsManager.purchaseProduct(mockk(), mockk(), mockk())

        verify(exactly = expectedCalls) { billingClient.purchaseProduct(any(), any(), any()) }
    }

    @ParameterizedTest
    @MethodSource
    fun onNewPurchaseComplete(
        newPurchase: RevXPurchase,
        validatedTokens: Array<String>?,
        isValidateSuccessful: Boolean,
        expectedValidateCalls: Int
    ) {
        mockSecureSharedPref(validatedTokens)
        mockTransactionsApi(isValidateSuccessful)

        transactionsManager = ImplTransactionsManager(
            transactionsApi,
            billingClient,
            productsManager,
            entitlementsManager,
            secureSharedPref,
            true
        )
        transactionsManager.onNewPurchaseComplete(newPurchase)

        verify(exactly = expectedValidateCalls) { transactionsApi.validate(any(), any(), any()) }
        if (isValidateSuccessful) {
            verify { secureSharedPref.write<Array<String>>(any(), any()) }
        }
    }

    /**
     * Input domain for sendPurchaseHistory method
     * @param purchaseHistoryFromBilling - empty, oneProduct, twoProductsMix
     * @param validatedTokens - null, [ ], [ "PurchaseToken1" ]
     * @param isValidateSuccessful - false, true
     * @param expectedHistorySize - Changes according to parameters
     * @param expectedValidateCalls - Changes according to parameters
     *
     * @sample Base1 - oneProduct, null, false, 1, 1
     */
    private fun sendPurchaseHistory() = Stream.of(
        Arguments.of(listOf(history1), null, false, 1, 1),
        Arguments.of(listOf<RevXPurchaseHistory>(), null, false, 0, 0),
        Arguments.of(listOf(history1, history2), null, false, 2, 2),
        Arguments.of(listOf(history1), arrayOf<String>(), false, 1, 1),
        Arguments.of(listOf(history1), arrayOf("PurchaseToken1"), false, 1, 0),
        Arguments.of(listOf(history1), null, true, 1, 1)
    )

    /**
     * Input domain for sendActivePurchases method
     * @param purchasesFromBilling - empty, oneProduct, twoProductsMix
     * @param validatedTokens - null, [ ], [ "PurchaseToken1" ]
     * @param isValidateSuccessful - false, true
     * @param expectedPurchaseSize - Changes according to parameters
     * @param expectedValidateCalls - Changes according to parameters
     *
     * @sample Base1 - oneProduct, null, false, 1, 1
     */
    private fun sendActivePurchases() = Stream.of(
        Arguments.of(listOf(purchase1), null, false, 1, 1),
        Arguments.of(listOf<RevXPurchase>(), null, false, 0, 0),
        Arguments.of(listOf(purchase1, purchase2), null, false, 2, 2),
        Arguments.of(listOf(purchase1), arrayOf<String>(), false, 1, 1),
        Arguments.of(listOf(purchase1), arrayOf("PurchaseToken1"), false, 1, 0),
        Arguments.of(listOf(purchase1), null, true, 1, 1)
    )

    /**
     * Input domain for purchaseProduct method
     * @param observerMode - false, true
     * @param expectedCalls - Changes according to parameters
     *
     * @sample Base1 - true, 0
     */
    private fun purchaseProduct() = Stream.of(
        Arguments.of(true, 0),
        Arguments.of(false, 1)
    )

    /**
     * Input domain for onNewPurchaseComplete method
     * @param newPurchase - purchase1, purchase2
     * @param validatedTokens - null, [ ], [ "PurchaseToken1" ]
     * @param isValidateSuccessful - false, true
     * @param expectedValidateCalls - Changes according to parameters
     *
     * @sample Base1 - purchase1, null, false, 1
     */
    private fun onNewPurchaseComplete() = Stream.of(
        Arguments.of(purchase1, null, false, 1),
        Arguments.of(purchase2, null, false, 1),
        Arguments.of(purchase1, arrayOf<String>(), false, 1),
        Arguments.of(purchase1, arrayOf("PurchaseToken1"), false, 0),
        Arguments.of(purchase1, null, true, 1)
    )

    private val history1: RevXPurchaseHistory by lazy {
        mockk {
            every { purchaseToken } returns "PurchaseToken1"
            every { type } returns ProductType.Subs
            every { quantity } returns 1
            every { productIds } returns listOf("SubscriptionId")
        }
    }

    private val history2: RevXPurchaseHistory by lazy {
        mockk {
            every { purchaseToken } returns "PurchaseToken2"
            every { type } returns ProductType.InApp
            every { quantity } returns 1
            every { productIds } returns listOf("InAppId")
        }
    }

    private val purchase1: RevXPurchase by lazy {
        mockk {
            every { purchaseToken } returns "PurchaseToken1"
            every { type } returns ProductType.Subs
            every { quantity } returns 1
            every { productIds } returns listOf("SubscriptionId")
        }
    }

    private val purchase2: RevXPurchase by lazy {
        mockk {
            every { purchaseToken } returns "PurchaseToken2"
            every { type } returns ProductType.InApp
            every { quantity } returns 1
            every { productIds } returns listOf("InAppId")
        }
    }

    private val product1: RevXProduct by lazy {
        mockk {
            every { productId } returns "SubscriptionId"
            every { subscriptionPeriod } returns "P1M"
            every { priceAmountMicros } returns 14990000
            every { priceCurrencyCode } returns "USD"
        }
    }

    private val product2: RevXProduct by lazy {
        mockk {
            every { productId } returns "InAppId"
            every { subscriptionPeriod } returns "P1Y"
            every { priceAmountMicros } returns 24990000
            every { priceCurrencyCode } returns "USD"
        }
    }
}
