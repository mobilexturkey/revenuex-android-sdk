package tr.com.mobilex.transactions.network

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockkObject
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import java.io.Serializable
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplTransactionsApiTests {

    private lateinit var transactionsApi: TransactionsApi
    private lateinit var lock: CountDownLatch

    @MockK
    lateinit var systemInfo: SystemInfo

    @MockK
    lateinit var httpClient: HttpClient

    @BeforeEach
    fun setUp() {
        mockLogger()
        mockSystemInfo()
        lock = CountDownLatch(1)
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockSystemInfo() {
        every { systemInfo.revenueXUserId } returns "UserId"
        every { systemInfo.rootUrl } returns "https://www.revenuex.com"
        every { systemInfo.platform } returns "android"
        every { systemInfo.platformVersion } returns "10"
        every { systemInfo.deviceName } returns "TestDevice"
        every { systemInfo.applicationVersion } returns "1.2.3"
        every { systemInfo.isSandbox } returns true
        every { systemInfo.region } returns "TR"
        every { systemInfo.language } returns "tr-TR"
    }

    @Suppress("UNCHECKED_CAST")
    private fun <T : Serializable> mockHttpClientPost(
        clientId: String,
        responseFromClient: T?,
        exceptionMessageFromClient: String?
    ) {
        every {
            httpClient.post<T>(
                any(),
                any(),
                any(),
                any(),
                any(),
                any(),
                any()
            )
        } answers {
            val headers = it.invocation.args[3] as Map<String, String>
            assertEquals(clientId, headers["clientid"], "ClientId in HTTP headers does not match")

            if (exceptionMessageFromClient != null) {
                val onFailure = it.invocation.args[4] as (Exception) -> Unit
                onFailure.invoke(Exception(exceptionMessageFromClient))
            } else {
                val onResponse = it.invocation.args[5] as (T?) -> Unit
                onResponse.invoke(responseFromClient)
            }
        }
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    @ParameterizedTest
    @MethodSource
    fun validate(
        clientId: String,
        responseFromClient: String?,
        exceptionMessageFromClient: String?,
        isExceptionExpected: Boolean,
        expectedMessage: String,
        expectedResponse: String?
    ) {
        var isFailed = isExceptionExpected.not()
        var exceptionMessage = "DefaultMessage"
        var response: String? = null

        // Mocks test state with parameters
        every { systemInfo.clientId } returns clientId
        mockHttpClientPost(clientId, responseFromClient, exceptionMessageFromClient)
        transactionsApi = ImplTransactionsApi(systemInfo, httpClient)

        val productInformation = ProductInformation("ProductId", "monthly", 14.99, "USD")
        val purchaseInformation =
            PurchaseInformation("PurchaseToken", true, listOf(productInformation))
        transactionsApi.validate(
            purchaseInfo = purchaseInformation,
            onFailure = { exception ->
                isFailed = true
                exceptionMessage = exception.localizedMessage ?: exception.toString()
                response = null
                lock.countDown()
            },
            onResponse = { apiResponse ->
                isFailed = false
                exceptionMessage = ""
                response = apiResponse
                lock.countDown()
            }
        )
        lock.await(5, TimeUnit.SECONDS)

        assertResponse(
            isFailed,
            exceptionMessage,
            response,
            isExceptionExpected,
            expectedMessage,
            expectedResponse
        )
    }

    private fun assertResponse(
        isFailed: Boolean,
        exceptionMessage: String,
        response: String?,
        isExceptionExpected: Boolean,
        expectedMessage: String,
        expectedResponse: String?
    ) {
        assertEquals(isExceptionExpected, isFailed, "isFailed must be equal to expectation")
        assertEquals(
            expectedMessage,
            exceptionMessage,
            "Exception message must match with expectation"
        )

        if (isExceptionExpected) {
            assertNull(response, "Response must be null on exception")
        } else {
            assertEquals(
                expectedResponse,
                response,
                "Response must match with expectation"
            )
        }
    }

    /**
     * Input domain for validate method
     * @param clientId - "", "ClientId1", "ClientId2"
     * @param responseFromClient - null, { "subscriptions": [], "transactions": [], "eventLogs": [] }
     * @param exceptionMessageFromClient - null, "Exception thrown from http client"
     * @param isExceptionExpected - false, true
     * @param expectedMessage - If should success, empty string. Or changes according to expected exception
     * @param expectedResponse - Changes according to parameters
     *
     * @sample Base1 - "ClientId1", { "subscriptions": [], "transactions": [], "eventLogs": [] }, null, false, "", { "subscriptions": [], "transactions": [], "eventLogs": [] }
     */
    private fun validate() = Stream.of(
        Arguments.of(
            "ClientId1",
            "{ \"subscriptions\": [], \"transactions\": [], \"eventLogs\": [] }",
            null,
            false,
            "",
            "{ \"subscriptions\": [], \"transactions\": [], \"eventLogs\": [] }"
        ),
        Arguments.of(
            "",
            "{ \"subscriptions\": [], \"transactions\": [], \"eventLogs\": [] }",
            null,
            false,
            "",
            "{ \"subscriptions\": [], \"transactions\": [], \"eventLogs\": [] }"
        ),
        Arguments.of(
            "ClientId2",
            "{ \"subscriptions\": [], \"transactions\": [], \"eventLogs\": [] }",
            null,
            false,
            "",
            "{ \"subscriptions\": [], \"transactions\": [], \"eventLogs\": [] }"
        ),
        Arguments.of("ClientId1", null, null, false, "", null),
        Arguments.of(
            "ClientId1",
            "{ \"subscriptions\": [], \"transactions\": [], \"eventLogs\": [] }",
            "Exception thrown from http client",
            true,
            "Exception thrown from http client",
            null
        )
    )
}
