package tr.com.mobilex.entitlements.manager

import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.entitlements.network.EntitlementDto
import tr.com.mobilex.entitlements.network.EntitlementsApi
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplEntitlementsManagerTests {

    private lateinit var entitlementsManager: EntitlementsManager

    private lateinit var entitlementsApi: EntitlementsApi

    @BeforeEach
    fun setUp() {
        mockLogger()
        entitlementsApi = mockk()
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockGetCurrentEntitlements(
        response: List<EntitlementDto>?,
        throwException: Boolean
    ) {
        every { entitlementsApi.getCurrentEntitlements(any(), any()) } answers {
            if (throwException) {
                val onFailure = it.invocation.args[0] as (Exception) -> Unit
                onFailure.invoke(Exception("Mocked exception message thrown from api"))
            } else {
                val onResponse = it.invocation.args[1] as (List<EntitlementDto>?) -> Unit
                onResponse.invoke(response)
            }
        }
    }

    @ParameterizedTest
    @MethodSource
    fun fetchEntitlements(
        entitlementsFromApi: List<EntitlementDto>?,
        throwException: Boolean,
        expectedEntitlements: Array<String>
    ) {
        var entitlements: Map<String, Boolean>? = null

        // Mocks test state with parameters
        mockGetCurrentEntitlements(entitlementsFromApi, throwException)

        entitlementsManager = ImplEntitlementsManager(entitlementsApi)
        entitlementsManager.fetchEntitlements {
            entitlements = entitlementsManager.entitlements
        }

        verify(exactly = 1) { entitlementsApi.getCurrentEntitlements(any(), any()) }
        assertResults(expectedEntitlements, entitlements)
    }

    private fun assertResults(
        expectedEntitlements: Array<String>,
        entitlements: Map<String, Boolean>?
    ) {
        assertTrue(entitlements != null, "Entitlements is null")
        assertEquals(
            expectedEntitlements.size,
            entitlements!!.size,
            "Entitlements sizes not same"
        )

        expectedEntitlements.forEach { entitlementId ->
            val isEntitled = entitlements[entitlementId] ?: false
            assertTrue(isEntitled, "Entitlements not equal")
        }
    }

    @ParameterizedTest
    @MethodSource
    fun isEntitled(
        entitlementIdentifier: String,
        expectedStatus: Boolean
    ) {
        // Mocks test state with parameters
        mockGetCurrentEntitlements(multipleEntitlements, false)

        entitlementsManager = ImplEntitlementsManager(entitlementsApi)
        entitlementsManager.fetchEntitlements()

        val status = entitlementsManager.isEntitled(entitlementIdentifier)

        verify(exactly = 1) { entitlementsApi.getCurrentEntitlements(any(), any()) }
        assertEquals(expectedStatus, status, "Status does not match")
    }

    /**
     * Input domain for fetchEntitlements method
     * @param entitlementsFromApi - null, empty, singleEntitlement, multipleEntitlements, withNullValues
     * @param throwException - false, true
     * @param expectedEntitlements - Changes according to parameters
     *
     * @sample Base1 - singleEntitlement, false, singleExpectation
     */
    private fun fetchEntitlements() = Stream.of(
        Arguments.of(singleEntitlement, false, singleExpectation),
        Arguments.of(null, false, emptyExpectation),
        Arguments.of(empty, false, emptyExpectation),
        Arguments.of(multipleEntitlements, false, multipleExpectation),
        Arguments.of(withNullValues, false, emptyExpectation),
        Arguments.of(singleEntitlement, true, emptyExpectation)
    )

    /**
     * Input domain for isEntitled method
     * @param entitlementIdentifier - "", "com.mobilex.revenuex.silver", "com.mobilex.revenuex.gold", "com.mobilex.revenuex.invalid"
     * @param expectedStatus - Changes according to parameters
     *
     * @sample Base1 - "com.mobilex.revenuex.silver", true
     */
    private fun isEntitled() = Stream.of(
        Arguments.of("com.mobilex.revenuex.silver", true),
        Arguments.of("", false),
        Arguments.of("com.mobilex.revenuex.gold", true),
        Arguments.of("com.mobilex.revenuex.invalid", false)
    )

    private val empty: List<EntitlementDto> = listOf()

    private val singleEntitlement: List<EntitlementDto> = listOf(
        EntitlementDto(
            id = "123",
            identifier = "com.mobilex.revenuex.silver",
            description = "Silver entitlement description",
            products = listOf("com.mobilex.weekly.silver", "com.mobilex.monthly.silver")
        )
    )

    private val multipleEntitlements: List<EntitlementDto> = listOf(
        EntitlementDto(
            id = "123",
            identifier = "com.mobilex.revenuex.silver",
            description = "Silver entitlement description",
            products = listOf("com.mobilex.weekly.silver", "com.mobilex.monthly.silver")
        ),
        EntitlementDto(
            id = "345",
            identifier = "com.mobilex.revenuex.gold",
            description = "Gold entitlement description",
            products = listOf("com.mobilex.weekly.gold", "com.mobilex.monthly.gold")
        )
    )

    private val withNullValues: List<EntitlementDto> = listOf(
        EntitlementDto(
            id = "345",
            identifier = null,
            description = "Gold entitlement description"
        )
    )

    private val emptyExpectation: Array<String> = arrayOf()

    private val singleExpectation: Array<String> = arrayOf("com.mobilex.revenuex.silver")

    private val multipleExpectation: Array<String> = arrayOf(
        "com.mobilex.revenuex.silver",
        "com.mobilex.revenuex.gold"
    )
}
