package tr.com.mobilex.entitlements.network

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockkObject
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import tr.com.mobilex.core.utils.typeToken
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplEntitlementsApiTests {

    private lateinit var entitlementsApi: EntitlementsApi
    private lateinit var lock: CountDownLatch

    @MockK
    lateinit var systemInfo: SystemInfo

    @MockK
    lateinit var httpClient: HttpClient

    @BeforeEach
    fun setUp() {
        mockLogger()
        mockSystemInfo()
        lock = CountDownLatch(1)
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockSystemInfo() {
        every { systemInfo.revenueXUserId } returns "UserId"
        every { systemInfo.rootUrl } returns "https://www.revenuex.com"
        every { systemInfo.platform } returns "android"
        every { systemInfo.platformVersion } returns "10"
        every { systemInfo.deviceName } returns "TestDevice"
        every { systemInfo.applicationVersion } returns "1.2.3"
        every { systemInfo.isSandbox } returns true
        every { systemInfo.region } returns "TR"
        every { systemInfo.language } returns "tr-TR"
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockHttpClientGet(
        clientId: String,
        responseFromClient: ArrayList<EntitlementDto>?,
        exceptionFromClient: String?
    ) {
        every {
            httpClient.get(
                any(),
                any(),
                any(),
                any(),
                any(),
                typeToken<ArrayList<EntitlementDto>>()
            )
        } answers {
            val headers = it.invocation.args[2] as Map<String, String>
            assertEquals(
                clientId,
                headers["clientid"],
                "ClientId in HTTP headers does not match"
            )

            if (exceptionFromClient != null) {
                val onFailure = it.invocation.args[3] as (Exception) -> Unit
                onFailure.invoke(Exception(exceptionFromClient))
            } else {
                val onResponse = it.invocation.args[4] as (ArrayList<EntitlementDto>?) -> Unit
                onResponse.invoke(responseFromClient)
            }
        }
    }

    @ParameterizedTest
    @MethodSource
    fun getCurrentEntitlements(
        clientId: String,
        entitlementsFromClient: ArrayList<EntitlementDto>?,
        exceptionFromClient: String?,
        expectedMessage: String,
        expectedEntitlements: ArrayList<EntitlementDto>?
    ) {
        var isFailed = expectedMessage.isBlank().not()
        var exceptionMessage = "DefaultMessage"
        var entitlements: List<EntitlementDto>? = null

        // Mocks test state with parameters
        every { systemInfo.clientId } returns clientId
        mockHttpClientGet(clientId, entitlementsFromClient, exceptionFromClient)
        entitlementsApi = ImplEntitlementsApi(systemInfo, httpClient)

        entitlementsApi.getCurrentEntitlements({ exception ->
            isFailed = true
            exceptionMessage = exception.localizedMessage ?: exception.toString()
            entitlements = null
            lock.countDown()
        }, { response ->
            isFailed = false
            exceptionMessage = ""
            entitlements = response
            lock.countDown()
        })
        lock.await(5, TimeUnit.SECONDS)

        assertTestResults(
            isFailed,
            exceptionMessage,
            entitlements,
            expectedMessage,
            expectedEntitlements
        )
    }

    private fun assertTestResults(
        isFailed: Boolean,
        exceptionMessage: String,
        entitlements: List<EntitlementDto>?,
        expectedMessage: String,
        expectedEntitlements: List<EntitlementDto>?
    ) {
        val isExceptionExpected = expectedMessage.isBlank().not()
        assertEquals(
            isExceptionExpected,
            isFailed,
            "isFailed must be equal to expectation"
        )
        assertEquals(
            expectedMessage,
            exceptionMessage,
            "Exception message must match with expectation"
        )

        if (isExceptionExpected) {
            assertNull(entitlements, "Entitlements must be null on exception")
        } else {
            if (expectedEntitlements == null) {
                assertNull(entitlements, "Entitlements must be null")
            } else {
                assertNotNull(entitlements, "Entitlements should not be null")
                assertEquals(
                    expectedEntitlements.size,
                    entitlements!!.size,
                    "Entitlements not equal"
                )

                expectedEntitlements.forEachIndexed { index, expectedEntitlement ->
                    val entitlement = entitlements[index]
                    assertEquals(expectedEntitlement.id, entitlement.id, "Entitlements not equal")
                    assertEquals(
                        expectedEntitlement.identifier,
                        entitlement.identifier,
                        "Entitlements not equal"
                    )
                    assertEquals(
                        expectedEntitlement.description,
                        entitlement.description,
                        "Entitlements not equal"
                    )
                    assertEquals(
                        expectedEntitlement.products.size,
                        entitlement.products.size,
                        "Entitlements not equal"
                    )
                }
            }
        }
    }

    /**
     * Input domain for getCurrentEntitlements method
     * @param clientId - "", "ClientId1", "ClientId2"
     * @param entitlementsFromClient - null, empty, singleEntitlement, multipleEntitlements, withNullValues
     * @param exceptionFromClient - null, "Exception from http client"
     * @param expectedMessage - If should success, empty string. Or changes according to expected exception
     * @param expectedEntitlements - Changes according to parameters
     *
     * @sample Base1 - "ClientId1", singleEntitlement, null, "", singleEntitlement
     */
    private fun getCurrentEntitlements() = Stream.of(
        Arguments.of("ClientId1", singleEntitlement, null, "", singleEntitlement),
        Arguments.of("", singleEntitlement, null, "", singleEntitlement),
        Arguments.of("ClientId2", singleEntitlement, null, "", singleEntitlement),
        Arguments.of("ClientId1", null, null, "", null),
        Arguments.of("ClientId1", empty, null, "", empty),
        Arguments.of("ClientId1", multipleEntitlements, null, "", multipleEntitlements),
        Arguments.of("ClientId1", withNullValues, null, "", withNullValues),
        Arguments.of(
            "ClientId1",
            singleEntitlement,
            "Exception from http client",
            "Exception from http client",
            null
        )
    )

    private val empty: ArrayList<EntitlementDto> = arrayListOf()

    private val singleEntitlement: ArrayList<EntitlementDto> = arrayListOf(
        EntitlementDto(
            id = "123",
            identifier = "com.mobilex.revenuex.silver",
            description = "Silver entitlement description",
            products = listOf("com.mobilex.weekly.silver", "com.mobilex.monthly.silver")
        )
    )

    private val multipleEntitlements: ArrayList<EntitlementDto> = arrayListOf(
        EntitlementDto(
            id = "123",
            identifier = "com.mobilex.revenuex.silver",
            description = "Silver entitlement description",
            products = listOf("com.mobilex.weekly.silver", "com.mobilex.monthly.silver")
        ),
        EntitlementDto(
            id = "345",
            identifier = "com.mobilex.revenuex.gold",
            description = "Gold entitlement description",
            products = listOf("com.mobilex.weekly.gold", "com.mobilex.monthly.gold")
        )
    )

    private val withNullValues: ArrayList<EntitlementDto> = arrayListOf(
        EntitlementDto(
            id = "345",
            identifier = null,
            description = "Gold entitlement description"
        )
    )
}
