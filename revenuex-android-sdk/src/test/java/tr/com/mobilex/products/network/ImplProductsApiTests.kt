package tr.com.mobilex.products.network

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockkObject
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplProductsApiTests {

    private lateinit var productsApi: ProductsApi
    private lateinit var lock: CountDownLatch

    @MockK
    lateinit var systemInfo: SystemInfo

    @MockK
    lateinit var httpClient: HttpClient

    @BeforeEach
    fun setUp() {
        mockLogger()
        mockSystemInfo()
        lock = CountDownLatch(1)
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockSystemInfo() {
        every { systemInfo.revenueXUserId } returns "UserId"
        every { systemInfo.rootUrl } returns "https://www.revenuex.com"
        every { systemInfo.platform } returns "android"
        every { systemInfo.platformVersion } returns "10"
        every { systemInfo.deviceName } returns "TestDevice"
        every { systemInfo.applicationVersion } returns "1.2.3"
        every { systemInfo.isSandbox } returns true
        every { systemInfo.region } returns "TR"
        every { systemInfo.language } returns "tr-TR"
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockHttpClientGet(
        clientId: String,
        responseFromClient: Array<ProductInfoDto>?,
        exceptionMessageFromClient: String?
    ) {
        every {
            httpClient.get(
                any(),
                any(),
                any(),
                any(),
                any(),
                Array<ProductInfoDto>::class.java
            )
        } answers {
            val headers = it.invocation.args[2] as Map<String, String>
            assertEquals(clientId, headers["clientid"], "ClientId in HTTP headers does not match")

            if (exceptionMessageFromClient != null) {
                val onFailure = it.invocation.args[3] as (Exception) -> Unit
                onFailure.invoke(Exception(exceptionMessageFromClient))
            } else {
                val onResponse = it.invocation.args[4] as (Array<ProductInfoDto>?) -> Unit
                onResponse.invoke(responseFromClient)
            }
        }
    }

    @ParameterizedTest
    @MethodSource
    fun getAll(
        clientId: String,
        productsArrayFromClient: Array<ProductInfoDto>?,
        exceptionMessageFromClient: String?,
        isExceptionExpected: Boolean,
        expectedMessage: String,
        expectedProductsArray: Array<ProductInfoDto>?
    ) {
        var isFailed = isExceptionExpected.not()
        var exceptionMessage = "DefaultMessage"
        var productsArray: Array<ProductInfoDto>? = null

        // Mocks test state with parameters
        every { systemInfo.clientId } returns clientId
        mockHttpClientGet(clientId, productsArrayFromClient, exceptionMessageFromClient)
        productsApi = ImplProductsApi(systemInfo, httpClient)

        productsApi.getAll({ exception ->
            isFailed = true
            exceptionMessage = exception.localizedMessage ?: exception.toString()
            productsArray = null
            lock.countDown()
        }, { response ->
            isFailed = false
            exceptionMessage = ""
            productsArray = response
            lock.countDown()
        })
        lock.await(5, TimeUnit.SECONDS)

        assertTestResults(
            isFailed,
            exceptionMessage,
            productsArray,
            isExceptionExpected,
            expectedMessage,
            expectedProductsArray
        )
    }

    private fun assertTestResults(
        isFailed: Boolean,
        exceptionMessage: String,
        productsArray: Array<ProductInfoDto>?,
        isExceptionExpected: Boolean,
        expectedMessage: String,
        expectedProductsArray: Array<ProductInfoDto>?
    ) {
        assertEquals(isExceptionExpected, isFailed, "isFailed must be equal to expectation")
        assertEquals(
            expectedMessage,
            exceptionMessage,
            "Exception message must match with expectation"
        )

        if (isExceptionExpected) {
            assertNull(productsArray, "Product array must be null on exception")
        } else {
            if (expectedProductsArray == null) {
                assertNull(productsArray, "Product array must be null")
            } else {
                assertEquals(
                    productsArray!!.count(),
                    expectedProductsArray.count(),
                    "Product size must be equal to expectation"
                )

                expectedProductsArray.forEach { expectedProduct ->
                    val product = productsArray.first { it.id == expectedProduct.id }
                    assertProduct(product, expectedProduct)
                }
            }
        }
    }

    private fun assertProduct(product: ProductInfoDto, expectedProduct: ProductInfoDto) {
        assertEquals(
            product.id,
            expectedProduct.id,
            "Product must be equal to expectation"
        )
        assertEquals(
            product.productId,
            expectedProduct.productId,
            "Product must be equal to expectation"
        )
        assertEquals(
            product.platform,
            expectedProduct.platform,
            "Product must be equal to expectation"
        )
        assertEquals(
            product.application,
            expectedProduct.application,
            "Product must be equal to expectation"
        )
    }

    /**
     * Input domain for getAll method
     * @param clientId - "", "ClientId1", "ClientId2"
     * @param productsArrayFromClient - null, empty, oneProduct, twoProductsAnd, twoProductsMix, oneProductIos
     * @param exceptionMessageFromClient - null, "Exception from http client"
     * @param isExceptionExpected - false, true
     * @param expectedMessage - If should success, empty string. Or changes according to expected exception
     * @param expectedProductsArray - Changes according to parameters
     *
     * @sample Base1 - "ClientId1", oneProduct, null, false, "", oneProduct
     */
    private fun getAll() = Stream.of(
        Arguments.of("ClientId1", oneProduct, null, false, "", oneProduct),
        Arguments.of("", oneProduct, null, false, "", oneProduct),
        Arguments.of("ClientId2", oneProduct, null, false, "", oneProduct),
        Arguments.of("ClientId1", null, null, false, "", null),
        Arguments.of("ClientId1", empty, null, false, "", empty),
        Arguments.of("ClientId1", twoProductsAnd, null, false, "", twoProductsAnd),
        Arguments.of("ClientId1", twoProductsMix, null, false, "", twoProductsMix),
        Arguments.of("ClientId1", oneProductIos, null, false, "", oneProductIos),
        Arguments.of("ClientId1", oneProduct, "Exception from http client", true, "Exception from http client", oneProduct)
    )

    /**
     * Test data as empty product list
     */
    private val empty: Array<ProductInfoDto> = arrayOf()

    /**
     * Test data as product list with single android product
     */
    private val oneProduct: Array<ProductInfoDto> = arrayOf(
        ProductInfoDto("product1", "test_monthly_subscription", "android", "application1")
    )

    /**
     * Test data as product list with two android products
     */
    private val twoProductsAnd: Array<ProductInfoDto> = arrayOf(
        ProductInfoDto("product1", "test_monthly_subscription", "android", "application1"),
        ProductInfoDto("product2", "test_yearly_subscription", "android", "application2")
    )

    /**
     * Test data as product list with two products from different platforms
     */
    private val twoProductsMix: Array<ProductInfoDto> = arrayOf(
        ProductInfoDto("product1", "test_monthly_subscription", "android", "application1"),
        ProductInfoDto("product3", "test_monthly_subscription", "ios", "application1")
    )

    /**
     * Test data as product list with single ios product
     */
    private val oneProductIos: Array<ProductInfoDto> = arrayOf(
        ProductInfoDto("product3", "test_monthly_subscription", "ios", "application1")
    )
}
