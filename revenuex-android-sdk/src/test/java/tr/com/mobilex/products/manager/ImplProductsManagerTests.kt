package tr.com.mobilex.products.manager

import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.InstantTaskExecutorExtension
import tr.com.mobilex.core.billing.BillingClientManager
import tr.com.mobilex.core.billing.ProductType
import tr.com.mobilex.core.billing.RevXProduct
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.products.network.ProductInfoDto
import tr.com.mobilex.products.network.ProductsApi
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplProductsManagerTests {

    private lateinit var productsManager: ProductsManager

    private lateinit var productsApi: ProductsApi

    private lateinit var billingClient: BillingClientManager

    @BeforeEach
    fun setUp() {
        mockLogger()
        productsApi = mockk()
        billingClient = mockk()
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockGetAll(
        response: Array<ProductInfoDto>?,
        throwException: Boolean
    ) {
        every { productsApi.getAll(any(), any()) } answers {
            if (throwException) {
                val onFailure = it.invocation.args[0] as (Exception) -> Unit
                onFailure.invoke(Exception("Mocked exception message thrown from api"))
            } else {
                val onResponse = it.invocation.args[1] as (Array<ProductInfoDto>?) -> Unit
                onResponse.invoke(response)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockQueryProductDetails(subList: List<RevXProduct>, inAppList: List<RevXProduct>) {
        every { billingClient.queryProductDetails(any(), ProductType.Subs, any()) } answers {
            val callback = it.invocation.args[2] as ((List<RevXProduct>) -> Unit)?
            callback?.invoke(subList)
        }
        every { billingClient.queryProductDetails(any(), ProductType.InApp, any()) } answers {
            val callback = it.invocation.args[2] as ((List<RevXProduct>) -> Unit)?
            callback?.invoke(inAppList)
        }
    }

    @ParameterizedTest
    @MethodSource
    fun fetchProducts(
        productsArrayFromApi: Array<ProductInfoDto>?,
        skuListFromBilling: List<RevXProduct>?,
        throwException: Boolean,
        expectedProductsArray: Array<ProductInfoDto>,
        expectedSkuList: List<RevXProduct>
    ) {
        var productsArray: List<RevXProduct>? = null

        // Mocks test state with parameters
        mockGetAll(productsArrayFromApi, throwException)
        skuListFromBilling?.let {
            val subList = skuListFromBilling.filter { it.type == ProductType.Subs }
            val inAppList = skuListFromBilling.filter { it.type == ProductType.InApp }
            mockQueryProductDetails(subList, inAppList)
        }

        productsManager = ImplProductsManager(productsApi, billingClient)
        productsManager.fetchProducts {
            productsArray = productsManager.products
        }

        validateCalls(expectedProductsArray.isNotEmpty())
        assertTestResults(productsArray, expectedSkuList)
    }

    @ParameterizedTest
    @MethodSource
    fun getProducts(
        productsArrayFromApi: Array<ProductInfoDto>?,
        skuListFromBilling: List<RevXProduct>?,
        throwException: Boolean,
        isExceptionExpected: Boolean,
        expectedProductsArray: Array<ProductInfoDto>,
        expectedSkuList: List<RevXProduct>
    ) {
        var productsArray: List<RevXProduct>? = null

        // Mocks test state with parameters
        mockGetAll(productsArrayFromApi, throwException)
        skuListFromBilling?.let {
            val subList = skuListFromBilling.filter { it.type == ProductType.Subs }
            val inAppList = skuListFromBilling.filter { it.type == ProductType.InApp }
            mockQueryProductDetails(subList, inAppList)
        }

        productsManager = ImplProductsManager(productsApi, billingClient)
        productsManager.fetchProducts()

        productsManager.getProducts(ProductType.Subs) { response ->
            productsArray = response
        }

        // Assertions
        validateCalls(expectedProductsArray.isNotEmpty())
        assertTestResults(
            productsArray,
            expectedSkuList
        )
    }

    private fun validateCalls(shouldBillingCalled: Boolean) {
        verify(atMost = 1) { productsApi.getAll(any(), any()) }

        if (shouldBillingCalled) {
            verify(exactly = 2) { billingClient.queryProductDetails(any(), any(), any()) }
        } else {
            verify(exactly = 0) { billingClient.queryProductDetails(any(), any(), any()) }
        }
    }

    private fun assertTestResults(
        productsList: List<RevXProduct>?,
        expectedProductsList: List<RevXProduct>
    ) {
        assertProductsArray(
            productsList = productsList,
            expectedProductsList = expectedProductsList
        )
        assertProductsArray(
            productsList = productsManager.products,
            expectedProductsList = expectedProductsList
        )
    }

    private fun assertProductsArray(
        productsList: List<RevXProduct>?,
        expectedProductsList: List<RevXProduct>
    ) {
        assertNotNull(productsList, "Product array must be not null")
        assertEquals(
            expectedProductsList.count(),
            productsList!!.count(),
            "Product size must be equal to expectation"
        )

        expectedProductsList.forEach { expectedProduct ->
            val product = productsList.first { it.productId == expectedProduct.productId }
            assertProduct(product, expectedProduct)
        }
    }

    private fun assertProduct(product: RevXProduct, expectedProduct: RevXProduct) {
        assertEquals(
            product.productId,
            expectedProduct.productId,
            "Product must be equal to expectation"
        )
        assertEquals(
            product.title,
            expectedProduct.title,
            "Product must be equal to expectation"
        )
        assertEquals(
            product.price,
            expectedProduct.price,
            "Product must be equal to expectation"
        )
        assertEquals(
            product.priceAmountMicros,
            expectedProduct.priceAmountMicros,
            "Product must be equal to expectation"
        )
        assertEquals(
            product.priceCurrencyCode,
            expectedProduct.priceCurrencyCode,
            "Product must be equal to expectation"
        )
        assertEquals(
            product.type,
            expectedProduct.type,
            "Product must be equal to expectation"
        )
    }

    /**
     * Input domain for fetchProducts method
     * @param productsArrayFromApi - null, empty, oneProduct, twoProductsAnd, twoProductsMix, oneProductIos
     * @param skuListFromBilling - null, oneSkuDetails, twoSkuDetails
     * @param throwException - false, true
     * @param expectedProductsArray - Changes according to parameters
     * @param expectedSkuList - Changes according to parameters
     *
     * @sample Base1 - oneProduct, false, oneProduct
     */
    private fun fetchProducts() = Stream.of(
        Arguments.of(oneProduct, oneSkuDetails, false, oneProduct, oneSkuDetails),
        Arguments.of(null, null, false, empty, emptySkuDetails),
        Arguments.of(empty, null, false, empty, emptySkuDetails),
        Arguments.of(twoProductsAnd, twoSkuDetails, false, twoProductsAnd, twoSkuDetails),
        Arguments.of(twoProductsMix, oneSkuDetails, false, oneProduct, oneSkuDetails),
        Arguments.of(oneProductIos, null, false, empty, emptySkuDetails),
        Arguments.of(oneProduct, null, true, empty, emptySkuDetails)
    )

    /**
     * Input domain for getAll method
     * @param productsArrayFromApi - null, empty, oneProduct, twoProductsAnd, twoProductsMix, oneProductIos
     * @param skuListFromBilling - null, oneSkuDetails, twoSkuDetails
     * @param throwException - false, true
     * @param isExceptionExpected - false, true
     * @param expectedProductsArray - Changes according to parameters
     * @param expectedSkuList - Changes according to parameters
     *
     * @sample Base1 - oneProduct, false, false, oneProduct
     */
    private fun getProducts() = Stream.of(
        Arguments.of(oneProduct, oneSkuDetails, false, false, oneProduct, oneSkuDetails),
        Arguments.of(null, null, false, false, empty, emptySkuDetails),
        Arguments.of(empty, null, false, false, empty, emptySkuDetails),
        Arguments.of(twoProductsAnd, twoSkuDetails, false, false, twoProductsAnd, twoSkuDetails),
        Arguments.of(twoProductsMix, oneSkuDetails, false, false, oneProduct, oneSkuDetails),
        Arguments.of(oneProductIos, null, false, false, empty, emptySkuDetails),
        Arguments.of(oneProduct, null, true, true, empty, emptySkuDetails)
    )

    /**
     * Test data as empty product list
     */
    private val empty: Array<ProductInfoDto> = arrayOf()

    /**
     * Test data as product list with single android product
     */
    private val oneProduct: Array<ProductInfoDto> = arrayOf(
        ProductInfoDto("product1", "test_monthly_subscription", "android", "application1")
    )

    /**
     * Test data as product list with two android products
     */
    private val twoProductsAnd: Array<ProductInfoDto> = arrayOf(
        ProductInfoDto("product1", "test_monthly_subscription", "android", "application1"),
        ProductInfoDto("product2", "test_yearly_subscription", "android", "application2")
    )

    /**
     * Test data as product list with two products from different platforms
     */
    private val twoProductsMix: Array<ProductInfoDto> = arrayOf(
        ProductInfoDto("product1", "test_monthly_subscription", "android", "application1"),
        ProductInfoDto("product3", "test_monthly_subscription", "ios", "application1")
    )

    /**
     * Test data as product list with single ios product
     */
    private val oneProductIos: Array<ProductInfoDto> = arrayOf(
        ProductInfoDto("product3", "test_monthly_subscription", "ios", "application1")
    )

    private val emptySkuDetails: List<RevXProduct> = listOf()

    private val oneSkuDetails: List<RevXProduct> = listOf(
        mockk {
            every { productId } returns "product1"
            every { title } returns "Product 1"
            every { description } returns "Product 1 Desc"
            every { price } returns "15$"
            every { priceAmountMicros } returns 15000000
            every { priceCurrencyCode } returns "USD"
            every { subscriptionPeriod } returns "P1M"
            every { type } returns ProductType.Subs
        }
    )

    private val twoSkuDetails: List<RevXProduct> = listOf(
        mockk {
            every { productId } returns "product1"
            every { title } returns "Product 1"
            every { description } returns "Product 1 Desc"
            every { price } returns "15$"
            every { priceAmountMicros } returns 15000000
            every { priceCurrencyCode } returns "USD"
            every { subscriptionPeriod } returns "P1M"
            every { type } returns ProductType.Subs
        },
        mockk {
            every { productId } returns "product2"
            every { title } returns "Product 2"
            every { description } returns "Product 2 Desc"
            every { price } returns "24,99$"
            every { priceAmountMicros } returns 24990000
            every { priceCurrencyCode } returns "USD"
            every { subscriptionPeriod } returns "P1Y"
            every { type } returns ProductType.Subs
        }
    )
}
