package tr.com.mobilex.config.network

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockkObject
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import tr.com.mobilex.core.utils.typeToken
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplRemoteConfigApiTests {

    private lateinit var configApi: RemoteConfigApi
    private lateinit var lock: CountDownLatch

    @MockK
    lateinit var systemInfo: SystemInfo

    @MockK
    lateinit var httpClient: HttpClient

    @BeforeEach
    fun setUp() {
        mockLogger()
        mockSystemInfo()
        lock = CountDownLatch(1)
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockSystemInfo() {
        every { systemInfo.revenueXUserId } returns "UserId"
        every { systemInfo.rootUrl } returns "https://www.revenuex.com"
        every { systemInfo.platform } returns "android"
        every { systemInfo.platformVersion } returns "10"
        every { systemInfo.deviceName } returns "TestDevice"
        every { systemInfo.applicationVersion } returns "1.2.3"
        every { systemInfo.isSandbox } returns true
        every { systemInfo.region } returns "TR"
        every { systemInfo.language } returns "tr-TR"
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockHttpClientGet(
        clientId: String,
        responseFromClient: HashMap<String, Any?>?,
        exceptionFromClient: String?
    ) {
        every {
            httpClient.get(
                any(),
                any(),
                any(),
                any(),
                any(),
                typeToken<HashMap<String, Any?>>()
            )
        } answers {
            val headers = it.invocation.args[2] as Map<String, String>
            assertEquals(clientId, headers["clientid"], "ClientId in HTTP headers does not match")

            if (exceptionFromClient != null) {
                val onFailure = it.invocation.args[3] as (Exception) -> Unit
                onFailure.invoke(Exception(exceptionFromClient))
            } else {
                val onResponse = it.invocation.args[4] as (HashMap<String, Any?>?) -> Unit
                onResponse.invoke(responseFromClient)
            }
        }
    }

    @ParameterizedTest
    @MethodSource
    fun getRemoteConfigs(
        clientId: String,
        configsFromClient: HashMap<String, Any?>?,
        exceptionFromClient: String?,
        expectedMessage: String,
        expectedConfigs: HashMap<String, Any?>?
    ) {
        var isFailed = expectedMessage.isBlank().not()
        var exceptionMessage = "DefaultMessage"
        var remoteConfigs: Map<String, Any?>? = null

        // Mocks test state with parameters
        every { systemInfo.clientId } returns clientId
        mockHttpClientGet(clientId, configsFromClient, exceptionFromClient)
        configApi = ImplRemoteConfigApi(systemInfo, httpClient)

        configApi.getRemoteConfigs({ exception ->
            isFailed = true
            exceptionMessage = exception.localizedMessage ?: exception.toString()
            remoteConfigs = null
            lock.countDown()
        }, { response ->
            isFailed = false
            exceptionMessage = ""
            remoteConfigs = response
            lock.countDown()
        })
        lock.await(5, TimeUnit.SECONDS)

        assertTestResults(
            isFailed,
            exceptionMessage,
            remoteConfigs,
            expectedMessage,
            expectedConfigs
        )
    }

    private fun assertTestResults(
        isFailed: Boolean,
        exceptionMessage: String,
        remoteConfigs: Map<String, Any?>?,
        expectedMessage: String,
        expectedConfigs: java.util.HashMap<String, Any?>?
    ) {
        val isExceptionExpected = expectedMessage.isBlank().not()
        assertEquals(
            isExceptionExpected,
            isFailed,
            "isFailed must be equal to expectation"
        )
        assertEquals(
            expectedMessage,
            exceptionMessage,
            "Exception message must match with expectation"
        )

        if (isExceptionExpected) {
            assertNull(remoteConfigs, "Configs must be null on exception")
        } else {
            if (expectedConfigs == null) {
                assertNull(remoteConfigs, "Configs must be null")
            } else {
                assertNotNull(remoteConfigs, "Configs should not be null")
                assertEquals(expectedConfigs.size, remoteConfigs!!.size, "Configs not equal")
                expectedConfigs.forEach { expectedConfig ->
                    val configValue = remoteConfigs[expectedConfig.key]
                    assertEquals(expectedConfig.value, configValue, "Configs not equal")
                }
            }
        }
    }

    /**
     * Input domain for getRemoteConfigs method
     * @param clientId - "", "ClientId1", "ClientId2"
     * @param configsFromClient - null, empty, singleValue, multipleValue, withNullValue
     * @param exceptionFromClient - null, "Exception from http client"
     * @param expectedMessage - If should success, empty string. Or changes according to expected exception
     * @param expectedConfigs - Changes according to parameters
     *
     * @sample Base1 - "ClientId1", singleValue, null, "", singleValue
     */
    private fun getRemoteConfigs() = Stream.of(
        Arguments.of("ClientId1", singleValue, null, "", singleValue),
        Arguments.of("", singleValue, null, "", singleValue),
        Arguments.of("ClientId2", singleValue, null, "", singleValue),
        Arguments.of("ClientId1", null, null, "", null),
        Arguments.of("ClientId1", empty, null, "", empty),
        Arguments.of("ClientId1", multipleValue, null, "", multipleValue),
        Arguments.of("ClientId1", withNullValue, null, "", withNullValue),
        Arguments.of(
            "ClientId1",
            singleValue,
            "Exception from http client",
            "Exception from http client",
            null
        )
    )

    private val empty: HashMap<String, Any?> = hashMapOf()

    private val singleValue: HashMap<String, Any?> = hashMapOf("app_name" to "RevenueX")

    private val multipleValue: HashMap<String, Any?> = hashMapOf(
        "app_name" to "RevenueX",
        "package_name" to "tr.com.mobilex",
        "version" to 3,
        "isPublished" to false
    )

    private val withNullValue: HashMap<String, Any?> = hashMapOf(
        "app_name" to "Momo",
        "package_name" to "tr.com.mobilex",
        "version" to 5,
        "isPublished" to true,
        "email" to null as String?
    )
}
