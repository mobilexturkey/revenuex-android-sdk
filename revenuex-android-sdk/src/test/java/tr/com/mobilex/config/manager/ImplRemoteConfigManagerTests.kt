package tr.com.mobilex.config.manager

import android.content.Context
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.InstantTaskExecutorExtension
import tr.com.mobilex.config.network.RemoteConfigApi
import tr.com.mobilex.core.coroutine.DispatcherProvider
import tr.com.mobilex.core.logging.Logger
import java.util.stream.Stream

@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplRemoteConfigManagerTests {

    private lateinit var configManager: RemoteConfigManager

    private lateinit var configApi: RemoteConfigApi

    private val testDispatcher = TestCoroutineDispatcher()

    @MockK
    lateinit var context: Context

    @BeforeEach
    fun setUp() {
        mockLogger()
        mockDispatcherProvider()
        configApi = mockk()
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockDispatcherProvider() {
        mockkObject(DispatcherProvider)
        every { DispatcherProvider.Default } returns testDispatcher
        every { DispatcherProvider.IO } returns testDispatcher
        every { DispatcherProvider.Main } returns testDispatcher
        every { DispatcherProvider.Unconfined } returns testDispatcher
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockGetOfferings(
        response: HashMap<String, Any?>?,
        throwException: Boolean
    ) {
        every { configApi.getRemoteConfigs(any(), any()) } answers {
            if (throwException) {
                val onFailure = it.invocation.args[0] as (Exception) -> Unit
                onFailure.invoke(Exception("Mocked exception message thrown from api"))
            } else {
                val onResponse = it.invocation.args[1] as (HashMap<String, Any?>?) -> Unit
                onResponse.invoke(response)
            }
        }
    }

    @ParameterizedTest
    @MethodSource
    fun fetchRemoteConfigs(
        configFromApi: HashMap<String, Any?>?,
        throwException: Boolean,
        expectedConfigs: HashMap<String, Any?>
    ) {
        var remoteConfigs: Map<String, Any?>? = null

        // Mocks test state with parameters
        mockGetOfferings(configFromApi, throwException)

        configManager = ImplRemoteConfigManager(configApi, context)
        configManager.fetchRemoteConfigs {
            remoteConfigs = configManager.remoteConfigs
        }

        verify(exactly = 1) { configApi.getRemoteConfigs(any(), any()) }
        assertResults(expectedConfigs, remoteConfigs)
    }

    private fun assertResults(
        expectedConfigs: HashMap<String, Any?>,
        remoteConfigs: Map<String, Any?>?
    ) {
        assertTrue(remoteConfigs != null, "Configs is null")
        assertEquals(expectedConfigs.size, remoteConfigs!!.size, "Config sizes not same")

        expectedConfigs.forEach { expectedConfig ->
            val configValue = remoteConfigs[expectedConfig.key]
            assertEquals(expectedConfig.value, configValue, "Configs not equal")
        }
    }

    @ParameterizedTest
    @MethodSource
    fun getRemoteConfig(
        key: String,
        defaultValue: Any?,
        expectedValue: Any?
    ) {
        // Mocks test state with parameters
        mockGetOfferings(withNullValue, false)

        configManager = ImplRemoteConfigManager(configApi, context)
        configManager.fetchRemoteConfigs()

        val configValue = configManager.getRemoteConfig(key, defaultValue)

        assertEquals(expectedValue, configValue, "Config value is not same")
        if (expectedValue != null && configValue != null)
            assertEquals(expectedValue::class, configValue::class, "Types are not same")
    }

    @Test
    fun getRemoteConfig_ReturnsDefaultOnWrongType() {
        val key = "app_name"
        val defaultValue = 5
        val expectedValue = 5

        // Mocks test state with parameters
        mockGetOfferings(withNullValue, false)

        configManager = ImplRemoteConfigManager(configApi, context)
        configManager.fetchRemoteConfigs()

        // app_name is remote config of a String type
        val configValue = configManager.getRemoteConfig(key, defaultValue)

        assertEquals(expectedValue, configValue, "Config value is not same")
        if (configValue != null)
            assertEquals(expectedValue::class, configValue::class, "Types are not same")
    }

    /**
     * Input domain for fetchRemoteConfigs method
     * @param configFromApi - null, empty, singleValue, multipleValue, withNullValue
     * @param throwException - false, true
     * @param expectedConfigs - Changes according to parameters
     *
     * @sample Base1 - singleValue, false, singleValue
     */
    private fun fetchRemoteConfigs() = Stream.of(
        Arguments.of(singleValue, false, singleValue),
        Arguments.of(null, false, empty),
        Arguments.of(empty, false, empty),
        Arguments.of(multipleValue, false, multipleValue),
        Arguments.of(withNullValue, false, withNullValue),
        Arguments.of(singleValue, true, empty)
    )

    /**
     * Input domain for getRemoteConfig method
     * @param key - "app_name", "package_name", "version", "isPublished", "email"
     * @param defaultValue - Changes according to key
     * @param expectedValue - Changes according to parameters
     *
     * @sample Base1 - "app_name", "Default name", "Momo"
     */
    private fun getRemoteConfig() = Stream.of(
        Arguments.of("app_name", "Default name", "Momo"),
        Arguments.of("package_name", "Default pName", "tr.com.mobilex"),
        Arguments.of("version", 7, 5),
        Arguments.of("isPublished", false, true),
        Arguments.of("email", "Default mail", "Default mail"),
        Arguments.of("email", null, null)
    )

    private val empty: HashMap<String, Any?> = hashMapOf()

    private val singleValue: HashMap<String, Any?> = hashMapOf("app_name" to "RevenueX")

    private val multipleValue: HashMap<String, Any?> = hashMapOf(
        "app_name" to "RevenueX",
        "package_name" to "tr.com.mobilex",
        "version" to 3,
        "isPublished" to false
    )

    private val withNullValue: HashMap<String, Any?> = hashMapOf(
        "app_name" to "Momo",
        "package_name" to "tr.com.mobilex",
        "version" to 5,
        "isPublished" to true,
        "email" to null as String?
    )
}
