package tr.com.mobilex.offerings.manager

import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.InstantTaskExecutorExtension
import tr.com.mobilex.core.billing.ProductType
import tr.com.mobilex.core.billing.RevXOffering
import tr.com.mobilex.core.billing.RevXPackage
import tr.com.mobilex.core.billing.RevXProduct
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.offerings.network.OfferingsApi
import tr.com.mobilex.offerings.network.OfferingsDto
import tr.com.mobilex.offerings.network.PackageDto
import tr.com.mobilex.offerings.network.ProductDto
import tr.com.mobilex.products.manager.ProductsManager
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplOfferingsManagerTests {

    private lateinit var offeringsManager: OfferingsManager

    private lateinit var offeringsApi: OfferingsApi

    private lateinit var productsManager: ProductsManager

    @BeforeEach
    fun setUp() {
        mockLogger()
        offeringsApi = mockk()
        productsManager = mockk()
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockGetOfferings(
        response: OfferingsDto?,
        throwException: Boolean
    ) {
        every { offeringsApi.getOfferings(any(), any()) } answers {
            if (throwException) {
                val onFailure = it.invocation.args[0] as (Exception) -> Unit
                onFailure.invoke(Exception("Mocked exception message thrown from api"))
            } else {
                val onResponse = it.invocation.args[1] as (OfferingsDto?) -> Unit
                onResponse.invoke(response)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockGetProducts(productList: List<RevXProduct>) {
        every { productsManager.getProducts(any<List<String>>(), any()) } answers {
            val onResponse = it.invocation.args[1] as (List<RevXProduct>) -> Unit
            onResponse.invoke(productList)
        }
    }

    @ParameterizedTest
    @MethodSource
    fun fetchOfferings(
        offeringFromApi: OfferingsDto?,
        productList: List<RevXProduct>,
        throwException: Boolean,
        isPManagerCallExpected: Boolean,
        expectedOfferings: List<RevXOffering>
    ) {
        var offerings: List<RevXOffering>? = null

        // Mocks test state with parameters
        mockGetOfferings(offeringFromApi, throwException)
        mockGetProducts(productList)

        offeringsManager = ImplOfferingsManager(offeringsApi, productsManager)
        offeringsManager.fetchOfferings {
            offerings = offeringsManager.offerings
        }

        validateCalls(isPManagerCallExpected)
        assertResults(expectedOfferings, offerings)
    }

    private fun validateCalls(isPManagerCallExpected: Boolean) {
        val getProductCallCount = if (isPManagerCallExpected) 1 else 0

        verify(exactly = 1) { offeringsApi.getOfferings(any(), any()) }
        verify(exactly = getProductCallCount) {
            productsManager.getProducts(any<List<String>>(), any())
        }
    }

    private fun assertResults(
        expectedOfferings: List<RevXOffering>,
        offerings: List<RevXOffering>?
    ) {
        assertTrue(offerings != null, "Offerings is null")
        assertEquals(expectedOfferings.size, offerings!!.size, "Offering sizes not same")
        expectedOfferings.forEachIndexed { index, expectedOffering ->
            val offering = offerings[index]
            validateOffering(expectedOffering, offering)
        }
    }

    private fun validateOffering(expectedOffering: RevXOffering, offering: RevXOffering) {
        val expectedPackages = expectedOffering.packages
        val packages = offering.packages
        assertEquals(expectedPackages.size, packages.size, "Package sizes not same")
        expectedPackages.forEachIndexed { index, expectedPackage ->
            val revXPackage = packages[index]
            validatePackage(expectedPackage, revXPackage)
        }
    }

    private fun validatePackage(expectedPackage: RevXPackage, revXPackage: RevXPackage) {
        val expectedProduct = expectedPackage.product
        val product = revXPackage.product
        assertEquals(expectedProduct == null, product == null, "Products not same")
        assertEquals(expectedProduct?.productId, product?.productId, "Products not same")
        assertEquals(expectedProduct?.title, product?.title, "Products not same")
        assertEquals(
            expectedProduct?.subscriptionPeriod,
            product?.subscriptionPeriod,
            "Products not same"
        )
        assertEquals(
            expectedProduct?.priceAmountMicros,
            product?.priceAmountMicros,
            "Products not same"
        )
    }

    /**
     * Input domain for fetchOfferings method
     * @param offeringFromApi - null, onePackOneProd, twoPackThreeProdAnd, twoPackThreeProdIos, onePackOneProdIos
     * @param productList - empty, oneProduct, twoProduct
     * @param throwException - false, true
     * @param isPManagerCallExpected - If products should be queried from manager, changes according to parameters
     * @param expectedOffering - Changes according to parameters
     *
     * @sample Base1 - onePackOneProd, twoProduct, false, true, onePackOneProdRevX
     */
    private fun fetchOfferings() = Stream.of(
        Arguments.of(onePackOneProd, twoProduct, false, true, listOf(weeklyOfferingRevX)),
        Arguments.of(null, twoProduct, false, false, listOf<RevXOffering>()),
        Arguments.of(twoPackThreeProdAnd, twoProduct, false, true, listOf(twoProductOfferingRevX)),
        Arguments.of(twoPackThreeProdIos, twoProduct, false, true, listOf(monthlyOfferingRevX)),
        Arguments.of(onePackOneProdIos, twoProduct, false, false, listOf<RevXOffering>()),
        Arguments.of(onePackOneProd, emptyProduct, false, true, listOf(emptyOfferings)),
        Arguments.of(onePackOneProd, oneProduct, false, true, listOf(weeklyOfferingRevX)),
        Arguments.of(onePackOneProd, twoProduct, true, false, listOf<RevXOffering>())
    )

    // Offering DTOs returned by api
    private val onePackOneProd by lazy {
        OfferingsDto(
            id = "123",
            identifier = "com.test.one_pack.one_prod",
            description = "Offering description",
            isCurrent = true,
            packages = listOf(weeklyPackageAnd)
        )
    }

    private val onePackOneProdIos by lazy {
        OfferingsDto(
            id = "321",
            identifier = "com.test.one_pack.one_prod_ios",
            description = "Offering description",
            isCurrent = false,
            packages = listOf(weeklyPackageIos)
        )
    }

    private val twoPackThreeProdAnd by lazy {
        OfferingsDto(
            id = "234",
            identifier = "com.test.two_pack.three_prod_and",
            description = "Offering description",
            isCurrent = true,
            packages = listOf(weeklyPackageAnd, monthlyPackage)
        )
    }

    private val twoPackThreeProdIos by lazy {
        OfferingsDto(
            id = "345",
            identifier = "com.test.two_pack.three_prod_ios",
            description = "Offering description",
            isCurrent = false,
            packages = listOf(weeklyPackageIos, monthlyPackage)
        )
    }

    // Package DTOs returned by api
    private val weeklyPackageAnd by lazy {
        PackageDto(
            id = "123PCK",
            identifier = "com.test.weekly_package_and",
            description = "Package description",
            products = listOf(androidWeekly)
        )
    }

    private val weeklyPackageIos by lazy {
        PackageDto(
            id = "321PCK",
            identifier = "com.test.weekly_package_ios",
            description = "Package description",
            products = listOf(iosWeekly)
        )
    }

    private val monthlyPackage by lazy {
        PackageDto(
            id = "234PCK",
            identifier = "com.test.monthly_package",
            description = "Package description",
            products = listOf(androidMonthly, iosMonthly)
        )
    }

    // Product DTOs returned by api
    private val androidWeekly by lazy {
        ProductDto(
            id = "123PRO",
            source = "manual",
            identifier = "com.test.android.weekly",
            platform = "android"
        )
    }

    private val iosWeekly by lazy {
        ProductDto(
            id = "321PRO",
            source = "manual",
            identifier = "com.test.ios.weekly",
            platform = "ios"
        )
    }

    private val androidMonthly by lazy {
        ProductDto(
            id = "234PRO",
            source = "manual",
            identifier = "com.test.android.monthly",
            platform = "android"
        )
    }

    private val iosMonthly by lazy {
        ProductDto(
            id = "432PRO",
            source = "manual",
            identifier = "com.test.ios.monthly",
            platform = "ios"
        )
    }

    // Products returned by product manager
    private val emptyProduct: List<RevXProduct> by lazy {
        listOf()
    }

    private val oneProduct: List<RevXProduct> by lazy {
        listOf(androidWeeklyRevX)
    }

    private val twoProduct: List<RevXProduct> by lazy {
        listOf(androidWeeklyRevX, androidMonthlyRevX)
    }

    // Products returned by product manager
    private val androidWeeklyRevX: RevXProduct by lazy {
        mockk {
            every { productId } returns "com.test.android.weekly"
            every { title } returns "Android Weekly"
            every { description } returns "Android Weekly"
            every { type } returns ProductType.Subs
            every { subscriptionPeriod } returns "P1W"
            every { price } returns "14,99$"
            every { priceAmountMicros } returns 14990000
            every { priceCurrencyCode } returns "USD"
        }
    }

    private val androidMonthlyRevX: RevXProduct by lazy {
        mockk {
            every { productId } returns "com.test.android.monthly"
            every { title } returns "Android Monthly"
            every { description } returns "Android Monthly"
            every { type } returns ProductType.Subs
            every { subscriptionPeriod } returns "P1M"
            every { price } returns "24,99$"
            every { priceAmountMicros } returns 24990000
            every { priceCurrencyCode } returns "USD"
        }
    }

    // Offerings returned by offering manager
    private val emptyOfferings by lazy {
        RevXOffering(
            id = "123",
            identifier = "com.test.offering.empty",
            description = "Offering Desc",
            isCurrent = true,
            packages = listOf()
        )
    }

    private val weeklyOfferingRevX by lazy {
        RevXOffering(
            id = "123",
            identifier = "com.test.offering.week",
            description = "Offering Desc",
            isCurrent = true,
            packages = listOf(weeklyPackageRevX)
        )
    }

    private val monthlyOfferingRevX by lazy {
        RevXOffering(
            id = "123",
            identifier = "com.test.offering.month",
            description = "Offering Desc",
            isCurrent = true,
            packages = listOf(monthlyPackageRevX)
        )
    }

    private val twoProductOfferingRevX by lazy {
        RevXOffering(
            id = "123",
            identifier = "com.test.offering.both",
            description = "Offering Desc",
            isCurrent = true,
            packages = listOf(weeklyPackageRevX, monthlyPackageRevX)
        )
    }

    // Packages returned by offering manager
    private val weeklyPackageRevX by lazy {
        RevXPackage(
            id = "123",
            identifier = "com.test.package.weekly",
            description = "Package Desc",
            product = androidWeeklyRevX
        )
    }

    private val monthlyPackageRevX by lazy {
        RevXPackage(
            id = "123",
            identifier = "com.test.package.monthly",
            description = "Package Desc",
            product = androidMonthlyRevX
        )
    }
}
