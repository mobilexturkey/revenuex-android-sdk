package tr.com.mobilex.offerings.network

import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockkObject
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.stream.Stream

@ExtendWith(MockKExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class ImplOfferingsApiTests {

    private lateinit var offeringsApi: OfferingsApi
    private lateinit var lock: CountDownLatch

    @MockK
    lateinit var systemInfo: SystemInfo

    @MockK
    lateinit var httpClient: HttpClient

    @BeforeEach
    fun setUp() {
        mockLogger()
        mockSystemInfo()
        lock = CountDownLatch(1)
    }

    @AfterEach
    fun tearDown() {
        // Called after each test
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockSystemInfo() {
        every { systemInfo.revenueXUserId } returns "UserId"
        every { systemInfo.rootUrl } returns "https://www.revenuex.com"
        every { systemInfo.platform } returns "android"
        every { systemInfo.platformVersion } returns "10"
        every { systemInfo.deviceName } returns "TestDevice"
        every { systemInfo.applicationVersion } returns "1.2.3"
        every { systemInfo.isSandbox } returns true
        every { systemInfo.region } returns "TR"
        every { systemInfo.language } returns "tr-TR"
    }

    @Suppress("UNCHECKED_CAST")
    private fun mockHttpClientGet(
        clientId: String,
        responseFromClient: OfferingsDto?,
        exceptionFromClient: String?
    ) {
        every {
            httpClient.get(
                any(),
                any(),
                any(),
                any(),
                any(),
                OfferingsDto::class.java
            )
        } answers {
            val headers = it.invocation.args[2] as Map<String, String>
            assertEquals(clientId, headers["clientid"], "ClientId in HTTP headers does not match")

            if (exceptionFromClient != null) {
                val onFailure = it.invocation.args[3] as (Exception) -> Unit
                onFailure.invoke(Exception(exceptionFromClient))
            } else {
                val onResponse = it.invocation.args[4] as (OfferingsDto?) -> Unit
                onResponse.invoke(responseFromClient)
            }
        }
    }

    @ParameterizedTest
    @MethodSource
    fun getOfferings(
        clientId: String,
        offeringFromClient: OfferingsDto?,
        exceptionFromClient: String?,
        expectedMessage: String,
        expectedOffering: OfferingsDto?
    ) {
        var isFailed = expectedMessage.isBlank().not()
        var exceptionMessage = "DefaultMessage"
        var offering: OfferingsDto? = null

        // Mocks test state with parameters
        every { systemInfo.clientId } returns clientId
        mockHttpClientGet(clientId, offeringFromClient, exceptionFromClient)
        offeringsApi = ImplOfferingsApi(systemInfo, httpClient)

        offeringsApi.getOfferings({ exception ->
            isFailed = true
            exceptionMessage = exception.localizedMessage ?: exception.toString()
            offering = null
            lock.countDown()
        }, { response ->
            isFailed = false
            exceptionMessage = ""
            offering = response
            lock.countDown()
        })
        lock.await(5, TimeUnit.SECONDS)

        assertTestResults(
            isFailed,
            exceptionMessage,
            offering,
            expectedMessage,
            expectedOffering
        )
    }

    private fun assertTestResults(
        isFailed: Boolean,
        exceptionMessage: String,
        offering: OfferingsDto?,
        expectedMessage: String,
        expectedOffering: OfferingsDto?
    ) {
        val isExceptionExpected = expectedMessage.isBlank().not()
        assertEquals(
            isExceptionExpected,
            isFailed,
            "isFailed must be equal to expectation"
        )
        assertEquals(
            expectedMessage,
            exceptionMessage,
            "Exception message must match with expectation"
        )

        if (isExceptionExpected) {
            assertNull(offering, "Offering must be null on exception")
        } else {
            if (expectedOffering == null) {
                assertNull(offering, "Offering must be null")
            } else {
                assertNotNull(offering, "Offering should not be null")
                assertOffering(expectedOffering, offering!!)
            }
        }
    }

    private fun assertOffering(expectedOffering: OfferingsDto, offering: OfferingsDto) {
        assertEquals(expectedOffering.id, offering.id, "Offering not equal")
        assertEquals(expectedOffering.identifier, offering.identifier, "Offering not equal")
        assertEquals(expectedOffering.description, offering.description, "Offering not equal")
        assertEquals(expectedOffering.isCurrent, offering.isCurrent, "Offering not equal")
        assertEquals(expectedOffering.packages.size, offering.packages.size, "Offering not equal")

        expectedOffering.packages.forEachIndexed { index, expectedPackage ->
            val packageDto = offering.packages[index]
            assertPackage(expectedPackage, packageDto)
        }
    }

    private fun assertPackage(expectedPackage: PackageDto, packageDto: PackageDto) {
        assertEquals(expectedPackage.id, packageDto.id, "Package not equal")
        assertEquals(expectedPackage.identifier, packageDto.identifier, "Package not equal")
        assertEquals(expectedPackage.description, packageDto.description, "Package not equal")
        assertEquals(expectedPackage.products.size, packageDto.products.size, "Package not equal")

        expectedPackage.products.forEachIndexed { index, expectedProduct ->
            val product = packageDto.products[index]
            assertProduct(expectedProduct, product)
        }
    }

    private fun assertProduct(expectedProduct: ProductDto, product: ProductDto) {
        assertEquals(expectedProduct.id, product.id, "Product not equal")
        assertEquals(expectedProduct.source, product.source, "Product not equal")
        assertEquals(expectedProduct.identifier, product.identifier, "Product not equal")
        assertEquals(expectedProduct.platform, product.platform, "Product not equal")
    }

    /**
     * Input domain for getOfferings method
     * @param clientId - "", "ClientId1", "ClientId2"
     * @param offeringFromClient - null, onePackOneProd, twoPackThreeProdAnd, twoPackThreeProdIos, onePackOneProdIos
     * @param exceptionFromClient - null, "Exception from http client"
     * @param expectedMessage - If should success, empty string. Or changes according to expected exception
     * @param expectedOffering - Changes according to parameters
     *
     * @sample Base1 - "ClientId1", onePackOneProd, null, "", onePackOneProd
     */
    private fun getOfferings() = Stream.of(
        Arguments.of("ClientId1", onePackOneProd, null, "", onePackOneProd),
        Arguments.of("", onePackOneProd, null, "", onePackOneProd),
        Arguments.of("ClientId2", onePackOneProd, null, "", onePackOneProd),
        Arguments.of("ClientId1", null, null, "", null),
        Arguments.of("ClientId1", twoPackThreeProdAnd, null, "", twoPackThreeProdAnd),
        Arguments.of("ClientId1", twoPackThreeProdIos, null, "", twoPackThreeProdIos),
        Arguments.of("ClientId1", onePackOneProdIos, null, "", onePackOneProdIos),
        Arguments.of(
            "ClientId1",
            onePackOneProd,
            "Exception from http client",
            "Exception from http client",
            null
        )
    )

    private val onePackOneProd by lazy {
        OfferingsDto(
            id = "123",
            identifier = "com.test.one_pack.one_prod",
            description = "Offering description",
            isCurrent = true,
            packages = listOf(weeklyPackageAnd)
        )
    }

    private val onePackOneProdIos by lazy {
        OfferingsDto(
            id = "321",
            identifier = "com.test.one_pack.one_prod_ios",
            description = "Offering description",
            isCurrent = false,
            packages = listOf(weeklyPackageIos)
        )
    }

    private val twoPackThreeProdAnd by lazy {
        OfferingsDto(
            id = "234",
            identifier = "com.test.two_pack.three_prod_and",
            description = "Offering description",
            isCurrent = true,
            packages = listOf(weeklyPackageAnd, monthlyPackage)
        )
    }

    private val twoPackThreeProdIos by lazy {
        OfferingsDto(
            id = "345",
            identifier = "com.test.two_pack.three_prod_ios",
            description = "Offering description",
            isCurrent = false,
            packages = listOf(weeklyPackageIos, monthlyPackage)
        )
    }

    private val weeklyPackageAnd by lazy {
        PackageDto(
            id = "123PCK",
            identifier = "com.test.weekly_package_and",
            description = "Package description",
            products = listOf(androidWeekly)
        )
    }

    private val weeklyPackageIos by lazy {
        PackageDto(
            id = "321PCK",
            identifier = "com.test.weekly_package_ios",
            description = "Package description",
            products = listOf(iosWeekly)
        )
    }

    private val monthlyPackage by lazy {
        PackageDto(
            id = "234PCK",
            identifier = "com.test.monthly_package",
            description = "Package description",
            products = listOf(androidMonthly, iosMonthly)
        )
    }

    private val androidWeekly by lazy {
        ProductDto(
            id = "123PRO",
            source = "manual",
            identifier = "com.test.android.weekly",
            platform = "android"
        )
    }

    private val iosWeekly by lazy {
        ProductDto(
            id = "321PRO",
            source = "manual",
            identifier = "com.test.ios.weekly",
            platform = "ios"
        )
    }

    private val androidMonthly by lazy {
        ProductDto(
            id = "234PRO",
            source = "manual",
            identifier = "com.test.android.monthly",
            platform = "android"
        )
    }

    private val iosMonthly by lazy {
        ProductDto(
            id = "432PRO",
            source = "manual",
            identifier = "com.test.ios.monthly",
            platform = "ios"
        )
    }
}
