package tr.com.mobilex.core.billing

import android.app.Activity
import android.app.Application
import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingResult
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchaseHistoryRecord
import com.android.billingclient.api.PurchaseHistoryResponseListener
import com.android.billingclient.api.PurchasesResponseListener
import com.android.billingclient.api.SkuDetails
import com.android.billingclient.api.SkuDetailsResponseListener
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.mockkStatic
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import tr.com.mobilex.core.coroutine.DispatcherProvider
import tr.com.mobilex.core.logging.Logger
import java.lang.reflect.Field

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@Config(application = Application::class)
internal class ImplBillingClientManagerTests {

    private lateinit var billingClientManager: ImplBillingClientManager

    private val testDispatcher = TestCoroutineDispatcher()

    @MockK
    lateinit var billingClient: BillingClient

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        resetSingletonClientManager()
        mockLogger()
        mockDispatcherProvider()
        mockBillingClient()

        val appContext = ApplicationProvider.getApplicationContext<Context>()
        billingClientManager = ImplBillingClientManager.getInstance(appContext as Application, null)
    }

    @After
    fun tearDown() {
        // Called after each test
    }

    private fun resetSingletonClientManager() {
        val instance: Field = ImplBillingClientManager::class.java.getDeclaredField("INSTANCE")
        instance.isAccessible = true
        instance.set(null, null)
    }

    private fun mockLogger() {
        mockkObject(Logger)
        every { Logger.info(any(), any()) } answers { }
        every { Logger.debug(any(), any()) } answers { }
        every { Logger.warning(any(), any()) } answers { }
        every { Logger.error(any(), any()) } answers { }
    }

    private fun mockDispatcherProvider() {
        mockkObject(DispatcherProvider)
        every { DispatcherProvider.Default } returns testDispatcher
        every { DispatcherProvider.IO } returns testDispatcher
        every { DispatcherProvider.Main } returns testDispatcher
        every { DispatcherProvider.Unconfined } returns testDispatcher
    }

    private fun mockBillingClient() {
        every { billingClient.isReady } returns false
        every { billingClient.startConnection(any()) } answers {
            every { billingClient.isReady } returns true
        }

        mockkStatic(BillingClient::class)
        every {
            BillingClient.newBuilder(any()).setListener(any()).enablePendingPurchases().build()
        } returns billingClient
    }

    private fun mockSkuDetailsResult(responseCode: Int, skuDetailsList: List<SkuDetails>?) {
        val result = BillingResult.newBuilder()
            .setResponseCode(responseCode)
            .setDebugMessage("")
            .build()

        every { billingClient.querySkuDetailsAsync(any(), any()) } answers {
            val listener = it.invocation.args[1] as SkuDetailsResponseListener
            listener.onSkuDetailsResponse(result, skuDetailsList)
        }
    }

    private fun mockPurchaseHistoryResult(
        responseCode: Int,
        purchaseHistory: List<PurchaseHistoryRecord>?
    ) {
        val result = BillingResult.newBuilder()
            .setResponseCode(responseCode)
            .setDebugMessage("")
            .build()

        every { billingClient.queryPurchaseHistoryAsync(any(), any()) } answers {
            val listener = it.invocation.args[1] as PurchaseHistoryResponseListener
            listener.onPurchaseHistoryResponse(result, purchaseHistory)
        }
    }

    private fun mockPurchasesResult(
        responseCode: Int,
        purchaseList: List<Purchase>
    ) {
        val result = BillingResult.newBuilder()
            .setResponseCode(responseCode)
            .setDebugMessage("")
            .build()

        every { billingClient.queryPurchasesAsync(any(), any()) } answers {
            val listener = it.invocation.args[1] as PurchasesResponseListener
            listener.onQueryPurchasesResponse(result, purchaseList)
        }
    }

    private fun mockBillingFlowResult(responseCode: Int) {
        val result = BillingResult.newBuilder()
            .setResponseCode(responseCode)
            .setDebugMessage("")
            .build()

        every { billingClient.launchBillingFlow(any(), any()) } returns result
    }

    private fun mockPreviousSubscription() {
        val expectedList = listOf(purchase1)
        mockPurchasesResult(BillingClient.BillingResponseCode.OK, expectedList)

        billingClientManager.queryPurchases(ProductType.Subs) {
        }
    }

    @Test
    fun withSuccessfulResultBillingSetupFinishedShouldNotTryReconnection() = runBlocking {
        val result = BillingResult.newBuilder()
            .setResponseCode(BillingClient.BillingResponseCode.OK)
            .setDebugMessage("")
            .build()
        billingClientManager.onBillingSetupFinished(result)

        delay(1500)
        verify(atMost = 1) { billingClient.startConnection(any()) }
    }

    @Test
    fun withUnsuccessfulResultBillingSetupFinishedShouldTryReconnection() = runBlocking {
        val result = BillingResult.newBuilder()
            .setResponseCode(BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE)
            .setDebugMessage("")
            .build()
        billingClientManager.onBillingSetupFinished(result)

        delay(1500)
        verify(atMost = 2) { billingClient.startConnection(any()) }
    }

    @Test
    fun inAllCasesBillingServiceDisconnectedShouldTryReconnection() = runBlocking {
        billingClientManager.onBillingServiceDisconnected()

        delay(1500)
        verify(atMost = 2) { billingClient.startConnection(any()) }
    }

    @Test
    fun withSuccessfulResultQueryProductDetailsShouldReturnProducts_KnownProducts() {
        val expectedList = listOf(knownProduct1, knownProduct2)
        mockSkuDetailsResult(BillingClient.BillingResponseCode.OK, expectedList)

        billingClientManager.queryProductDetails(
            listOf("KnownProduct1", "KnownProduct2"),
            ProductType.Subs
        ) { productDetails -> assertProductDetails(expectedList, productDetails) }
    }

    @Test
    fun withSuccessfulResultQueryProductDetailsShouldReturnProducts_UnknownProduct() {
        val expectedList = listOf(knownProduct2)
        mockSkuDetailsResult(BillingClient.BillingResponseCode.OK, expectedList)

        billingClientManager.queryProductDetails(
            listOf("UnknownProduct3", "KnownProduct2"),
            ProductType.Subs
        ) { productDetails -> assertProductDetails(expectedList, productDetails) }
    }

    @Test
    fun withSuccessfulResultQueryProductDetailsShouldReturnProducts_InAppProduct() {
        val expectedList = listOf(knownProduct4)
        mockSkuDetailsResult(BillingClient.BillingResponseCode.OK, expectedList)

        billingClientManager.queryProductDetails(
            listOf("KnownProduct4"),
            ProductType.InApp
        ) { productDetails -> assertProductDetails(expectedList, productDetails) }
    }

    @Test
    fun withUnsuccessfulResultQueryProductDetailsShouldReturnEmptyList() {
        mockSkuDetailsResult(BillingClient.BillingResponseCode.ITEM_UNAVAILABLE, null)

        billingClientManager.queryProductDetails(
            listOf("KnownProduct4"),
            ProductType.InApp
        ) { productDetails ->
            assertTrue(
                "When response is not successful, empty list must be returned",
                productDetails.isEmpty()
            )
        }
    }

    private fun assertProductDetails(
        expectedList: List<SkuDetails>,
        productDetails: List<RevXProduct>
    ) {
        assertEquals(expectedList.size, productDetails.size)
        expectedList.forEachIndexed { index, expectedDetail ->
            val skuDetails = productDetails[index]
            assertEquals(expectedDetail.sku, skuDetails.productId)
            assertEquals(expectedDetail.title, skuDetails.title)
            assertEquals(expectedDetail.description, skuDetails.description)
            assertEquals(expectedDetail.price, skuDetails.price)
            assertEquals(expectedDetail.priceAmountMicros, skuDetails.priceAmountMicros)
            assertEquals(expectedDetail.priceCurrencyCode, skuDetails.priceCurrencyCode)
            assertEquals(expectedDetail.subscriptionPeriod, skuDetails.subscriptionPeriod)
            assertEquals(expectedDetail.type, skuDetails.type?.text)
        }
    }

    @Test
    fun withSuccessfulResultQueryPurchaseHistoryShouldReturnSubscriptions() {
        val expectedList = listOf(purchaseHistory1)
        mockPurchaseHistoryResult(BillingClient.BillingResponseCode.OK, expectedList)

        billingClientManager.queryPurchaseHistory(ProductType.Subs) { purchaseHistory ->
            assertPurchaseHistory(expectedList, purchaseHistory)
        }
    }

    @Test
    fun withSuccessfulResultQueryPurchaseHistoryShouldReturnInAppProducts() {
        val expectedList = listOf(purchaseHistory2)
        mockPurchaseHistoryResult(BillingClient.BillingResponseCode.OK, expectedList)

        billingClientManager.queryPurchaseHistory(ProductType.InApp) { purchaseHistory ->
            assertPurchaseHistory(expectedList, purchaseHistory)
        }
    }

    @Test
    fun withUnsuccessfulResultQueryPurchaseHistoryShouldReturnEmptyList() {
        mockPurchaseHistoryResult(BillingClient.BillingResponseCode.ERROR, null)

        billingClientManager.queryPurchaseHistory(ProductType.Subs) { purchaseHistory ->
            assertTrue(
                "When response is not successful, empty list must be returned",
                purchaseHistory.isEmpty()
            )
        }
    }

    private fun assertPurchaseHistory(
        expectedList: List<PurchaseHistoryRecord>,
        purchaseHistory: List<RevXPurchaseHistory>
    ) {
        assertEquals(expectedList.size, purchaseHistory.size)
        expectedList.forEachIndexed { index, expectedRecord ->
            val historyRecord = purchaseHistory[index]
            assertEquals(expectedRecord.purchaseToken, historyRecord.purchaseToken)
            assertEquals(expectedRecord.quantity, historyRecord.quantity)
            assertEquals(expectedRecord.purchaseTime, historyRecord.purchaseTime)
        }
    }

    @Test
    fun withSuccessfulResultQueryPurchasesShouldReturnSubscriptions() {
        val expectedList = listOf(purchase1)
        mockPurchasesResult(BillingClient.BillingResponseCode.OK, expectedList)

        billingClientManager.queryPurchases(ProductType.Subs) { purchases ->
            assertPurchases(expectedList, purchases)
        }
    }

    @Test
    fun withSuccessfulResultQueryPurchasesShouldReturnInAppProducts() {
        val expectedList = listOf(purchase2)
        mockPurchasesResult(BillingClient.BillingResponseCode.OK, expectedList)

        billingClientManager.queryPurchases(ProductType.InApp) { purchases ->
            assertPurchases(expectedList, purchases)
        }
    }

    @Test
    fun withUnsuccessfulResultQueryPurchasesShouldReturnEmptyList() {
        mockPurchasesResult(BillingClient.BillingResponseCode.ERROR, listOf())

        billingClientManager.queryPurchases(ProductType.Subs) { purchases ->
            assertTrue(
                "When response is not successful, empty list must be returned",
                purchases.isEmpty()
            )
        }
    }

    private fun assertPurchases(
        expectedList: List<Purchase>,
        purchaseList: List<RevXPurchase>
    ) {
        assertEquals(expectedList.size, purchaseList.size)
        expectedList.forEachIndexed { index, expectedRecord ->
            val purchase = purchaseList[index]
            assertEquals(expectedRecord.purchaseToken, purchase.purchaseToken)
            assertEquals(expectedRecord.orderId, purchase.orderId)
            assertEquals(expectedRecord.quantity, purchase.quantity)
            assertEquals(expectedRecord.purchaseTime, purchase.purchaseTime)
            assertEquals(expectedRecord.isAutoRenewing, purchase.isAutoRenewing)
            assertEquals(expectedRecord.isAcknowledged, purchase.isAcknowledged)
        }
    }

    @Test
    fun withNewProductProvidedPurchaseProductShouldLaunchBillingFlow() {
        mockBillingFlowResult(BillingClient.BillingResponseCode.OK)
        val activity: Activity = mockk()

        billingClientManager.purchaseProduct(activity, knownRevXProduct1)

        verify(atMost = 1) { billingClient.launchBillingFlow(any(), any()) }
    }

    @Test
    fun withSubscriptionUpdatedPurchaseProductShouldLaunchBillingFlow() {
        mockBillingFlowResult(BillingClient.BillingResponseCode.OK)
        mockPreviousSubscription()
        val activity: Activity = mockk()

        billingClientManager.purchaseProduct(activity, knownRevXProduct2, knownRevXProduct1)

        verify(atMost = 1) { billingClient.launchBillingFlow(any(), any()) }
    }

    @Test
    fun withUnsuccessfulFlowResponsePurchaseProductShouldTryLaunchBillingFlow() {
        mockBillingFlowResult(BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE)
        val activity: Activity = mockk()

        billingClientManager.purchaseProduct(activity, knownRevXProduct1)

        verify(atMost = 1) { billingClient.launchBillingFlow(any(), any()) }
    }

    @Test
    fun withNonNullListPurchaseUpdatedShouldCallListener() {
        val expectedList = mutableListOf(purchase1)
        val result = BillingResult.newBuilder()
            .setResponseCode(BillingClient.BillingResponseCode.OK)
            .setDebugMessage("")
            .build()

        billingClientManager.newPurchaseListener = object : NewPurchaseListener {
            override fun onNewPurchaseComplete(purchase: RevXPurchase) {
                assertPurchases(expectedList, listOf(purchase))
            }
        }
        billingClientManager.onPurchasesUpdated(result, expectedList)
    }

    @Test
    fun withNullListPurchaseUpdatedShouldNotCallListener() {
        val result = BillingResult.newBuilder()
            .setResponseCode(BillingClient.BillingResponseCode.OK)
            .setDebugMessage("")
            .build()

        billingClientManager.newPurchaseListener = object : NewPurchaseListener {
            override fun onNewPurchaseComplete(purchase: RevXPurchase) {
                assertTrue("This callback should not get called", false)
            }
        }
        billingClientManager.onPurchasesUpdated(result, null)
    }

    @Test
    fun withUnsuccessfulResponsePurchaseUpdatedShouldNotCallListener() {
        val result = BillingResult.newBuilder()
            .setResponseCode(BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED)
            .setDebugMessage("")
            .build()

        billingClientManager.newPurchaseListener = object : NewPurchaseListener {
            override fun onNewPurchaseComplete(purchase: RevXPurchase) {
                assertTrue("This callback should not get called", false)
            }
        }
        billingClientManager.onPurchasesUpdated(result, null)
    }

    private val knownProduct1: SkuDetails by lazy {
        mockk {
            every { sku } returns "KnownProduct1"
            every { title } returns "Know Product 1"
            every { description } returns "Know Product 1 Desc"
            every { price } returns "15$"
            every { priceAmountMicros } returns 15000000
            every { priceCurrencyCode } returns "USD"
            every { subscriptionPeriod } returns "P1M"
            every { type } returns "subs"
            every { zzc() } returns "com.mobilex.revenuex.test"
            every { originalJson } returns "{ \"productId\": \"KnownProduct1\", \"type\": \"subs\", \"title\": \"Know Product 1\",\n" +
                "        \"name\": \"Know Product 1\", \"price\": \"15\$\", \"price_amount_micros\": 15000000, \"price_currency_code\": \"USD\",\n" +
                "        \"description\": \"Know Product 1 Desc\", \"subscriptionPeriod\": \"P1M\",\n" +
                "        \"skuDetailsToken\": \"AEuhp4K0pW7O_pJYKOmM9TATIPv20yEd_G_nxiAPes3hphAfx1rmueo7EvTbqKsOVi32\" }"
            every { freeTrialPeriod } returns ""
            every { originalPrice } returns ""
            every { originalPriceAmountMicros } returns 0
            every { introductoryPrice } returns ""
            every { introductoryPriceAmountMicros } returns 0
            every { introductoryPricePeriod } returns ""
            every { introductoryPriceCycles } returns 0
            every { iconUrl } returns ""
        }
    }

    private val knownRevXProduct1: RevXProduct by lazy {
        mockk {
            every { productId } returns "KnownProduct1"
            every { title } returns "Know Product 1"
            every { description } returns "Know Product 1 Desc"
            every { price } returns "15$"
            every { priceAmountMicros } returns 15000000
            every { priceCurrencyCode } returns "USD"
            every { subscriptionPeriod } returns "P1M"
            every { type } returns ProductType.Subs
            every { originalJson } returns "{ \"productId\": \"KnownProduct1\", \"type\": \"subs\", \"title\": \"Know Product 1\",\n" +
                "        \"name\": \"Know Product 1\", \"price\": \"15\$\", \"price_amount_micros\": 15000000, \"price_currency_code\": \"USD\",\n" +
                "        \"description\": \"Know Product 1 Desc\", \"subscriptionPeriod\": \"P1M\",\n" +
                "        \"skuDetailsToken\": \"AEuhp4K0pW7O_pJYKOmM9TATIPv20yEd_G_nxiAPes3hphAfx1rmueo7EvTbqKsOVi32\" }"
        }
    }

    private val knownProduct2: SkuDetails by lazy {
        mockk {
            every { sku } returns "KnownProduct2"
            every { title } returns "Know Product 2"
            every { description } returns "Know Product 2 Desc"
            every { price } returns "24,99$"
            every { priceAmountMicros } returns 24990000
            every { priceCurrencyCode } returns "USD"
            every { subscriptionPeriod } returns "P1Y"
            every { type } returns "subs"
            every { zzc() } returns "com.mobilex.revenuex.test"
            every { originalJson } returns "{ \"productId\": \"KnownProduct2\", \"type\": \"subs\", \"title\": \"Know Product 2\",\n" +
                "        \"name\": \"Know Product 2\", \"price\": \"24,99\$\", \"price_amount_micros\": 24990000, \"price_currency_code\": \"USD\",\n" +
                "        \"description\": \"Know Product 2 Desc\", \"subscriptionPeriod\": \"P1Y\",\n" +
                "        \"skuDetailsToken\": \"AEuhp4LroodwocypUH8qNKFA2SQYD2rRuAczHjYorbgiskCCEte3qfiL2NKoAxz4cUmk\" }"
            every { freeTrialPeriod } returns ""
            every { originalPrice } returns ""
            every { originalPriceAmountMicros } returns 0
            every { introductoryPrice } returns ""
            every { introductoryPriceAmountMicros } returns 0
            every { introductoryPricePeriod } returns ""
            every { introductoryPriceCycles } returns 0
            every { iconUrl } returns ""
        }
    }

    private val knownRevXProduct2: RevXProduct by lazy {
        mockk {
            every { productId } returns "KnownProduct2"
            every { title } returns "Know Product 2"
            every { description } returns "Know Product 2 Desc"
            every { price } returns "24,99$"
            every { priceAmountMicros } returns 24990000
            every { priceCurrencyCode } returns "USD"
            every { subscriptionPeriod } returns "P1Y"
            every { type } returns ProductType.Subs
            every { originalJson } returns "{ \"productId\": \"KnownProduct2\", \"type\": \"subs\", \"title\": \"Know Product 2\",\n" +
                "        \"name\": \"Know Product 2\", \"price\": \"24,99\$\", \"price_amount_micros\": 24990000, \"price_currency_code\": \"USD\",\n" +
                "        \"description\": \"Know Product 2 Desc\", \"subscriptionPeriod\": \"P1Y\",\n" +
                "        \"skuDetailsToken\": \"AEuhp4LroodwocypUH8qNKFA2SQYD2rRuAczHjYorbgiskCCEte3qfiL2NKoAxz4cUmk\" }"
        }
    }

    private val knownProduct4: SkuDetails by lazy {
        mockk {
            every { sku } returns "KnownProduct4"
            every { title } returns "Know Product 4"
            every { description } returns "Know Product 4 Desc"
            every { price } returns "9,99$"
            every { priceAmountMicros } returns 9990000
            every { priceCurrencyCode } returns "USD"
            every { subscriptionPeriod } returns ""
            every { type } returns "inapp"
            every { zzc() } returns "com.mobilex.revenuex.test"
            every { originalJson } returns "{ \"productId\": \"KnownProduct4\", \"type\": \"inapp\", \"title\": \"Know Product 4\",\n" +
                "        \"name\": \"Know Product 4\", \"price\": \"9,99\$\", \"price_amount_micros\": 9990000, \"price_currency_code\": \"USD\",\n" +
                "        \"description\": \"Know Product 4 Desc\", \"subscriptionPeriod\": \"\",\n" +
                "        \"skuDetailsToken\": \"AEuhp4LZ7wDwIYGE2l0WZr1-TRByBuc_XpTqIRGTcMAcf03MG83OFcbVktz-DdoYrh_m\" }"
            every { freeTrialPeriod } returns ""
            every { originalPrice } returns ""
            every { originalPriceAmountMicros } returns 0
            every { introductoryPrice } returns ""
            every { introductoryPriceAmountMicros } returns 0
            every { introductoryPricePeriod } returns ""
            every { introductoryPriceCycles } returns 0
            every { iconUrl } returns ""
        }
    }

    private val purchaseHistory1: PurchaseHistoryRecord by lazy {
        PurchaseHistoryRecord(
            "{ \"productId\": \"KnownProduct1\", \"purchaseTime\": 1639138943947,\n" +
                "    \"purchaseToken\": \"5a19c21359b444328fb9baafa52c74f9\",\n" +
                "    \"quantity\": 1\n }",
            "gmnmfhfdbkfnagmgfnpjeaok.AO-J1OwZDezVPwxiS6DZlv2E4byS8gf_JIVUpFG6qix4iXLzMtohqWiMcwrHR"
        )
    }

    private val purchaseHistory2: PurchaseHistoryRecord by lazy {
        PurchaseHistoryRecord(
            "{ \"productId\": \"KnownProduct4\", \"purchaseTime\": 1638973900481,\n" +
                "    \"purchaseToken\": \"17ab19211033486f90aa5b0e42c6999d\",\n" +
                "    \"quantity\": 1\n }",
            "gmnmfhfdbkfnagmgfnpjeaok.AO-17ab19211033486f90aa5b0e42c6999d_17760e7775504efca3754cb300b7ad56"
        )
    }

    private val purchase1: Purchase by lazy {
        Purchase(
            "{ \"orderId\": \"17760e7775504efca3754cb300b7ad56\", \"packageName\": \"com.mobilex.revenuex.test\", \"productId\": \"KnownProduct1\",\n" +
                "    \"purchaseTime\": 1639138943947, \"purchaseState\": 0,\n" +
                "    \"purchaseToken\": \"5a19c21359b444328fb9baafa52c74f9\",\n" +
                "    \"quantity\": 1, \"autoRenewing\": true, \"acknowledged\": false\n }",
            "gmnmfhfdbkfnagmgfnpjeaok.AO-J1OwZDezVPwxiS6DZlv2E4byS8gf_JIVUpFG6qix4iXLzMtohqWiMcwrHR"
        )
    }

    private val purchase2: Purchase by lazy {
        Purchase(
            "{ \"orderId\": \"b6dd1f71fc254cb4989efaa170823099\", \"packageName\": \"com.mobilex.revenuex.test\", \"productId\": \"KnownProduct4\",\n" +
                "    \"purchaseTime\": 1638973900481, \"purchaseState\": 0,\n" +
                "    \"purchaseToken\": \"17ab19211033486f90aa5b0e42c6999d\",\n" +
                "    \"quantity\": 1, \"autoRenewing\": false, \"acknowledged\": false\n }",
            "gmnmfhfdbkfnagmgfnpjeaok.AO-17ab19211033486f90aa5b0e42c6999d_17760e7775504efca3754cb300b7ad56"
        )
    }
}
