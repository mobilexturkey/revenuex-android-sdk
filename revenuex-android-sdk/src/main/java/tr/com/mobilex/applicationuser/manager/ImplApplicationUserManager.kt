package tr.com.mobilex.applicationuser.manager

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import tr.com.mobilex.applicationuser.network.ApplicationUserApi
import tr.com.mobilex.applicationuser.network.CurrentRevenueXUserDto
import tr.com.mobilex.applicationuser.network.UserAttributes
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.coroutine.DispatcherProvider
import tr.com.mobilex.core.db.dao.AttributeDao
import tr.com.mobilex.core.db.dao.UserDao
import tr.com.mobilex.core.db.entity.AttributeEntity
import tr.com.mobilex.core.db.entity.UserEntity
import tr.com.mobilex.core.db.relational.UserWithAttributes
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.utils.getExceptionMessage

/**
 * Class to manage logics about application user
 * For example; Setting user attributes,
 * caching and retrieving current user information,
 * sending open events to RevenueX, etc.
 */
internal class ImplApplicationUserManager(
    private val systemInfo: SystemInfo,
    private val applicationUserApi: ApplicationUserApi,
    private val applicationUserDao: UserDao,
    private val attributeDao: AttributeDao
) : ApplicationUserManager {

    /**
     * Mutable live data that holds current user information
     */
    private val _revenueXUser: MutableLiveData<UserWithAttributes> = MutableLiveData()

    /**
     * Live data that holds current user information
     */
    override val revenueXUser: LiveData<UserWithAttributes> = _revenueXUser

    /**
     * Initializes manager with cached data
     */
    init {
        CoroutineScope(DispatcherProvider.IO).launch {
            val cachedUser = applicationUserDao.getUsersWithAttributes().firstOrNull()
            cachedUser?.let {
                _revenueXUser.postValue(cachedUser)
            }
        }
    }

    /**
     * Calls open event from the ApplicationUserApi
     * and gets RevenueX user information
     * @param onComplete Callback function
     */
    override fun openEvent(onComplete: () -> Unit) {
        applicationUserApi.openEvent({
            Logger.error(it.getExceptionMessage(), it)
            onComplete.invoke()
        }, { response ->
            Logger.debug("openEvent response: $response")
            response?.let {
                // Syncs user information returned from RevenueX with local information
                updateRevenueXUser(response)
            }
            onComplete.invoke()
        })
    }

    /**
     * Sets new or updated user attributes on RevenueX
     * Unchanged parameters not needed to be sent
     * Current user will be updated with returned user
     * @param attributes User attributes map
     */
    override fun setAttributes(attributes: Map<UserAttributes, String>) {
        // Updates attributes on local user
        updateLocalUser(attributes)

        // Sends new attributes to RevenueX
        applicationUserApi.setAttributes(attributes, {
            Logger.error(it.getExceptionMessage(), it)
        }, { response ->
            Logger.debug("setAttributes response: $response")
            response?.let {
                // Syncs user information returned from RevenueX with local information
                updateRevenueXUser(response)
            }
        })
    }

    /**
     * Creates alias between the new user id and old user id
     * @param newUserId New user id
     * @param oldUserId Old user id
     */
    override fun createAlias(newUserId: String, oldUserId: String) {
        // If ids are same, no need to call RevenueX
        if (newUserId == oldUserId) return

        // Calls RevenueX to create alias between user ids
        applicationUserApi.createAlias(newUserId, oldUserId, {
            Logger.error(it.getExceptionMessage(), it)
        }, { response ->
            Logger.debug("createAlias response: $response")
            response?.let {
                // Syncs user information returned from RevenueX with local information
                updateRevenueXUser(response)
            }
        })
    }

    /**
     * Updates user attributes on local RevenueX user
     * @param attributes User attributes map
     */
    private fun updateLocalUser(attributes: Map<UserAttributes, String>) {
        CoroutineScope(DispatcherProvider.IO).launch {
            val currentUser =
                _revenueXUser.value ?: applicationUserDao.getUsersWithAttributes().first()

            val currentAttributes = currentUser.attributes.toMutableList()
            attributes.forEach { attribute ->
                val newAttribute =
                    AttributeEntity(name = attribute.key.key, value = attribute.value)
                currentAttributes.removeAll { attr -> attr.name == newAttribute.name }
                currentAttributes.add(newAttribute)
            }
            currentUser.attributes = currentAttributes

            updateRevenueXUser(currentUser)
        }
    }

    /**
     * Updates RevenueX user live data
     * @param user RevenueX user data model returned from the server
     */
    private fun updateRevenueXUser(user: CurrentRevenueXUserDto) {
        val currentUser = _revenueXUser.value ?: UserWithAttributes(UserEntity(id = 1))
        val updatedUser = currentUser.update(user)
        updateRevenueXUser(updatedUser)
    }

    /**
     * Updates RevenueX user live data
     * Stores updated user in the database
     * @param user User entity with attributes
     */
    private fun updateRevenueXUser(user: UserWithAttributes) {
        CoroutineScope(DispatcherProvider.IO).launch {
            storeUser(user)
            _revenueXUser.postValue(user)
            user.user.revenueXId?.let { id ->
                systemInfo.revenueXUserId = id
            }
        }
    }

    /**
     * Updates user information in the database
     * And replaces old attributes with current ones
     * @param userWithAttrs User entity with attributes
     */
    @WorkerThread
    @Synchronized
    private fun storeUser(userWithAttrs: UserWithAttributes) {
        // Clear old attributes
        attributeDao.clear()

        // Update user and insert attributes
        applicationUserDao.update(userWithAttrs.user).also {
            userWithAttrs.attributes.forEach { attr ->
                attr.userId = userWithAttrs.user.id
                attributeDao.insert(attr)
            }
        }
    }
}
