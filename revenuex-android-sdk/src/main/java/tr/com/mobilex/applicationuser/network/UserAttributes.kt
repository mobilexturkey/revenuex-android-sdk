package tr.com.mobilex.applicationuser.network

/**
 * Enum values for valid user attributes
 */
enum class UserAttributes(val key: String) {
    DisplayName("displayName"),
    Email("email"),
    PhoneNumber("phoneNumber"),
    ApnsTokens("apnsTokens"),
    FcmTokens("fcmTokens"),
    Idfa("idfa"),
    Idfv("idfv"),
    GpsAdId("gpsAdId"),
    AndroidId("androidId"),
    MediaSource("mediaSource"),
    Campaign("campaign"),
    AdGroup("adGroup"),
    Ad("ad"),
    TitleWord("titleword"),
    Creative("creative"),
    AdjustId("adjustId"),
    AppsFlyerId("appsflyerId"),
    FbAnonId("fbAnonId"),
    MParticleId("mparticleId"),
    OneSignalId("onesignalId")
}
