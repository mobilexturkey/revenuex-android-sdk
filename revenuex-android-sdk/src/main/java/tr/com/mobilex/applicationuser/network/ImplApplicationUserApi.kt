package tr.com.mobilex.applicationuser.network

import com.google.gson.Gson
import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import tr.com.mobilex.core.utils.getConcatenatedUrl
import tr.com.mobilex.core.utils.getExceptionMessage

/**
 * Class that manages calls to application-users API
 */
internal class ImplApplicationUserApi(
    override val systemInfo: SystemInfo,
    private val httpClient: HttpClient
) : ApplicationUserApi {

    /**
     * Constant params
     */
    private val apiUrl = systemInfo.rootUrl
        .getConcatenatedUrl(Constants.APPLICATION_USERS_API_URL)

    /**
     * Base HTTP headers used in all requests
     */
    private val baseHeaders: Map<String, String> = mapOf(
        Constants.CLIENT_ID to systemInfo.clientId,
        Constants.PLATFORM to systemInfo.platform,
        Constants.PLATFORM_VERSION to systemInfo.platformVersion,
        Constants.APPLICATION_VERSION to systemInfo.applicationVersion,
        Constants.DEVICE_NAME to systemInfo.deviceName,
        Constants.SANDBOX to "${systemInfo.isSandbox}",
        Constants.REGION to systemInfo.region,
        Constants.CONTENT_TYPE to Constants.JSON_CONTENT_TYPE
    )

    /**
     * Registers new user to RevenueX
     * Even if 'user-revenueXId' sent to server,
     * new id is generated for the new user
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when user is registered
     *          This callback takes data model with new users information
     */
    override fun registerApplicationUser(
        onFailure: (Exception) -> Unit,
        onResponse: (CurrentRevenueXUserDto?) -> Unit
    ) {
        // Prepares endpoint url and headers for the request
        val registerUserUrl = apiUrl.getConcatenatedUrl()
        val headers = baseHeaders.toMutableMap().also { newMap ->
            newMap[Constants.CONTENT_LANGUAGE] = systemInfo.language
            newMap[Constants.USER_REVENUEX_ID] = systemInfo.revenueXUserId
        }

        // Makes HTTP call to RevenueX API
        httpClient.post(
            url = registerUserUrl,
            jsonBody = Constants.EMPTY_JSON,
            headers = headers,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("ApplicationUserApi registerApplicationUser server response: $response")
                onResponse.invoke(response)
            },
            classType = CurrentRevenueXUserDto::class.java
        )
    }

    /**
     * Sets user attributes on RevenueX
     * @param attributes User attributes map
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when attributes are set
     *          This callback takes data model with users latest information
     */
    override fun setAttributes(
        attributes: Map<UserAttributes, String>,
        onFailure: (Exception) -> Unit,
        onResponse: (CurrentRevenueXUserDto?) -> Unit
    ) {
        // Prepares endpoint url and headers for the request
        val setAttributeUrl = apiUrl.getConcatenatedUrl(Constants.APPLICATION_USERS_API_ATTRIBUTES)
        val headers = baseHeaders.toMutableMap().also { newMap ->
            newMap[Constants.USER_REVENUEX_ID] = systemInfo.revenueXUserId
        }

        // Prepares jsonBody with attributes parameter
        val attributesModel = attributes.map { AttributeModel(it.key.key, it.value) }
        val userAttributes = UserAttributesModel(attributesModel)
        val body = Gson().toJson(userAttributes)

        // Makes HTTP call to RevenueX API
        httpClient.post(
            url = setAttributeUrl,
            jsonBody = body,
            headers = headers,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("ApplicationUserApi setAttributes server response: $response")
                onResponse.invoke(response)
            },
            classType = CurrentRevenueXUserDto::class.java
        )
    }

    /**
     * Sends open event to RevenueX
     * If user is a new user, RevenueX registers the user automatically
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered if open event is successful
     *          This callback takes data model with users latest information
     */
    override fun openEvent(
        onFailure: (Exception) -> Unit,
        onResponse: (CurrentRevenueXUserDto?) -> Unit
    ) {
        // Prepares endpoint url and headers for the request
        val openEventUrl = apiUrl.getConcatenatedUrl(Constants.APPLICATION_USERS_API_OPEN)
        val headers = baseHeaders.toMutableMap().also { newMap ->
            newMap[Constants.CONTENT_LANGUAGE] = systemInfo.language
            newMap[Constants.USER_REVENUEX_ID] = systemInfo.revenueXUserId
        }

        // Makes HTTP call to RevenueX API
        httpClient.post(
            url = openEventUrl,
            jsonBody = Constants.EMPTY_JSON,
            headers = headers,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("ApplicationUserApi openEvent server response: $response")
                onResponse.invoke(response)
            },
            classType = CurrentRevenueXUserDto::class.java
        )
    }

    /**
     * Sends ping event to RevenueX
     * This event is used to know that user is still active
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered if ping event is successful
     *          This callback takes data model with users latest information
     */
    override fun pingEvent(
        onFailure: (Exception) -> Unit,
        onResponse: (CurrentRevenueXUserDto?) -> Unit
    ) {
        // Prepares endpoint url and headers for the request
        val pingEventUrl = apiUrl.getConcatenatedUrl(Constants.APPLICATION_USERS_API_PING)
        val headers = baseHeaders.toMutableMap().also { newMap ->
            newMap[Constants.CONTENT_LANGUAGE] = systemInfo.language
            newMap[Constants.USER_REVENUEX_ID] = systemInfo.revenueXUserId
        }

        // Makes HTTP call to RevenueX API
        httpClient.post(
            url = pingEventUrl,
            jsonBody = Constants.EMPTY_JSON,
            headers = headers,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("ApplicationUserApi pingEvent server response: $response")
                onResponse.invoke(response)
            },
            classType = CurrentRevenueXUserDto::class.java
        )
    }

    /**
     * Creates alias between the new user id and old user id
     * @param newUserId New user id
     * @param oldUserId Old user id
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered if alias created
     *          This callback takes data model with users latest information
     */
    override fun createAlias(
        newUserId: String,
        oldUserId: String,
        onFailure: (Exception) -> Unit,
        onResponse: (CurrentRevenueXUserDto?) -> Unit
    ) {
        // Prepares endpoint url and headers for the request
        val createAliasUrl = apiUrl.getConcatenatedUrl(Constants.APPLICATION_USERS_API_ALIAS)
        val headers = baseHeaders.toMutableMap().also { newMap ->
            newMap[Constants.USER_REVENUEX_ID] = oldUserId
        }

        val body = "{ \"newUserId\": \"$newUserId\" }"

        // Makes HTTP call to RevenueX API
        httpClient.post(
            url = createAliasUrl,
            jsonBody = body,
            headers = headers,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("ApplicationUserApi createAlias server response: $response")
                onResponse.invoke(response)
            },
            classType = CurrentRevenueXUserDto::class.java
        )
    }

    /**
     * Intermediate data model to parse user attributes to requested JSON body
     */
    private data class UserAttributesModel(val attributes: List<AttributeModel>)

    /**
     * Intermediate data model to parse user attributes to requested JSON body
     */
    private data class AttributeModel(val key: String, val value: String)
}
