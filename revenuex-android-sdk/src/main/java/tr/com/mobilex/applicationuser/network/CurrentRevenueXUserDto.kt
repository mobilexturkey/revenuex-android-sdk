package tr.com.mobilex.applicationuser.network

import java.io.Serializable

/**
 * Data model returned by the application user api
 * Holds information about the application user
 * @param applicationId Id of the application in RevenueX
 * @param revenueXId User id of the current user
 * @param lastSeenDate Time that user has accessed the server last time
 * @param createdAt Time that user is created
 * @param updatedAt Time that user is updated
 * @param attributes Attributes set for the user
 */
internal data class CurrentRevenueXUserDto(
    var applicationId: String? = null,
    var revenueXId: String? = null,
    var lastSeenDate: Long? = null,
    var createdAt: Long? = null,
    var updatedAt: Long? = null,
    var attributes: List<AttributeDto> = listOf()
) : Serializable

/**
 * Data model returned by the application user api
 * Holds information about an attribute
 * @param name Name of the user attribute
 * @param value Value of the user attribute
 */
internal data class AttributeDto(
    var name: String? = null,
    var value: String? = null
) : Serializable
