package tr.com.mobilex.applicationuser.manager

import androidx.lifecycle.LiveData
import tr.com.mobilex.applicationuser.network.UserAttributes
import tr.com.mobilex.core.db.relational.UserWithAttributes

/**
 * Class to manage logics about application user
 * For example; Setting user attributes,
 * caching and retrieving current user information,
 * sending open events to RevenueX, etc.
 */
internal interface ApplicationUserManager {

    /**
     * Live data that holds current user information
     */
    val revenueXUser: LiveData<UserWithAttributes>

    /**
     * Calls open event from the ApplicationUserApi
     * and gets RevenueX user information
     * @param onComplete Callback function
     */
    fun openEvent(onComplete: () -> Unit)

    /**
     * Sets new or updated user attributes on RevenueX
     * Unchanged parameters not needed to be sent
     * Current user will be updated with returned user
     * @param attributes User attributes map
     */
    fun setAttributes(attributes: Map<UserAttributes, String>)

    /**
     * Creates alias between the new user id and old user id
     * @param newUserId New user id
     * @param oldUserId Old user id
     */
    fun createAlias(newUserId: String, oldUserId: String)
}
