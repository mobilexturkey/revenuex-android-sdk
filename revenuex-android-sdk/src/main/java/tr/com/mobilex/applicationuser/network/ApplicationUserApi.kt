package tr.com.mobilex.applicationuser.network

import tr.com.mobilex.core.network.Api

/**
 * Application-user API interface
 */
internal interface ApplicationUserApi : Api {

    /**
     * Registers new user to RevenueX
     * Even if 'user-revenueXId' sent to server,
     * new id is generated for the new user
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when user is registered
     *          This callback takes data model with new users information
     */
    fun registerApplicationUser(
        onFailure: (Exception) -> Unit,
        onResponse: (CurrentRevenueXUserDto?) -> Unit
    )

    /**
     * Sets user attributes on RevenueX
     * @param attributes User attributes map
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when attributes are set
     *          This callback takes data model with users latest information
     */
    fun setAttributes(
        attributes: Map<UserAttributes, String>,
        onFailure: (Exception) -> Unit,
        onResponse: (CurrentRevenueXUserDto?) -> Unit
    )

    /**
     * Sends open event to RevenueX
     * If user is a new user, RevenueX registers the user automatically
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered if open event is successful
     *          This callback takes data model with users latest information
     */
    fun openEvent(
        onFailure: (Exception) -> Unit,
        onResponse: (CurrentRevenueXUserDto?) -> Unit
    )

    /**
     * Sends ping event to RevenueX
     * This event is used to know that user is still active
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered if ping event is successful
     *          This callback takes data model with users latest information
     */
    fun pingEvent(
        onFailure: (Exception) -> Unit,
        onResponse: (CurrentRevenueXUserDto?) -> Unit
    )

    /**
     * Creates alias between the new user id and old user id
     * @param newUserId New user id
     * @param oldUserId Old user id
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered if alias created
     *          This callback takes data model with users latest information
     */
    fun createAlias(
        newUserId: String,
        oldUserId: String,
        onFailure: (Exception) -> Unit,
        onResponse: (CurrentRevenueXUserDto?) -> Unit
    )
}
