package tr.com.mobilex.applications.network

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Data model returned by the applications api
 * Holds information about the application
 * @param id Id of the application in RevenueX
 * @param name Name of the application
 * @param currency Currency of the application
 * @param remoteConfigVersion Remote config version of the application
 * @param createdAt Time that application is created
 * @param updatedAt Time that application is updated
 * @param enabled If application is enabled
 * @param isDeleted If application is deleted
 * @param secretCredentials Credentials for application stores
 */
internal data class ApplicationInfoDto(
    @SerializedName("_id")
    var id: String?,
    var name: String?,
    var currency: String?,
    var remoteConfigVersion: Int?,
    var createdAt: String?,
    var updatedAt: String?,
    var enabled: Boolean?,
    var isDeleted: Boolean?,
    var secretCredentials: SecretCredentials?
) : Serializable

/**
 * Data model for credentials of application stores
 * @param android Credential for android application
 */
internal data class SecretCredentials(
    var android: AndroidCredential?
)

/**
 * Data model for credentials of android application
 * @param packageId Android application package id
 * @param serviceAccountCredentials Credential for service account
 */
internal data class AndroidCredential(
    var packageId: String?,
    var serviceAccountCredentials: String?
) : Serializable
