package tr.com.mobilex.applications.network

import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import tr.com.mobilex.core.utils.getConcatenatedUrl
import tr.com.mobilex.core.utils.getExceptionMessage

/**
 * Class that manages calls to applications API
 */
internal class ImplApplicationsApi(
    override val systemInfo: SystemInfo,
    private val httpClient: HttpClient
) : ApplicationsApi {

    /**
     * Constant params
     */
    private val apiUrl = systemInfo.rootUrl
        .getConcatenatedUrl(Constants.APPLICATIONS_API_URL)
    private val baseHeaders: Map<String, String> = mapOf()

    /**
     * Gets information about current application
     * @param clientSecret Client secret from the RevenueX portal
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when application info retrieved
     *          This callback takes data model with application information
     */
    override fun info(
        clientSecret: String,
        onFailure: (Exception) -> Unit,
        onResponse: (ApplicationInfoDto?) -> Unit
    ) {
        // Prepares endpoint url and headers for the request
        val applicationInfoUrl = apiUrl.getConcatenatedUrl(Constants.APPLICATIONS_API_INFO)
        val headers = baseHeaders.toMutableMap().also { newMap ->
            newMap[Constants.CLIENT_ID] = systemInfo.clientId
            newMap[Constants.CLIENT_SECRET] = clientSecret
        }

        // Makes HTTP call to RevenueX API
        httpClient.get(
            url = applicationInfoUrl,
            headers = headers,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("ApplicationsApi info server response: $response")
                onResponse.invoke(response)
            },
            classType = ApplicationInfoDto::class.java
        )
    }
}
