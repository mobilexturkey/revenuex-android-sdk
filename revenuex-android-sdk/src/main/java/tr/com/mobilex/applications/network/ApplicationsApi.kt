package tr.com.mobilex.applications.network

import tr.com.mobilex.core.network.Api

/**
 * Applications API interface
 */
internal interface ApplicationsApi : Api {

    /**
     * Gets information about current application
     * @param clientSecret Client secret from the RevenueX portal
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when application info retrieved
     *          This callback takes data model with application information
     */
    fun info(
        clientSecret: String,
        onFailure: (Exception) -> Unit,
        onResponse: (ApplicationInfoDto?) -> Unit
    )
}
