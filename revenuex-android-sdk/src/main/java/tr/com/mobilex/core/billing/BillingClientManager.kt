package tr.com.mobilex.core.billing

import android.app.Activity

/**
 * Interface for the billing client managers
 * Must manage fetching product details, purchase history and purchases
 */
internal interface BillingClientManager : ProductInformationProvider, PurchaseInformationProvider {

    /**
     * Listener of setup completion
     */
    var setupCompletionListener: SetupCompletionListener?
}

/**
 * Interface for product information providers
 * Fetches product information from billing client
 */
internal interface ProductInformationProvider {

    /**
     * Calls the billing client to query product details
     * @param productIds Ids of the products
     * @param type Type of the products
     * @param callback Callback function that listens for product details
     */
    fun queryProductDetails(
        productIds: List<String>,
        type: ProductType = ProductType.Subs,
        callback: ((List<RevXProduct>) -> Unit)? = null
    )
}

/**
 * Interface for purchase information providers
 * Fetches purchase history and purchases products with billing client
 */
internal interface PurchaseInformationProvider {

    /**
     * Listener of new purchases
     */
    var newPurchaseListener: NewPurchaseListener?

    /**
     * Gets previous purchases from billing client
     * @param type Type of the products
     * @param callback Callback function that listens for purchase history
     */
    fun queryPurchaseHistory(
        type: ProductType,
        callback: ((List<RevXPurchaseHistory>) -> Unit)? = null
    )

    /**
     * Gets active purchases from billing client
     * @param type Type of the products
     * @param callback Callback function that listens for purchases
     */
    fun queryPurchases(
        type: ProductType,
        callback: ((List<RevXPurchase>) -> Unit)? = null
    )

    /**
     * Starts purchase flow for the given product
     * If an active subscription is updated,
     * old subscription can be provided as parameter
     * When purchase flow is completed,
     * new purchase can be listened by newPurchaseListener
     * @param activity Active activity to launch purchase flow from
     * @param product Product to be purchased
     * @param upgradedProduct Product that the subscription can be upgraded from
     */
    fun purchaseProduct(
        activity: Activity,
        product: RevXProduct,
        upgradedProduct: RevXProduct? = null
    )
}

/**
 * Interface for listener of new purchases
 */
internal interface NewPurchaseListener {

    /**
     * Called when a new purchase is done
     * @param purchase Information about latest purchase
     */
    fun onNewPurchaseComplete(purchase: RevXPurchase)
}

/**
 * Interface for listener of billing client setup
 */
internal interface SetupCompletionListener {

    /**
     * Called when billing client setup is completed
     */
    fun onSetupComplete()
}
