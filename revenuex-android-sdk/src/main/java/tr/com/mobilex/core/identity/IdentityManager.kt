package tr.com.mobilex.core.identity

/**
 * Interface for the identity managers
 */
internal interface IdentityManager {

    /**
     * Gets RevenueX user id provided by developer
     * If not provided, checks cache or creates a new one
     * @param revenueXUserId User id provided by the developer
     * @param onUserIdChanged Callback function that will be called if user id changed
     *
     * @return RevenueX user id
     */
    fun getRevenueXUserId(
        revenueXUserId: String? = null,
        onUserIdChanged: ((newUserId: String, oldUserId: String) -> Unit)? = null
    ): String
}
