package tr.com.mobilex.core.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import tr.com.mobilex.core.billing.RevXPurchase

/**
 * Checks if purchase with given token is already in the live data list
 * @return If list contains the purchase or not
 */
internal fun LiveData<List<RevXPurchase>>.contains(purchaseToken: String): Boolean {
    val purchases = this.value ?: listOf()
    return purchases.any { it.purchaseToken == purchaseToken }
}

/**
 * Adds new purchase to the mutable live data list and calls observers
 * @param purchase New purchase that will be added to list
 */
internal fun MutableLiveData<List<RevXPurchase>>.add(purchase: RevXPurchase) {
    val purchases = this.value?.toMutableList() ?: mutableListOf()
    purchases.add(purchase)
    this.postValue(purchases)
}
