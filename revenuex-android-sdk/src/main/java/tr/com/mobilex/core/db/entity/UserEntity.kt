package tr.com.mobilex.core.db.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import kotlinx.parcelize.Parcelize
import tr.com.mobilex.core.utils.Converters
import java.util.Date

@Parcelize
@Entity(
    tableName = UserEntity.TABLE_USER_NAME
)
@TypeConverters(Converters::class)
data class UserEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    var applicationId: String? = null,
    var revenueXId: String? = null,
    var createdAt: Date? = Date(),
    var updatedAt: Date? = null,
    var lastSeenDate: Date? = null
) : Parcelable {

    companion object {
        const val USER_ID = "id"
        const val USER_DEFAULT_ID_VALUE = 1
        const val USER_APPLICATION_ID = "applicationId"
        const val USER_DEFAULT_APPLICATION_ID_VALUE = -1
        const val USER_REVENUEX_ID = "revenueXId"
        const val USER_DEFAULT_REVENUEX_ID_VALUE = -1
        const val USER_CREATED_AT = "createdAt"
        const val USER_DEFAULT_CREATED_AT_VALUE = 0
        const val USER_UPDATED_AT = "updatedAt"
        const val USER_DEFAULT_UPDATED_AT_VALUE = 0
        const val USER_LAST_SEEN_DATE = "lastSeenDate"
        const val USER_DEFAULT_LAST_SEEN_DATE_VALUE = 0
        const val TABLE_USER_NAME = "User"
    }
}
