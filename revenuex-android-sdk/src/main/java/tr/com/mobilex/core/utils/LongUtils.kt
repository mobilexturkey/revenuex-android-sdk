package tr.com.mobilex.core.utils

import tr.com.mobilex.core.Constants
import java.util.Date

/**
 * Converts long milliseconds to Date
 * @return Date created with milliseconds
 */
internal fun Long.toDate(): Date {
    return Date(this)
}

/**
 * Converts price in micro-units to unit of the currency
 * 1,000,000 micro-units equal one unit of the currency
 *
 * @return Price in unit of the currency
 */
internal fun Long.toPrice(): Double {
    return this.toDouble() / Constants.MICRO_TO_CURRENCY_CONVERSION_RATE
}
