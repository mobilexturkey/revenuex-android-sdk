package tr.com.mobilex.core

internal object Constants {

    /**
     * Root url of the RevenueX SDK
     * API endpoints will be concatenated to this url in api implementations
     */
    internal const val ROOT_URL = "https://sdk-test.revenueplus.net/"

    internal const val LOGGER_TAG = "RevenueX"
    internal const val ANDROID_PLATFORM = "android"

    internal const val HTTP_OK = 200
    internal const val HTTP_MULTIPLE_CHOICE = 300

    internal const val DATABASE_NAME = "revenuex_database"
    internal const val SHARED_PREF_FILE_NAME = "secure_shared_pref"
    internal const val VALIDATED_PURCHASE_TOKENS = "validated_purchase_tokens"
    internal const val REVENUEX_USER_ID_KEY = "revenuex_user_id_key"

    internal const val RECONNECT_TIMER_START_MILLISECONDS = 1000L * 1L // 1 second
    internal const val RECONNECT_TIMER_MAX_TIME_MILLISECONDS = 1000L * 60L * 15L // 15 minutes
    internal const val MICRO_TO_CURRENCY_CONVERSION_RATE = 1000000

    /**
     * Constants for API url parts
     */
    internal const val APPLICATIONS_API_URL = "api/applications/"
    internal const val APPLICATIONS_API_INFO = "info"
    internal const val APPLICATION_USERS_API_URL = "api/transactions/application-users/"
    internal const val APPLICATION_USERS_API_ATTRIBUTES = "set-attributes"
    internal const val APPLICATION_USERS_API_OPEN = "open-event"
    internal const val APPLICATION_USERS_API_PING = "ping-event"
    internal const val APPLICATION_USERS_API_ALIAS = "create-alias"
    internal const val TRANSACTIONS_API_URL = "api/transactions/"
    internal const val TRANSACTIONS_API_VALIDATE = "validate"
    internal const val PRODUCTS_API_URL = "api/products/"
    internal const val PRODUCTS_API_ALL = "all"
    internal const val OFFERINGS_API_URL = "api/offerings/"
    internal const val OFFERINGS_API_GET_OFFERINGS = "currentby/application"
    internal const val CONFIG_API_URL = "api/config/"
    internal const val CONFIG_API_REMOTE_CONFIG = "remote-config"
    internal const val ENTITLEMENTS_API_URL = "api/entitlements/"
    internal const val ENTITLEMENTS_API_GET_CURRENT = "current"

    /**
     * Constants for HTTP header and query params
     */
    internal const val CLIENT_ID = "clientid"
    internal const val CLIENT_SECRET = "clientsecret"
    internal const val USER_REVENUEX_ID = "user-revenueXId"
    internal const val PLATFORM = "platform"
    internal const val PLATFORM_VERSION = "platform-version"
    internal const val APPLICATION_VERSION = "application-version"
    internal const val DEVICE_NAME = "device-name"
    internal const val SANDBOX = "sandbox"
    internal const val REGION = "region"
    internal const val CONTENT_TYPE = "Content-Type"
    internal const val CONTENT_LANGUAGE = "content-language"
    internal const val EMPTY_JSON = "{}"
    internal const val JSON_CONTENT_TYPE = "application/json"
    internal const val JSON_MEDIA_TYPE = "application/json; charset=utf-8"
}
