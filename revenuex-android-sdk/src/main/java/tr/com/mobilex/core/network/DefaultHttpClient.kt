package tr.com.mobilex.core.network

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import okhttp3.Call
import okhttp3.Callback
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.utils.addHeaders
import tr.com.mobilex.core.utils.typeToken
import java.io.IOException
import java.io.Serializable

/**
 * Default implementation of the HttpClient service
 * This implementation uses OkHttp library to manage HTTP requests
 * @param client OkHttpClient instance to be used when making calls
 *          !!! OkHttpClient recommended to be a singleton object
 *          With this way, OkHttp can manage connection and thread pools better
 *          And device resources are not wasted on idle pools
 */
internal class DefaultHttpClient(private val client: OkHttpClient) : HttpClient {

    companion object {
        /**
         * Gson instance to convert json response to class instances
         */
        private val gson = Gson()
    }

    /**
     * Function to make HTTP Get requests
     * @param url URL of the website
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    override fun <T : Serializable> get(
        url: String,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    ) {
        val requestBuilder =
            getGenericRequestBuilder(url, queryParams, headers, onFailure) ?: return

        val getRequest = requestBuilder.get().build()
        makeCall(getRequest, onFailure, onResponse, classType)
    }

    /**
     * Function to make HTTP Get requests
     * @param url URL of the website
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param typeToken TypeToken of type T
     */
    override fun <T : Serializable> get(
        url: String,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        typeToken: TypeToken<T>
    ) {
        val requestBuilder =
            getGenericRequestBuilder(url, queryParams, headers, onFailure) ?: return

        val getRequest = requestBuilder.get().build()
        makeCall(getRequest, onFailure, onResponse, typeToken)
    }

    /**
     * Function to make HTTP Post requests
     * @param url URL of the website
     * @param jsonBody Json body that will be send with request
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    override fun <T : Serializable> post(
        url: String,
        jsonBody: String,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    ) {
        val body: RequestBody =
            jsonBody.toRequestBody(Constants.JSON_MEDIA_TYPE.toMediaTypeOrNull())
        val requestBuilder =
            getGenericRequestBuilder(url, queryParams, headers, onFailure) ?: return

        val postRequest = requestBuilder.post(body).build()
        makeCall(postRequest, onFailure, onResponse, classType)
    }

    /**
     * Function to make HTTP Put requests
     * @param url URL of the website
     * @param jsonBody Json body that will be send with request
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    override fun <T : Serializable> put(
        url: String,
        jsonBody: String,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    ) {
        val body: RequestBody =
            jsonBody.toRequestBody(Constants.JSON_MEDIA_TYPE.toMediaTypeOrNull())
        val requestBuilder =
            getGenericRequestBuilder(url, queryParams, headers, onFailure) ?: return

        val putRequest = requestBuilder.put(body).build()
        makeCall(putRequest, onFailure, onResponse, classType)
    }

    /**
     * Function to make HTTP Patch requests
     * @param url URL of the website
     * @param jsonBody Json body that will be send with request
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    override fun <T : Serializable> patch(
        url: String,
        jsonBody: String,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    ) {
        val body: RequestBody =
            jsonBody.toRequestBody(Constants.JSON_MEDIA_TYPE.toMediaTypeOrNull())
        val requestBuilder =
            getGenericRequestBuilder(url, queryParams, headers, onFailure) ?: return

        val patchRequest = requestBuilder.patch(body).build()
        makeCall(patchRequest, onFailure, onResponse, classType)
    }

    /**
     * Function to make HTTP Delete requests
     * @param url URL of the website
     * @param jsonBody Json body that will be send with request
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    override fun <T : Serializable> delete(
        url: String,
        jsonBody: String?,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    ) {
        val body: RequestBody? =
            jsonBody?.toRequestBody(Constants.JSON_MEDIA_TYPE.toMediaTypeOrNull())
        val requestBuilder =
            getGenericRequestBuilder(url, queryParams, headers, onFailure) ?: return

        val deleteRequest = requestBuilder.delete(body).build()
        makeCall(deleteRequest, onFailure, onResponse, classType)
    }

    /**
     * Creates a Request.Builder instance with common settings for all request types
     * @param url URL of the website
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     *
     * @return Request.Builder instance with common settings
     */
    private fun getGenericRequestBuilder(
        url: String,
        queryParams: Map<String, String?>?,
        headers: Map<String, String>?,
        onFailure: (Exception) -> Unit
    ): Request.Builder? {
        val fullUrl: String = getUrlWithParams(url, queryParams)

        var requestBuilder: Request.Builder? = null
        try {
            requestBuilder = Request.Builder().url(fullUrl).addHeaders(headers)
        } catch (exception: IllegalArgumentException) {
            onFailure.invoke(exception)
        }

        return requestBuilder
    }

    /**
     * Creates the final url injected with query params
     * @param url URL of the website
     * @param queryParams Query parameters that will be put in the url
     *
     * @return Full url of the website with query parameters
     */
    private fun getUrlWithParams(url: String, queryParams: Map<String, String?>?): String {
        val httpUrl: HttpUrl = url.toHttpUrlOrNull() ?: return ""

        val urlBuilder: HttpUrl.Builder = httpUrl.newBuilder()

        // If query parameters is not null, adds them to url
        queryParams?.let { params ->
            for (entry in params) {
                urlBuilder.addQueryParameter(entry.key, entry.value)
            }
        }

        return urlBuilder.build().toString()
    }

    /**
     * Makes an asynchronous call with given request and handles server response
     * @param request Request instance will be used when making call
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    private fun <T : Serializable> makeCall(
        request: Request,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    ) {
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                onFailure.invoke(e)
            }

            override fun onResponse(call: Call, response: Response) {
                handleResponse(response, onFailure, onResponse, classType)
            }
        })
    }

    /**
     * Makes an asynchronous call with given request and handles server response
     * @param request Request instance will be used when making call
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param typeToken TypeToken of type T
     */
    private fun <T : Serializable> makeCall(
        request: Request,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        typeToken: TypeToken<T>
    ) {
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                onFailure.invoke(e)
            }

            override fun onResponse(call: Call, response: Response) {
                handleResponse(response, onFailure, onResponse, typeToken)
            }
        })
    }

    /**
     * Handles response returned from the server
     * Response is not necessarily a successful one.
     * Server may return with an error code (For example; 401, 500 etc.)
     * @param response Response returned from the server
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    private fun <T : Serializable> handleResponse(
        response: Response,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    ) {
        if (response.isSuccessful &&
            response.code >= Constants.HTTP_OK &&
            response.code < Constants.HTTP_MULTIPLE_CHOICE
        ) {
            if (response.body == null) {
                onResponse.invoke(null)
            } else {
                try {
                    val result: T? = decodeResponse(response.body!!.string(), classType)
                    onResponse.invoke(result)
                } catch (exception: JsonSyntaxException) {
                    onFailure.invoke(exception)
                }
            }
        } else {
            onFailure.invoke(IOException("${response.code} : $response"))
        }
    }

    /**
     * Handles response returned from the server
     * Response is not necessarily a successful one.
     * Server may return with an error code (For example; 401, 500 etc.)
     * @param response Response returned from the server
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param typeToken TypeToken of type T
     */
    private fun <T : Serializable> handleResponse(
        response: Response,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        typeToken: TypeToken<T>
    ) {
        if (response.isSuccessful &&
            response.code >= Constants.HTTP_OK &&
            response.code < Constants.HTTP_MULTIPLE_CHOICE
        ) {
            if (response.body == null) {
                onResponse.invoke(null)
            } else {
                try {
                    val result: T? = decodeResponse(response.body!!.string(), typeToken)
                    onResponse.invoke(result)
                } catch (exception: JsonSyntaxException) {
                    onFailure.invoke(exception)
                }
            }
        } else {
            onFailure.invoke(IOException("${response.code} : $response"))
        }
    }

    /**
     * Deserializes response body to generic type T
     * @param responseBody Response body returned from server
     * @param classType Class object of type T
     *
     * @return Deserialized instance of T
     */
    @Suppress("UNCHECKED_CAST")
    private fun <T : Serializable> decodeResponse(
        responseBody: String,
        classType: Class<T>
    ): T? {
        val result: T? = if (classType == String::class.java) {
            responseBody as T
        } else {
            gson.fromJson(responseBody, classType)
        }

        return result
    }

    /**
     * Deserializes response body to generic type T
     * @param responseBody Response body returned from server
     * @param typeToken TypeToken of type T
     *
     * @return Deserialized instance of T
     */
    @Suppress("UNCHECKED_CAST")
    private fun <T : Serializable> decodeResponse(
        responseBody: String,
        typeToken: TypeToken<T>
    ): T? {
        val result: T? = if (typeToken == typeToken<String>()) {
            responseBody as T
        } else {
            gson.fromJson(responseBody, typeToken.type)
        }

        return result
    }
}
