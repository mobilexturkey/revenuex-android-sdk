package tr.com.mobilex.core.utils

import com.google.gson.reflect.TypeToken
import java.io.Serializable

/**
 * Gets type token for the generic type T
 *
 * @return TypeToken of generic type T
 */
internal inline fun <reified T : Serializable> typeToken(): TypeToken<T> {
    return object : TypeToken<T>() {}
}
