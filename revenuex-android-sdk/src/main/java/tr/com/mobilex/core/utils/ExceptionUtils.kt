package tr.com.mobilex.core.utils

/**
 * Gets message of the exception
 * or string representation of the exception
 * @return Message of the exception
 */
internal fun Exception.getExceptionMessage(): String {
    return this.localizedMessage ?: this.message ?: this.toString()
}
