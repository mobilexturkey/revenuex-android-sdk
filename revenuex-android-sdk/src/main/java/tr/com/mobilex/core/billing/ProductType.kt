package tr.com.mobilex.core.billing

import com.android.billingclient.api.BillingClient

/**
 * Enum values for SKU types
 */
enum class ProductType(val text: String) {
    InApp(BillingClient.SkuType.INAPP),
    Subs(BillingClient.SkuType.SUBS)
}

/**
 * Converts string to ProductType
 *
 * @return Converted product type.
 *     If string is not valid (subs or inapp), returns null
 */
internal fun String.toProductType(): ProductType? {
    return when {
        this == ProductType.Subs.text -> {
            ProductType.Subs
        }
        this == ProductType.InApp.text -> {
            ProductType.InApp
        }
        else -> {
            null
        }
    }
}

/**
 * Tells if product type is subscription
 *
 * @return True if product type is Subs or returns false
 */
internal fun ProductType.isSubscription(): Boolean {
    return this == ProductType.Subs
}
