package tr.com.mobilex.core.queue

/**
 * Queue structure to hold operations that will be executed later
 * @param O Function type for the operations that will be added to queue
 * @param D Data type that operations expect as parameter
 */
internal interface OperationQueue<O, D> where O : (D) -> Unit {

    /**
     * Size of the queue
     */
    val size: Int

    /**
     * Tells if the queue is empty or not
     * @return Returns true if queue size is zero
     */
    fun isEmpty(): Boolean

    /**
     * Adds a new operation to queue
     * @param operation New operation that will be added to queue
     *
     * @return If operation is added to queue successfully
     */
    fun add(operation: O): Boolean

    /**
     * Gets top operation from the queue that will be executed first
     * But does not remove the operation from queue
     *
     * @return Top operation of the queue
     */
    fun peek(): O?

    /**
     * Gets top operation from the queue that will be executed first
     * Removes the operation from queue
     *
     * @return Top operation of the queue
     */
    fun remove(): O

    /**
     * Clears all of the operations from queue
     */
    fun clear()
}
