package tr.com.mobilex.core.queue

import java.util.concurrent.LinkedBlockingQueue

/**
 * Queue structure to hold operations that will be executed later
 * @param O Function type for the operations that will be added to queue
 * @param D Data type that operations expect as parameter
 */
internal class ImplOperationQueue<O, D> : OperationQueue<O, D> where O : (D) -> Unit {

    private val operationQueue: LinkedBlockingQueue<O> = LinkedBlockingQueue()

    /**
     * Size of the queue
     */
    override val size: Int = operationQueue.size

    /**
     * Tells if the queue is empty or not
     * @return Returns true if queue size is zero
     */
    override fun isEmpty(): Boolean {
        return operationQueue.isEmpty()
    }

    /**
     * Adds a new operation to queue
     * @param operation New operation that will be added to queue
     *
     * @return If operation is added to queue successfully
     */
    override fun add(operation: O): Boolean {
        return operationQueue.add(operation)
    }

    /**
     * Gets top operation from the queue that will be executed first
     * But does not remove the operation from queue
     *
     * @return Top operation of the queue
     */
    override fun peek(): O? {
        return operationQueue.peek()
    }

    /**
     * Gets top operation from the queue that will be executed first
     * Removes the operation from queue
     *
     * @return Top operation of the queue
     */
    override fun remove(): O {
        return operationQueue.remove()
    }

    /**
     * Clears all of the operations from queue
     */
    override fun clear() {
        operationQueue.clear()
    }
}
