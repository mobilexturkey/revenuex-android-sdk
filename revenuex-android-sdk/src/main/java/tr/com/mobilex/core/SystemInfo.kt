package tr.com.mobilex.core

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ApplicationInfo
import android.os.Build
import android.provider.Settings
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.utils.getExceptionMessage
import tr.com.mobilex.core.utils.getLocale
import java.io.IOException
import java.lang.ref.WeakReference

/**
 * Class that provides information about the current application
 */
internal object SystemInfo {

    /**
     * Version of the RevenueX SDK
     * This value cannot get from context or android classes
     * !!! It must be updated when a new version is released
     */
    private const val SDK_VERSION = "0.1.0"

    /**
     * Application context provided by the developer when configuring SDK
     */
    private lateinit var context: WeakReference<Context>

    /**
     * Client id provided by the developer when configuring SDK
     */
    lateinit var clientId: String

    /**
     * User id either provided by the developer when configuring SDK
     * Or this might be generated on application startup for the first time
     */
    lateinit var revenueXUserId: String

    /**
     * Configures system info with the parameters provided from outside
     * @param context Application context provided by the developer when configuring SDK
     * @param clientId Client id provided by the developer when configuring SDK
     * @param revenueXUserId User id for the current instance of the application
     */
    fun configure(
        context: WeakReference<Context>,
        clientId: String,
        revenueXUserId: String
    ) {
        this.context = context
        this.clientId = clientId
        this.revenueXUserId = revenueXUserId
    }

    /**
     * Root url of the RevenueX SDK
     */
    val rootUrl: String
        get() = Constants.ROOT_URL

    /**
     * Platform which this client runs on (android)
     */
    val platform: String
        get() = Constants.ANDROID_PLATFORM

    /**
     * OS version which this client runs on (4.4.1, 11, etc.)
     */
    val platformVersion: String
        get() = Build.VERSION.RELEASE

    /**
     * Device name which this client runs on
     */
    val deviceName: String
        get() = Build.MODEL

    /**
     * Unique Id of the device
     */
    val androidId: String
        @SuppressLint("HardwareIds")
        get() {
            val contextRef = context.get() ?: return ""
            return Settings.Secure.getString(
                contextRef.contentResolver,
                Settings.Secure.ANDROID_ID
            ) ?: ""
        }

    /**
     * Play service advertisement id of the device
     */
    val gpsAdId: String
        get() {
            val contextRef = context.get() ?: return ""

            var advertisingID: String? = null
            try {
                val adInfo = AdvertisingIdClient.getAdvertisingIdInfo(contextRef)
                if (!adInfo.isLimitAdTrackingEnabled) {
                    advertisingID = adInfo.id
                }
            } catch (e: GooglePlayServicesNotAvailableException) {
                Logger.error(e.getExceptionMessage(), e)
            } catch (e: GooglePlayServicesRepairableException) {
                Logger.error(e.getExceptionMessage(), e)
            } catch (e: IOException) {
                Logger.error(e.getExceptionMessage(), e)
            } catch (e: Exception) {
                Logger.error(e.getExceptionMessage(), e)
            }
            return advertisingID ?: ""
        }

    /**
     * Version of the SDK
     */
    val sdkVersion: String
        get() {
            return SDK_VERSION
        }

    /**
     * Package id of the client application
     */
    val applicationId: String
        get() {
            val contextRef = context.get() ?: return ""
            return contextRef.applicationInfo.packageName
        }

    /**
     * Version of the client application
     */
    val applicationVersion: String
        get() {
            val contextRef = context.get() ?: return ""
            val packageName = contextRef.applicationInfo.packageName
            return contextRef.packageManager.getPackageInfo(packageName, 0).versionName
        }

    /**
     * If client application runs on debug mode or not
     */
    val isSandbox: Boolean
        get() {
            val contextRef = context.get() ?: return true
            return 0 != (contextRef.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE)
        }

    /**
     * Region of the device (TR, US, etc.)
     */
    val region: String
        get() {
            val contextRef = context.get() ?: return ""
            return contextRef.getLocale().country
        }

    /**
     * Language of the device (tr-TR, en-US, etc.)
     */
    val language: String
        get() {
            val contextRef = context.get() ?: return ""
            return "${contextRef.getLocale().language}-${contextRef.getLocale().country}"
        }
}
