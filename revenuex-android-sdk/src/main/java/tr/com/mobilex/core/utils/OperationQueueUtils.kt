package tr.com.mobilex.core.utils

import tr.com.mobilex.core.queue.OperationQueue

/**
 * Executes all pending operations with the same input
 * @param param Data that operations expect as parameter
 */
internal fun <D, O : (D) -> Unit> OperationQueue<O, D>.executePendingOperations(param: D) {
    while (isEmpty().not()) {
        remove().invoke(param)
    }
}
