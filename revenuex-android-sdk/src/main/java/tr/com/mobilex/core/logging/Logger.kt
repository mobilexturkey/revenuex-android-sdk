package tr.com.mobilex.core.logging

import android.util.Log
import tr.com.mobilex.core.Constants

/**
 * Logger that tags all messages with same tag
 */
internal object Logger {

    /**
     * Logs messages with debug level
     * @param message Message that will be logged
     * @param throwable Throwable instance that will be logged with message
     */
    fun debug(message: String, throwable: Throwable? = null) {
        Log.d(Constants.LOGGER_TAG, message, throwable)
    }

    /**
     * Logs messages with information level
     * @param message Message that will be logged
     * @param throwable Throwable instance that will be logged with message
     */
    fun info(message: String, throwable: Throwable? = null) {
        Log.i(Constants.LOGGER_TAG, message, throwable)
    }

    /**
     * Logs messages with warning level
     * @param message Message that will be logged
     * @param throwable Throwable instance that will be logged with message
     */
    fun warning(message: String, throwable: Throwable? = null) {
        Log.w(Constants.LOGGER_TAG, message, throwable)
    }

    /**
     * Logs messages with error level
     * @param message Message that will be logged
     * @param throwable Throwable instance that will be logged with message
     */
    fun error(message: String, throwable: Throwable? = null) {
        Log.e(Constants.LOGGER_TAG, message, throwable)
    }
}
