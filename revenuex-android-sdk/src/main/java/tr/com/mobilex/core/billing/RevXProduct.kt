package tr.com.mobilex.core.billing

import java.io.Serializable

/**
 * RevenueX product data model
 */
data class RevXProduct(
    val productId: String,
    val title: String,
    val description: String,
    val type: ProductType?,
    val freeTrialPeriod: String,
    val subscriptionPeriod: String,
    val price: String,
    val priceAmountMicros: Long,
    val priceCurrencyCode: String,
    val originalPrice: String,
    val originalPriceAmountMicros: Long,
    val introductoryPrice: String,
    val introductoryPriceAmountMicros: Long,
    val introductoryPricePeriod: String,
    val introductoryPriceCycles: Int,
    val iconUrl: String,
    val originalJson: String
) : Serializable
