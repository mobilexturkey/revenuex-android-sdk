package tr.com.mobilex.core.utils

import okhttp3.Request

/**
 * Adds list of headers to Request.Builder and returns Builder instance
 * @param headers Request headers that will be put in the request
 *
 * @return Same instance of the Builder
 */
internal fun Request.Builder.addHeaders(headers: Map<String, String>?): Request.Builder {
    // If request headers is not null, adds them to request
    headers?.let { requestHeaders ->
        for (header in requestHeaders) {
            addHeader(header.key, header.value)
        }
    }

    return this
}
