package tr.com.mobilex.core.billing

import java.io.Serializable

/**
 * RevenueX purchase history data model
 */
data class RevXPurchaseHistory(
    val purchaseToken: String,
    val signature: String,
    val originalJson: String,
    val payload: String,
    val type: ProductType,
    val purchaseTime: Long,
    val quantity: Int,
    val productIds: List<String>
) : Serializable
