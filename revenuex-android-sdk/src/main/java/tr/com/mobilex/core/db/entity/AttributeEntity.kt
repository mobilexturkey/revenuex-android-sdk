package tr.com.mobilex.core.db.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(
    tableName = AttributeEntity.TABLE_ATTRIBUTE_NAME,
    foreignKeys = [
        ForeignKey(
            entity = UserEntity::class,
            parentColumns = arrayOf(UserEntity.USER_ID),
            childColumns = arrayOf(AttributeEntity.ATTRIBUTE_USER_ID),
            onDelete = ForeignKey.CASCADE
        )
    ]
)
@Parcelize
data class AttributeEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    var userId: Long = 0,
    val name: String?,
    val value: String?
) : Parcelable {

    companion object {
        const val ATTRIBUTE_USER_ID = "userId"
        const val TABLE_ATTRIBUTE_NAME = "Attribute"
    }
}
