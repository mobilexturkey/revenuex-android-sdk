package tr.com.mobilex.core.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.db.dao.AttributeDao
import tr.com.mobilex.core.db.dao.UserDao
import tr.com.mobilex.core.db.entity.AttributeEntity
import tr.com.mobilex.core.db.entity.UserEntity
import tr.com.mobilex.core.utils.Converters

/**
 * Local database to store entities.
 */
@Database(
    entities = [
        UserEntity::class,
        AttributeEntity::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class RevXRoomDatabase : RoomDatabase() {

    /**
     * Provides User DAO to store application user
     */
    abstract fun userDao(): UserDao

    /**
     * Provides Attribute DAO to store user attributes
     */
    abstract fun attributeDao(): AttributeDao

    /**
     * Companion object to hold singleton database instance
     */
    companion object {
        // Singleton prevents multiple instances of database opening at the same time.
        @Volatile
        private var INSTANCE: RevXRoomDatabase? = null

        /**
         * Provides room database instance of RevenueX SDK
         */
        fun getDatabase(context: Context): RevXRoomDatabase {
            // If the INSTANCE is not null, then return it,
            // If it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room
                    .databaseBuilder(
                        context.applicationContext,
                        RevXRoomDatabase::class.java,
                        Constants.DATABASE_NAME
                    )
                    .fallbackToDestructiveMigration()
                    .addCallback(object : RoomDatabase.Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {
                            super.onCreate(db)

                            db.beginTransaction()

                            createDefaultUserEntity(db)

                            db.setTransactionSuccessful()
                            db.endTransaction()
                        }
                    })
                    .build()

                // Return instance
                INSTANCE = instance
                instance
            }
        }

        /**
         * Creates default user record when database is created
         */
        private fun createDefaultUserEntity(db: SupportSQLiteDatabase) {
            val contentValues = ContentValues()
            contentValues.apply {
                put(UserEntity.USER_ID, UserEntity.USER_DEFAULT_ID_VALUE)
                put(UserEntity.USER_APPLICATION_ID, UserEntity.USER_DEFAULT_APPLICATION_ID_VALUE)
                put(UserEntity.USER_REVENUEX_ID, UserEntity.USER_DEFAULT_REVENUEX_ID_VALUE)
                put(UserEntity.USER_CREATED_AT, UserEntity.USER_DEFAULT_CREATED_AT_VALUE)
                put(UserEntity.USER_UPDATED_AT, UserEntity.USER_DEFAULT_UPDATED_AT_VALUE)
                put(UserEntity.USER_LAST_SEEN_DATE, UserEntity.USER_DEFAULT_LAST_SEEN_DATE_VALUE)
            }
            db.insert(UserEntity.TABLE_USER_NAME, SQLiteDatabase.CONFLICT_IGNORE, contentValues)
        }
    }
}
