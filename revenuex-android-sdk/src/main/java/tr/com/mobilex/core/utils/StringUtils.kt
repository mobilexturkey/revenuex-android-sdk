package tr.com.mobilex.core.utils

/**
 * Concatenates two strings that contains url parts
 * @param lastPart Part that will be concatenated to first url
 * @return Concatenated url
 */
internal fun String.getConcatenatedUrl(lastPart: String = ""): String {
    val separator = if (this.last() == '/' || lastPart.first() == '/') "" else "/"
    return "$this$separator$lastPart"
}

/**
 * Converts Google Billing period to RevenueX period
 *
 * @return Converted RevenueX period.
 *     If string is not a valid period, returns null
 */
internal fun String.toRevXPeriod(): String? {
    return when (this) {
        "P1W" -> "weekly"
        "P1M" -> "monthly"
        "P2M" -> "twoMonth"
        "P3M" -> "threeMonth"
        "P6M" -> "sixMonth"
        "P1Y" -> "yearly"
        else -> null
    }
}
