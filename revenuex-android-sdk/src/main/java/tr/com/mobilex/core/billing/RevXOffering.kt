package tr.com.mobilex.core.billing

import java.io.Serializable

/**
 * RevenueX offering data model
 */
data class RevXOffering(
    val id: String,
    val identifier: String,
    val description: String,
    val isCurrent: Boolean,
    val packages: List<RevXPackage> = listOf()
) : Serializable

/**
 * RevenueX package of an offering data model
 */
data class RevXPackage(
    val id: String,
    val identifier: String,
    val description: String,
    val product: RevXProduct?
) : Serializable
