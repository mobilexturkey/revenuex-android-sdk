package tr.com.mobilex.core.sharedpref

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.google.gson.Gson
import tr.com.mobilex.core.Constants
import java.io.Serializable

/**
 * Singleton object to manage caching in EncryptedSharedPreferences
 */
internal object EncryptedSharedPrefManager {

    /**
     * Instance of secure shared preferences
     */
    private lateinit var sharedPreferences: SharedPreferences

    /**
     * Master key to encrypt shared preferences values
     */
    private lateinit var masterKey: MasterKey

    /**
     * Gson instance to convert objects to and from json string
     */
    private val gson = Gson()

    /**
     * Creates encrypted shared preferences instance
     * @param applicationContext Application context
     */
    fun createSecureSharedPreferences(applicationContext: Context) {
        masterKey = MasterKey.Builder(applicationContext, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
            .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
            .build()

        sharedPreferences = EncryptedSharedPreferences.create(
            applicationContext,
            Constants.SHARED_PREF_FILE_NAME,
            masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }

    /**
     * Caches value in encrypted shared preferences with given key
     * @param key Key for the data
     * @param value Data that will be saved in shared preferences
     */
    fun <T : Serializable> write(key: String, value: T) {
        with(sharedPreferences.edit()) {
            putString(key, gson.toJson(value))
            apply()
        }
    }

    /**
     * Gets data from encrypted shared preferences with given key
     * @param key Key to search data with
     * @param classType Class of the returned data
     *
     * @return An instance of class type, if data is found in shared preferences
     * Returns null if data is not found or data type does not match
     */
    fun <T : Serializable> read(key: String, classType: Class<T>): T? {
        val serializedObject = sharedPreferences.getString(key, null)
        if (serializedObject == null) return serializedObject

        return gson.fromJson(serializedObject, classType)
    }
}
