package tr.com.mobilex.core.utils

import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchaseHistoryRecord
import com.android.billingclient.api.SkuDetails
import tr.com.mobilex.core.billing.ProductType
import tr.com.mobilex.core.billing.RevXProduct
import tr.com.mobilex.core.billing.RevXPurchase
import tr.com.mobilex.core.billing.RevXPurchaseHistory
import tr.com.mobilex.core.billing.toProductType

/**
 * Converts Google billing history record to RevenueX history data model
 * @param type Product type which is bought with this history record
 *
 * @return RevenueX history data model
 */
internal fun PurchaseHistoryRecord.toRevXPurchaseHistory(type: ProductType): RevXPurchaseHistory {
    return RevXPurchaseHistory(
        purchaseToken,
        signature,
        originalJson,
        developerPayload,
        type,
        purchaseTime,
        quantity,
        skus
    )
}

/**
 * Converts Google billing purchase record to RevenueX purchase data model
 * @param type Product type which is bought with this purchase record
 *
 * @return RevenueX purchase data model
 */
internal fun Purchase.toRevXPurchase(type: ProductType): RevXPurchase {
    return RevXPurchase(
        this.purchaseToken,
        this.orderId,
        this.signature,
        this.originalJson,
        this.developerPayload,
        type,
        this.purchaseTime,
        this.quantity,
        this.skus,
        this.isAcknowledged,
        this.isAutoRenewing,
        this.purchaseState
    )
}

/**
 * Converts Google billing sku details to RevenueX product data model
 *
 * @return RevenueX product data model
 */
internal fun SkuDetails.toRevXProduct(): RevXProduct {
    return RevXProduct(
        this.sku,
        this.title,
        this.description,
        this.type.toProductType(),
        this.freeTrialPeriod,
        this.subscriptionPeriod,
        this.price,
        this.priceAmountMicros,
        this.priceCurrencyCode,
        this.originalPrice,
        this.originalPriceAmountMicros,
        this.introductoryPrice,
        this.introductoryPriceAmountMicros,
        this.introductoryPricePeriod,
        this.introductoryPriceCycles,
        this.iconUrl,
        this.originalJson
    )
}
