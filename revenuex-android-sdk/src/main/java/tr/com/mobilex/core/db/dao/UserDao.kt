package tr.com.mobilex.core.db.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import tr.com.mobilex.core.db.entity.UserEntity
import tr.com.mobilex.core.db.relational.UserWithAttributes

/**
 * DAO for application user
 * Handles CRUD operations with Room for application user
 */
@Dao
interface UserDao : BaseDao<UserEntity> {

    /**
     * Fetches users with it's attributes
     */
    @Transaction
    @Query("SELECT * FROM User")
    fun getUsersWithAttributes(): List<UserWithAttributes>

    /**
     * Clears User table
     */
    @Query("DELETE FROM User")
    fun clear()
}
