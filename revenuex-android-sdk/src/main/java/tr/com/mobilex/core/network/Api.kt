package tr.com.mobilex.core.network

import tr.com.mobilex.core.SystemInfo

/**
 * Interface for API classes to implement
 * Common parameters (like system info) or functions
 * must be added to this interface
 */
internal interface Api {

    /**
     * Provides information about the current configuration
     * Such as root url of the SDK and client id, etc.
     */
    val systemInfo: SystemInfo
}
