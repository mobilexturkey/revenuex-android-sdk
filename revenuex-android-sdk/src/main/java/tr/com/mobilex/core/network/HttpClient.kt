package tr.com.mobilex.core.network

import com.google.gson.reflect.TypeToken
import java.io.Serializable

/**
 * Service that handles network calls
 */
internal interface HttpClient {

    /**
     * Function to make HTTP Get requests
     * @param url URL of the website
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    fun <T : Serializable> get(
        url: String,
        queryParams: Map<String, String?>? = null,
        headers: Map<String, String>? = null,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    )

    /**
     * Function to make HTTP Get requests
     * @param url URL of the website
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param typeToken TypeToken of type T
     */
    fun <T : Serializable> get(
        url: String,
        queryParams: Map<String, String?>? = null,
        headers: Map<String, String>? = null,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        typeToken: TypeToken<T>
    )

    /**
     * Function to make HTTP Post requests
     * @param url URL of the website
     * @param jsonBody Json body that will be send with request
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    fun <T : Serializable> post(
        url: String,
        jsonBody: String,
        queryParams: Map<String, String?>? = null,
        headers: Map<String, String>? = null,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    )

    /**
     * Function to make HTTP Put requests
     * @param url URL of the website
     * @param jsonBody Json body that will be send with request
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    fun <T : Serializable> put(
        url: String,
        jsonBody: String,
        queryParams: Map<String, String?>? = null,
        headers: Map<String, String>? = null,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    )

    /**
     * Function to make HTTP Patch requests
     * @param url URL of the website
     * @param jsonBody Json body that will be send with request
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    fun <T : Serializable> patch(
        url: String,
        jsonBody: String,
        queryParams: Map<String, String?>? = null,
        headers: Map<String, String>? = null,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    )

    /**
     * Function to make HTTP Delete requests
     * @param url URL of the website
     * @param jsonBody Json body that will be send with request
     * @param queryParams Query parameters that will be put in the url
     * @param headers Request headers that will be put in the request
     * @param onFailure Callback function that will be triggered when request is failed
     * @param onResponse Callback function that will be triggered when request is successful
     *          Response will be converted to generic type T
     * @param classType Class object of type T
     */
    fun <T : Serializable> delete(
        url: String,
        jsonBody: String? = null,
        queryParams: Map<String, String?>? = null,
        headers: Map<String, String>? = null,
        onFailure: (Exception) -> Unit,
        onResponse: (T?) -> Unit,
        classType: Class<T>
    )
}
