package tr.com.mobilex.core.coroutine

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * Object that must be used instead of Dispatchers
 * This object can be mocked and return test dispatcher in unit tests
 */
internal object DispatcherProvider {

    val Default: CoroutineDispatcher = Dispatchers.Default

    val IO: CoroutineDispatcher = Dispatchers.IO

    val Main: CoroutineDispatcher = Dispatchers.Main

    val Unconfined: CoroutineDispatcher = Dispatchers.Unconfined
}
