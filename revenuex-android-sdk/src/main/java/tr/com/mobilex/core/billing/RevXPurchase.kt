package tr.com.mobilex.core.billing

import java.io.Serializable

/**
 * RevenueX purchase data model
 */
data class RevXPurchase(
    val purchaseToken: String,
    val orderId: String,
    val signature: String,
    val originalJson: String,
    val developerPayload: String,
    val type: ProductType,
    val purchaseTime: Long,
    val quantity: Int,
    val productIds: List<String>,
    val isAcknowledged: Boolean,
    val isAutoRenewing: Boolean,
    val purchaseState: Int
) : Serializable
