package tr.com.mobilex.core.billing

import android.app.Activity
import android.app.Application
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClientStateListener
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.BillingResult
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.PurchaseHistoryRecord
import com.android.billingclient.api.PurchasesUpdatedListener
import com.android.billingclient.api.SkuDetails
import com.android.billingclient.api.SkuDetailsParams
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.coroutine.DispatcherProvider
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.utils.toRevXProduct
import tr.com.mobilex.core.utils.toRevXPurchase
import tr.com.mobilex.core.utils.toRevXPurchaseHistory
import kotlin.math.min

/**
 * Implementation of the billing client manager
 * Fetches product details, purchase information, etc. from Google Billing
 */
internal class ImplBillingClientManager private constructor(
    application: Application,
    override var setupCompletionListener: SetupCompletionListener?,
    override var newPurchaseListener: NewPurchaseListener?
) : BillingClientManager,
    BillingClientStateListener,
    PurchasesUpdatedListener {

    /**
     * Companion object to get singleton instance of ImplBillingClientManager
     */
    companion object {

        @Volatile
        private var INSTANCE: ImplBillingClientManager? = null
        private val handler = Handler(Looper.getMainLooper())

        /**
         * Gets the instance of billing client manager
         * @param app Application instance
         * @param setupCompletionListener Listener for billing client setup completion
         * @param newPurchaseListener Listener for new purchases
         */
        fun getInstance(
            app: Application,
            setupCompletionListener: SetupCompletionListener? = null,
            newPurchaseListener: NewPurchaseListener? = null
        ): ImplBillingClientManager {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: ImplBillingClientManager(
                    app,
                    setupCompletionListener,
                    newPurchaseListener
                ).also { INSTANCE = it }
            }
        }
    }

    // Billing client instance to manage calls to Google Billing
    private val billingClient: BillingClient = BillingClient.newBuilder(application)
        .setListener(this)
        .enablePendingPurchases()
        .build()

    // How long before the data source tries to reconnect to Google billing client
    private var reconnectMilliseconds = Constants.RECONNECT_TIMER_START_MILLISECONDS

    // LiveData list of sku details
    private val skusWithSkuDetails: MutableLiveData<MutableMap<String, RevXProduct>> =
        MutableLiveData(mutableMapOf())

    // LiveData list of purchase history
    private val previousPurchases: MutableLiveData<MutableMap<String, RevXPurchaseHistory>> =
        MutableLiveData(mutableMapOf())

    // LiveData list of purchases
    private val currentPurchases: MutableLiveData<MutableMap<String, RevXPurchase>> =
        MutableLiveData(mutableMapOf())

    init {
        // Starting connection for billing client
        if (!billingClient.isReady) {
            Logger.info("BillingClient: Start connection...")
            billingClient.startConnection(this)
        }
    }

    /**
     * Callback function which is called when billing client setup is finished
     * But result code may be an unsuccessful code
     */
    override fun onBillingSetupFinished(billingResult: BillingResult) {
        val responseCode = billingResult.responseCode
        val debugMessage = billingResult.debugMessage
        Logger.debug("onBillingSetupFinished: responseCode: $responseCode message: $debugMessage")

        when (responseCode) {
            BillingClient.BillingResponseCode.OK -> {
                Logger.info("BillingClient: Connection established")
                reconnectMilliseconds = Constants.RECONNECT_TIMER_START_MILLISECONDS
                setupCompletionListener?.onSetupComplete()
            }
            else -> {
                // Connection must be retried in unsuccessful code case
                retryBillingServiceConnectionWithExponentialBackoff()
            }
        }
    }

    /**
     * Callback function which is called when there is a connection problem
     */
    override fun onBillingServiceDisconnected() {
        // Connection must be retried when there is a problem with connection
        retryBillingServiceConnectionWithExponentialBackoff()
    }

    /**
     * Calls the billing client to query product details
     * @param productIds Ids of the products
     * @param type Type of the products
     * @param callback Callback function that listens for product details
     */
    override fun queryProductDetails(
        productIds: List<String>,
        type: ProductType,
        callback: ((List<RevXProduct>) -> Unit)?
    ) {
        CoroutineScope(DispatcherProvider.IO).launch {
            val params = SkuDetailsParams.newBuilder()
                .setType(type.text)
                .setSkusList(productIds)
                .build()
            billingClient.querySkuDetailsAsync(params) { billingResult, skuDetailsList ->
                onSkuDetailsResponse(billingResult, skuDetailsList, type, callback)
            }
        }
    }

    /**
     * Gets previous purchases from billing client
     * @param type Type of the products
     * @param callback Callback function that listens for purchase history
     */
    override fun queryPurchaseHistory(
        type: ProductType,
        callback: ((List<RevXPurchaseHistory>) -> Unit)?
    ) {
        CoroutineScope(DispatcherProvider.IO).launch {
            billingClient.queryPurchaseHistoryAsync(type.text) { billingResult, purchaseHistoryList ->
                onPurchaseHistoryResponse(billingResult, purchaseHistoryList, type, callback)
            }
        }
    }

    /**
     * Gets active purchases from billing client
     * @param type Type of the products
     * @param callback Callback function that listens for purchases
     */
    override fun queryPurchases(
        type: ProductType,
        callback: ((List<RevXPurchase>) -> Unit)?
    ) {
        CoroutineScope(DispatcherProvider.IO).launch {
            billingClient.queryPurchasesAsync(type.text) { billingResult, purchaseList ->
                onQueryPurchasesResponse(billingResult, purchaseList, type, callback)
            }
        }
    }

    /**
     * Starts purchase flow for the given product
     * If an active subscription is updated,
     * old subscription can be provided as parameter
     * When purchase flow is completed,
     * new purchase can be listened by newPurchaseListener
     * @param activity Active activity to launch purchase flow from
     * @param product Product to be purchased
     * @param upgradedProduct Product that the subscription can be upgraded from
     */
    override fun purchaseProduct(
        activity: Activity,
        product: RevXProduct,
        upgradedProduct: RevXProduct?
    ) {
        val billingFlowParamsBuilder = BillingFlowParams.newBuilder()
            .setSkuDetails(SkuDetails(product.originalJson))

        CoroutineScope(DispatcherProvider.IO).launch {
            // If subscription is updated, sets required parameter
            val currentSubscription = findPurchaseForProduct(upgradedProduct)
            currentSubscription?.let {
                billingFlowParamsBuilder.setSubscriptionUpdateParams(
                    BillingFlowParams.SubscriptionUpdateParams.newBuilder()
                        .setOldSkuPurchaseToken(currentSubscription.purchaseToken)
                        .build()
                )
            }

            // Launches billing flow
            val result = billingClient.launchBillingFlow(activity, billingFlowParamsBuilder.build())
            if (result.responseCode != BillingClient.BillingResponseCode.OK) {
                Logger.error("Launch billing flow failed: ${result.debugMessage}")
            }
        }
    }

    /**
     * Callback function which is called when purchases are updated
     * Most possible reason is a new purchase is done
     * @param billingResult Result of the operation
     * @param purchaseList List of purchases
     */
    override fun onPurchasesUpdated(
        billingResult: BillingResult,
        purchaseList: MutableList<Purchase>?
    ) {
        val responseCode = billingResult.responseCode
        val debugMessage = billingResult.debugMessage

        when (responseCode) {
            BillingClient.BillingResponseCode.OK -> {
                Logger.debug("onPurchasesUpdated: responseCode: $responseCode message: $debugMessage")

                if (purchaseList.isNullOrEmpty()) {
                    Logger.error("onPurchasesUpdated: Empty Purchase List Returned from OK response!")
                } else {
                    // If purchase flow was successful, handle the result and inform listener
                    val lastPurchase = purchaseList.maxByOrNull { it.purchaseTime }!!
                    val productType = findProductTypeForPurchase(lastPurchase)
                    processPurchaseList(purchaseList, productType)
                    newPurchaseListener?.onNewPurchaseComplete(
                        lastPurchase.toRevXPurchase(productType)
                    )
                }
            }
            BillingClient.BillingResponseCode.USER_CANCELED -> Logger.info("onPurchasesUpdated: User canceled the purchase")
            BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED -> Logger.info("onPurchasesUpdated: The user already owns this item")
            BillingClient.BillingResponseCode.DEVELOPER_ERROR -> Logger.error("onPurchasesUpdated: Developer error thrown")
            else -> Logger.error("onPurchasesUpdated: responseCode: $responseCode message: $debugMessage")
        }
    }

    /**
     * Retries the billing service connection with exponential backoff,
     * maxing out at the time specified by RECONNECT_TIMER_MAX_TIME_MILLISECONDS
     */
    private fun retryBillingServiceConnectionWithExponentialBackoff() {
        handler.postDelayed(
            { billingClient.startConnection(this@ImplBillingClientManager) },
            reconnectMilliseconds
        )
        reconnectMilliseconds = min(
            reconnectMilliseconds * 2,
            Constants.RECONNECT_TIMER_MAX_TIME_MILLISECONDS
        )
    }

    /**
     * Handles queried sku details
     * @param billingResult Result of the sku details query
     * @param skuDetailsList List of sku details
     * @param type Type of the products
     * @param callback Callback function that listens for product details
     */
    private fun onSkuDetailsResponse(
        billingResult: BillingResult,
        skuDetailsList: MutableList<SkuDetails>?,
        type: ProductType,
        callback: ((List<RevXProduct>) -> Unit)?
    ) {
        val responseCode = billingResult.responseCode
        val debugMessage = billingResult.debugMessage

        when (responseCode) {
            BillingClient.BillingResponseCode.OK -> {
                val skuDetails = (skuDetailsList ?: listOf())
                    .map { it.toRevXProduct() }
                Logger.debug("onSkuDetailsResponse: responseCode: $responseCode message: $debugMessage")
                Logger.debug("onSkuDetailsResponse: Found ${skuDetails.size} SkuDetails as ${type.text}")

                // Returns product details and updates live data with new products
                callback?.invoke(skuDetails)
                val skuDetailsMap = skusWithSkuDetails.value ?: mutableMapOf()
                for (skuDetail in skuDetails) {
                    skuDetailsMap[skuDetail.productId] = skuDetail
                }
                skusWithSkuDetails.postValue(skuDetailsMap)
            }
            BillingClient.BillingResponseCode.SERVICE_DISCONNECTED,
            BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE,
            BillingClient.BillingResponseCode.BILLING_UNAVAILABLE,
            BillingClient.BillingResponseCode.ITEM_UNAVAILABLE,
            BillingClient.BillingResponseCode.DEVELOPER_ERROR,
            BillingClient.BillingResponseCode.ERROR,
            BillingClient.BillingResponseCode.USER_CANCELED,
            BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED,
            BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED,
            BillingClient.BillingResponseCode.ITEM_NOT_OWNED -> {
                // Logs the error and returns an empty list to callback
                Logger.error("onSkuDetailsResponse: responseCode: $responseCode message: $debugMessage")
                callback?.invoke(listOf())
            }
        }
    }

    /**
     * Handles queried purchase history records
     * @param billingResult Result of the purchase history query
     * @param purchaseHistoryList List of purchase history records
     * @param type Type of the products
     * @param callback Callback function that listens for purchase history
     */
    private fun onPurchaseHistoryResponse(
        billingResult: BillingResult,
        purchaseHistoryList: List<PurchaseHistoryRecord>?,
        type: ProductType,
        callback: ((List<RevXPurchaseHistory>) -> Unit)?
    ) {
        val responseCode = billingResult.responseCode
        val debugMessage = billingResult.debugMessage

        when (responseCode) {
            BillingClient.BillingResponseCode.OK -> {
                val purchaseHistory = (purchaseHistoryList ?: listOf())
                    .map { it.toRevXPurchaseHistory(type) }
                Logger.debug("onPurchaseHistoryResponse: responseCode: $responseCode message: $debugMessage")
                Logger.debug("onPurchaseHistoryResponse: Found ${purchaseHistory.size} purchase(s) as ${type.text}")

                // Returns purchase history and updates live data with new result
                callback?.invoke(purchaseHistory)
                val purchaseHistoryMap = previousPurchases.value ?: mutableMapOf()
                for (purchase in purchaseHistory) {
                    purchaseHistoryMap[purchase.purchaseToken] = purchase
                }
                previousPurchases.postValue(purchaseHistoryMap)
            }
            else -> {
                // Logs the error and returns an empty list to callback
                Logger.error("onPurchaseHistoryResponse: responseCode: $responseCode message: $debugMessage")
                callback?.invoke(listOf())
            }
        }
    }

    /**
     * Handles queried active purchase records
     * @param billingResult Result of the active purchases query
     * @param purchaseList List of active purchases
     * @param type Type of the products
     * @param callback Callback function that listens for purchases
     */
    private fun onQueryPurchasesResponse(
        billingResult: BillingResult,
        purchaseList: List<Purchase>,
        type: ProductType,
        callback: ((List<RevXPurchase>) -> Unit)?
    ) {
        val responseCode = billingResult.responseCode
        val debugMessage = billingResult.debugMessage

        when (responseCode) {
            BillingClient.BillingResponseCode.OK -> {
                Logger.debug("onQueryPurchasesResponse: responseCode: $responseCode message: $debugMessage")
                processPurchaseList(purchaseList, type, callback)
            }
            else -> {
                // Logs the error and returns an empty list to callback
                Logger.error("onQueryPurchasesResponse: responseCode: $responseCode message: $debugMessage")
                callback?.invoke(listOf())
            }
        }
    }

    /**
     * Gets active purchase for the given product
     * @param product Product for which the purchase information is queried
     *
     * @return Purchase information of the given product
     */
    private fun findPurchaseForProduct(product: RevXProduct?): RevXPurchase? {
        val result: RevXPurchase? = if (product == null) {
            null
        } else {
            currentPurchases.value?.values?.firstOrNull { it.productIds.contains(product.productId) }
        }

        return result
    }

    /**
     * Gets product type for the given purchase
     * @param purchase Purchase for which the product type is queried
     *
     * @return Product type of the given purchase
     */
    private fun findProductTypeForPurchase(purchase: Purchase?): ProductType {
        val result: ProductType? = if (purchase == null) {
            null
        } else {
            val sku = purchase.skus.first()
            skusWithSkuDetails.value?.get(sku)?.type
        }

        return result ?: ProductType.Subs
    }

    /**
     * Processes purchase list
     * @param purchaseList List of purchases
     * @param productType Type of the products
     * @param callback Callback function that listens for purchases
     */
    private fun processPurchaseList(
        purchaseList: List<Purchase>,
        productType: ProductType,
        callback: ((List<RevXPurchase>) -> Unit)? = null
    ) {
        val revXPurchases = purchaseList.map { it.toRevXPurchase(productType) }
        Logger.debug("processPurchaseList: Found ${purchaseList.size} purchase(s)")

        // Returns active purchases and updates live data with new result
        callback?.invoke(revXPurchases)
        val productMap = currentPurchases.value ?: mutableMapOf()
        for (purchase in revXPurchases) {
            productMap[purchase.orderId] = purchase
        }
        currentPurchases.postValue(productMap)
    }
}
