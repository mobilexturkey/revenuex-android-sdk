package tr.com.mobilex.core.identity

import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.sharedpref.EncryptedSharedPrefManager
import java.util.UUID

/**
 * Implementation of the identity manager
 * Decides which user id to use and caches it for later uses
 */
internal class ImplIdentityManager(
    private val securePreferences: EncryptedSharedPrefManager
) : IdentityManager {

    /**
     * Gets RevenueX user id provided by developer
     * If not provided, checks cache or creates a new one
     * @param revenueXUserId User id provided by the developer
     * @param onUserIdChanged Callback function that will be called if user id changed
     *
     * @return RevenueX user id
     */
    override fun getRevenueXUserId(
        revenueXUserId: String?,
        onUserIdChanged: ((newUserId: String, oldUserId: String) -> Unit)?
    ): String {
        var userId = securePreferences.read(Constants.REVENUEX_USER_ID_KEY, String::class.java)

        revenueXUserId?.let {
            if (userId != null && revenueXUserId != userId) {
                Logger.warning("Provided user id ($revenueXUserId) does not match with old user id ($userId).")
                onUserIdChanged?.invoke(revenueXUserId, userId!!)
            }
            userId = revenueXUserId
        }

        if (userId.isNullOrBlank()) {
            userId = UUID.randomUUID().toString().replace("-", "")
        }

        securePreferences.write(Constants.REVENUEX_USER_ID_KEY, userId!!)

        return userId!!
    }
}
