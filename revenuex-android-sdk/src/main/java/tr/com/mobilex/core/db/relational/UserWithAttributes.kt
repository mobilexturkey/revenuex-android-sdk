package tr.com.mobilex.core.db.relational

import androidx.room.Embedded
import androidx.room.Relation
import tr.com.mobilex.applicationuser.network.CurrentRevenueXUserDto
import tr.com.mobilex.core.db.entity.AttributeEntity
import tr.com.mobilex.core.db.entity.UserEntity
import tr.com.mobilex.core.utils.toDate

data class UserWithAttributes(
    @Embedded val user: UserEntity,
    @Relation(
        parentColumn = UserEntity.USER_ID,
        entityColumn = AttributeEntity.ATTRIBUTE_USER_ID
    )
    var attributes: List<AttributeEntity> = listOf()
) {

    internal fun update(user: CurrentRevenueXUserDto): UserWithAttributes {
        this.user.applicationId = user.applicationId
        this.user.revenueXId = user.revenueXId
        this.user.createdAt = user.createdAt?.toDate()
        this.user.updatedAt = user.updatedAt?.toDate()
        this.user.lastSeenDate = user.lastSeenDate?.toDate()

        val attributes: MutableList<AttributeEntity> = mutableListOf()
        user.attributes.forEach { attr ->
            val attributeEntity = AttributeEntity(
                name = attr.name,
                value = attr.value
            )
            attributes.add(attributeEntity)
        }
        this.attributes = attributes

        return this
    }
}
