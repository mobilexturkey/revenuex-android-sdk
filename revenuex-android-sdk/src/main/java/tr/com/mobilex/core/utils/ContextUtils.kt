package tr.com.mobilex.core.utils

import android.app.Application
import android.content.Context
import android.os.Build
import java.util.Locale

/**
 * Gets primary locale from the context
 * @return Primary locale of the device
 */
internal fun Context.getLocale(): Locale {
    val locale: Locale
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        locale = this.resources.configuration.locales[0]
    } else {
        @Suppress("DEPRECATION")
        locale = this.resources.configuration.locale
    }
    return locale
}

/**
 * Gets application from context
 */
internal val Context.application: Application
    get() = this.applicationContext as Application
