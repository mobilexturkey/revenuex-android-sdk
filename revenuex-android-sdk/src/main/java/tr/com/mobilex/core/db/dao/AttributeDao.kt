package tr.com.mobilex.core.db.dao

import androidx.room.Dao
import androidx.room.Query
import tr.com.mobilex.core.db.entity.AttributeEntity

/**
 * DAO for user attributes
 * Handles CRUD operations with Room for user attributes
 */
@Dao
interface AttributeDao : BaseDao<AttributeEntity> {

    /**
     * Clears Attribute table
     */
    @Query("DELETE FROM Attribute")
    fun clear()
}
