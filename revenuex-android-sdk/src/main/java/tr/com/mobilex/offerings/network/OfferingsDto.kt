package tr.com.mobilex.offerings.network

import tr.com.mobilex.core.billing.RevXOffering
import tr.com.mobilex.core.billing.RevXPackage
import tr.com.mobilex.core.billing.RevXProduct
import tr.com.mobilex.products.network.ProductPlatform
import java.io.Serializable

/**
 * Data model returned by the offerings api
 * Holds information about an offering
 * @param id Id of the offering in RevenueX
 * @param identifier Unique identifier of the offering
 * @param description Description of the offering
 * @param isCurrent If offering is the current one
 * @param packages Packages offered in the offering
 */
internal data class OfferingsDto(
    var id: String? = null,
    var identifier: String? = null,
    var description: String? = null,
    var isCurrent: Boolean? = true,
    var packages: List<PackageDto> = listOf()
) : Serializable

/**
 * Data model returned in the OfferingsDTO
 * Holds information about a package of an offering
 * @param id Id of the package in RevenueX
 * @param identifier Unique identifier of the package
 * @param description Description of the package
 * @param products Products offered in the package
 */
internal data class PackageDto(
    var id: String? = null,
    var identifier: String? = null,
    var description: String? = null,
    var products: List<ProductDto> = listOf()
) : Serializable

/**
 * Data model returned in the PackageDTO
 * Holds information about a product of a package
 * @param id Id of the product in RevenueX
 * @param source How this product is created (manuel/SDK)
 * @param identifier Unique identifier of the product
 * @param platform Platform this product is provided on (ios/android)
 */
internal data class ProductDto(
    var id: String? = null,
    var source: String? = null,
    var identifier: String? = null,
    var platform: String? = null
) : Serializable

/**
 * Converts Offerings DTO to RevenueX offering data model
 * @param products RevenueX products offered within the offering
 *
 * @return RevenueX offering data model
 */
internal fun OfferingsDto.toRevXOffering(products: List<RevXProduct>): RevXOffering {
    val revXPackages: MutableList<RevXPackage> = mutableListOf()

    // Loops through packages and converts to RevXPackage
    packages.forEach { packageDto ->
        // Gets first Android product offered in the package
        // (package should contain only one product per platform)
        val packageProduct = packageDto.products.firstOrNull {
            it.platform == ProductPlatform.Android.text
        } ?: return@forEach

        // Finds product information for the product
        val revXProduct = products.firstOrNull {
            it.productId == packageProduct.identifier
        } ?: return@forEach

        // Creates RevXPackage with information
        val revXPackage = RevXPackage(
            id = packageDto.id ?: "",
            identifier = packageDto.identifier ?: "",
            description = packageDto.description ?: "",
            product = revXProduct
        )
        revXPackages.add(revXPackage)
    }

    // Creates RevXOffering with information
    return RevXOffering(
        id = id ?: "",
        identifier = identifier ?: "",
        description = description ?: "",
        isCurrent = isCurrent ?: true,
        packages = revXPackages
    )
}
