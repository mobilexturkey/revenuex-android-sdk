package tr.com.mobilex.offerings.manager

import tr.com.mobilex.core.billing.RevXOffering

/**
 * Class to manage logics about offerings
 */
internal interface OfferingsManager {

    /**
     * List that holds offerings information
     */
    val offerings: List<RevXOffering>

    /**
     * Fetches offerings from RevenueX
     * @param onComplete Called when offerings fetched
     */
    fun fetchOfferings(onComplete: (() -> Unit)? = null)

    /**
     * Gets current offering from RevenueX
     * @param onResponse Callback function that will be triggered with current offering
     */
    fun getCurrentOffering(onResponse: (RevXOffering?) -> Unit)
}
