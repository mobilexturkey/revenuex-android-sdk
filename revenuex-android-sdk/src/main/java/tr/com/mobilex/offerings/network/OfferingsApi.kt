package tr.com.mobilex.offerings.network

import tr.com.mobilex.core.network.Api

/**
 * Offerings API interface
 */
internal interface OfferingsApi : Api {

    /**
     * Gets offerings from RevenueX
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when offerings retrieved
     *          This callback takes data model with offerings information
     */
    fun getOfferings(
        onFailure: (Exception) -> Unit,
        onResponse: (OfferingsDto?) -> Unit
    )
}
