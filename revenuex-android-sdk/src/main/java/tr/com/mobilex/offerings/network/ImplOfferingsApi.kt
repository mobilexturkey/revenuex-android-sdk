package tr.com.mobilex.offerings.network

import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import tr.com.mobilex.core.utils.getConcatenatedUrl
import tr.com.mobilex.core.utils.getExceptionMessage

/**
 * Class that manages calls to offerings API
 */
internal class ImplOfferingsApi(
    override val systemInfo: SystemInfo,
    private val httpClient: HttpClient
) : OfferingsApi {

    /**
     * Constant params
     */
    private val apiUrl = systemInfo.rootUrl
        .getConcatenatedUrl(Constants.OFFERINGS_API_URL)
    private val baseHeaders: Map<String, String> = mapOf(
        Constants.CLIENT_ID to systemInfo.clientId
    )

    /**
     * Gets offerings from RevenueX
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when offerings retrieved
     *          This callback takes data model with offerings information
     */
    override fun getOfferings(
        onFailure: (Exception) -> Unit,
        onResponse: (OfferingsDto?) -> Unit
    ) {
        // Prepares endpoint url
        val getOfferingsUrl = apiUrl.getConcatenatedUrl(Constants.OFFERINGS_API_GET_OFFERINGS)

        // Makes HTTP call to RevenueX API
        httpClient.get(
            url = getOfferingsUrl,
            headers = baseHeaders,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("OfferingsApi getOfferings server response: $response")
                onResponse.invoke(response)
            },
            classType = OfferingsDto::class.java
        )
    }
}
