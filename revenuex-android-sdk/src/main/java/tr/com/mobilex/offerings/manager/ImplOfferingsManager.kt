package tr.com.mobilex.offerings.manager

import tr.com.mobilex.core.billing.RevXOffering
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.queue.ImplOperationQueue
import tr.com.mobilex.core.queue.OperationQueue
import tr.com.mobilex.core.utils.executePendingOperations
import tr.com.mobilex.core.utils.getExceptionMessage
import tr.com.mobilex.offerings.network.OfferingsApi
import tr.com.mobilex.offerings.network.toRevXOffering
import tr.com.mobilex.products.manager.ProductsManager
import tr.com.mobilex.products.network.ProductPlatform

/**
 * Class to manage logics about offerings
 * For example; Fetching offerings from RevenueX,
 * finds prices and other product info from product manager, etc.
 */
internal class ImplOfferingsManager(
    private val offeringsApi: OfferingsApi,
    private val productsManager: ProductsManager
) : OfferingsManager {

    /**
     * Operation queue for the getCurrentOffering requests made before offerings fetched
     */
    private val operationQueue: OperationQueue<(List<RevXOffering>) -> Unit, List<RevXOffering>> =
        ImplOperationQueue()

    /**
     * Mutable list that holds offerings information
     */
    private var _offerings: List<RevXOffering>? = null
        set(value) {
            field = value

            // If offerings are not null (in valid cases not),
            // complete pending operations
            if (field != null) {
                operationQueue.executePendingOperations(field!!)
            }
        }

    /**
     * List that holds offerings information
     */
    override val offerings: List<RevXOffering>
        get() = _offerings ?: listOf()

    /**
     * Fetches offerings from RevenueX
     * @param onComplete Called when offerings fetched
     */
    override fun fetchOfferings(onComplete: (() -> Unit)?) {
        offeringsApi.getOfferings({
            Logger.error(it.getExceptionMessage(), it)
            _offerings = listOf()
            onComplete?.invoke()
        }, { response ->
            Logger.debug("getOfferings response: $response")

            // Finds android product ids in the offering
            var filteredIdList: List<String> = listOf()
            response?.let { offering ->
                filteredIdList = offering.packages.map { packageDto ->
                    // There is only one product per platform in a package
                    val androidProduct = packageDto.products.firstOrNull {
                        it.platform == ProductPlatform.Android.text &&
                            it.identifier.isNullOrBlank().not()
                    }
                    androidProduct?.identifier ?: ""
                }.filter { it.isBlank().not() }
            }
            Logger.debug("fetchOfferings filtered product ids: $filteredIdList")

            if (filteredIdList.isNullOrEmpty()) {
                _offerings = listOf()
                onComplete?.invoke()
                return@getOfferings
            }

            // Find products' information from product manager
            productsManager.getProducts(filteredIdList) { products ->
                _offerings = listOf(response!!.toRevXOffering(products))
                onComplete?.invoke()
            }
        })
    }

    /**
     * Gets current offering from RevenueX
     * @param onResponse Callback function that will be triggered with current offering
     */
    override fun getCurrentOffering(onResponse: (RevXOffering?) -> Unit) {
        operationQueue.add { offerings ->
            val currentOffering = offerings.firstOrNull { it.isCurrent }
            onResponse.invoke(currentOffering)
        }

        // If offerings are not null, immediately execute operation
        if (_offerings != null) {
            operationQueue.executePendingOperations(_offerings!!)
        }
    }
}
