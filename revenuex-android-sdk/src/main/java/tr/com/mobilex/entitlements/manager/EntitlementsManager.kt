package tr.com.mobilex.entitlements.manager

/**
 * Class to manage logics about entitlements
 */
internal interface EntitlementsManager {

    /**
     * List that holds entitlements
     */
    val entitlements: Map<String, Boolean>

    /**
     * Fetches entitlements from RevenueX
     * @param onComplete Called when entitlements fetched
     */
    fun fetchEntitlements(onComplete: (() -> Unit)? = null)

    /**
     * Checks if user is entitled with the given identifier
     * @param identifier Identifier of the entitlement
     *
     * @return If user is entitled with the given identifier
     */
    fun isEntitled(identifier: String): Boolean
}
