package tr.com.mobilex.entitlements.manager

import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.utils.getExceptionMessage
import tr.com.mobilex.entitlements.network.EntitlementsApi

/**
 * Class to manage logics about entitlements
 */
internal class ImplEntitlementsManager(
    private val entitlementsApi: EntitlementsApi
) : EntitlementsManager {

    /**
     * Mutable list that holds entitlements
     */
    private var _entitlements: MutableMap<String, Boolean>? = null

    /**
     * List that holds entitlements
     */
    override val entitlements: Map<String, Boolean>
        get() = _entitlements ?: mapOf()

    /**
     * Fetches entitlements from RevenueX
     * @param onComplete Called when entitlements fetched
     */
    override fun fetchEntitlements(onComplete: (() -> Unit)?) {
        entitlementsApi.getCurrentEntitlements({
            Logger.error(it.getExceptionMessage(), it)
            _entitlements = mutableMapOf()
            onComplete?.invoke()
        }, { response ->
            Logger.debug("fetchEntitlements response: $response")

            val filteredList = (response ?: listOf())
                .filter { it.identifier.isNullOrBlank().not() }
                .map { it.identifier!! }
            Logger.debug("fetchEntitlements filtered entitlement ids: $filteredList")

            if (filteredList.isNullOrEmpty()) {
                _entitlements = mutableMapOf()
                onComplete?.invoke()
                return@getCurrentEntitlements
            }

            _entitlements = mutableMapOf()
            filteredList.forEach {
                _entitlements!![it] = true
            }
            onComplete?.invoke()
        })
    }

    /**
     * Checks if user is entitled with the given identifier
     * @param identifier Identifier of the entitlement
     *
     * @return If user is entitled with the given identifier
     */
    override fun isEntitled(identifier: String): Boolean {
        return entitlements[identifier] ?: false
    }
}
