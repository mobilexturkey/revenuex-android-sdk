package tr.com.mobilex.entitlements.network

import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import tr.com.mobilex.core.utils.getConcatenatedUrl
import tr.com.mobilex.core.utils.getExceptionMessage
import tr.com.mobilex.core.utils.typeToken

/**
 * Class that manages calls to entitlements API
 */
internal class ImplEntitlementsApi(
    override val systemInfo: SystemInfo,
    private val httpClient: HttpClient
) : EntitlementsApi {

    /**
     * Constant params
     */
    private val apiUrl = systemInfo.rootUrl
        .getConcatenatedUrl(Constants.ENTITLEMENTS_API_URL)
    private val baseHeaders: Map<String, String> = mapOf(
        Constants.CLIENT_ID to systemInfo.clientId,
        Constants.USER_REVENUEX_ID to systemInfo.revenueXUserId
    )

    /**
     * Gets entitlements from RevenueX
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when entitlements retrieved
     */
    override fun getCurrentEntitlements(
        onFailure: (Exception) -> Unit,
        onResponse: (List<EntitlementDto>?) -> Unit
    ) {
        // Prepares endpoint url
        val currentEntitlementsUrl = apiUrl
            .getConcatenatedUrl(Constants.ENTITLEMENTS_API_GET_CURRENT)

        // Makes HTTP call to RevenueX API
        httpClient.get(
            url = currentEntitlementsUrl,
            headers = baseHeaders,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("EntitlementsApi getCurrentEntitlements server response: $response")
                onResponse.invoke(response)
            },
            typeToken = typeToken<ArrayList<EntitlementDto>>()
        )
    }
}
