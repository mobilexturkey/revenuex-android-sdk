package tr.com.mobilex.entitlements.network

import tr.com.mobilex.core.network.Api

/**
 * Entitlements API interface
 */
internal interface EntitlementsApi : Api {

    /**
     * Gets entitlements from RevenueX
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when entitlements retrieved
     */
    fun getCurrentEntitlements(
        onFailure: (Exception) -> Unit,
        onResponse: (List<EntitlementDto>?) -> Unit
    )
}
