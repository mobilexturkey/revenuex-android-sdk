package tr.com.mobilex.entitlements.network

import java.io.Serializable

/**
 * Data model returned by the entitlements api
 * Holds information about entitlements
 * @param id Id of the entitlement in RevenueX
 * @param identifier Unique identifier of the entitlement
 * @param description Description of the entitlement
 * @param products Products needed to have this entitlement
 */
internal data class EntitlementDto(
    var id: String? = null,
    var identifier: String? = null,
    var description: String? = null,
    var products: List<String> = listOf()
) : Serializable
