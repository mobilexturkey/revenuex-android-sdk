package tr.com.mobilex.products.network

import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import tr.com.mobilex.core.utils.getConcatenatedUrl
import tr.com.mobilex.core.utils.getExceptionMessage

/**
 * Class that manages calls to products API
 */
internal class ImplProductsApi(
    override val systemInfo: SystemInfo,
    private val httpClient: HttpClient
) : ProductsApi {

    /**
     * Constant params
     */
    private val apiUrl = systemInfo.rootUrl
        .getConcatenatedUrl(Constants.PRODUCTS_API_URL)
    private val baseHeaders: Map<String, String> = mapOf()

    /**
     * Gets all products from RevenueX
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when products retrieved
     */
    override fun getAll(
        onFailure: (Exception) -> Unit,
        onResponse: (Array<ProductInfoDto>?) -> Unit
    ) {
        // Prepares endpoint url and headers for the request
        val allProductsUrl = apiUrl.getConcatenatedUrl(Constants.PRODUCTS_API_ALL)
        val headers = baseHeaders.toMutableMap().also { newMap ->
            newMap[Constants.CLIENT_ID] = systemInfo.clientId
        }

        // Makes HTTP call to RevenueX API
        httpClient.get(
            url = allProductsUrl,
            headers = headers,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("ProductsApi getAll server response: ${response.contentToString()}")
                onResponse.invoke(response)
            },
            classType = Array<ProductInfoDto>::class.java
        )
    }
}
