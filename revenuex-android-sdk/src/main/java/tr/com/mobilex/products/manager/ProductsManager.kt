package tr.com.mobilex.products.manager

import tr.com.mobilex.core.billing.ProductType
import tr.com.mobilex.core.billing.RevXProduct

/**
 * Class to manage logics about products
 * For example; Fetching product ids from RevenueX,
 * fetching prices and other product info from billing, etc.
 */
internal interface ProductsManager {

    /**
     * List that holds products information
     */
    val products: List<RevXProduct>

    /**
     * Fetches products from RevenueX
     * @param onComplete Called when products fetched
     */
    fun fetchProducts(onComplete: (() -> Unit)? = null)

    /**
     * Gets products with given type
     * @param type Type of the products queried
     * @param onResponse Callback function that will be triggered with products
     */
    fun getProducts(
        type: ProductType = ProductType.Subs,
        onResponse: (List<RevXProduct>) -> Unit
    )

    /**
     * Gets products with given ids
     * @param productIds Product ids that will be queried
     * @param onResponse Callback function that will be triggered with products
     */
    fun getProducts(
        productIds: List<String>,
        onResponse: (List<RevXProduct>) -> Unit
    )
}
