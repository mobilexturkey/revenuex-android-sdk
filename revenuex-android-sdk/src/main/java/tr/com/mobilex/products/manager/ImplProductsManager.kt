package tr.com.mobilex.products.manager

import tr.com.mobilex.core.billing.ProductInformationProvider
import tr.com.mobilex.core.billing.ProductType
import tr.com.mobilex.core.billing.RevXProduct
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.queue.ImplOperationQueue
import tr.com.mobilex.core.queue.OperationQueue
import tr.com.mobilex.core.utils.executePendingOperations
import tr.com.mobilex.core.utils.getExceptionMessage
import tr.com.mobilex.products.network.ProductPlatform
import tr.com.mobilex.products.network.ProductsApi

/**
 * Class to manage logics about products
 * For example; Fetching product ids from RevenueX,
 * fetching prices and other product info from billing, etc.
 */
internal class ImplProductsManager(
    private val productsApi: ProductsApi,
    private val productInformationProvider: ProductInformationProvider
) : ProductsManager {

    /**
     * Operation queue for the getProducts requests made before products fetched
     */
    private val operationQueue: OperationQueue<(List<RevXProduct>) -> Unit, List<RevXProduct>> =
        ImplOperationQueue()

    /**
     * Mutable list that holds products information
     */
    private var _products: List<RevXProduct>? = null
        set(value) {
            field = value

            // If products are not null (in valid cases not),
            // complete pending operations
            if (field != null) {
                operationQueue.executePendingOperations(field!!)
            }
        }

    /**
     * List that holds products information
     */
    override val products: List<RevXProduct>
        get() = _products ?: listOf()

    /**
     * Fetches products from RevenueX
     * @param onComplete Called when products fetched
     */
    override fun fetchProducts(onComplete: (() -> Unit)?) {
        productsApi.getAll({
            Logger.error(it.getExceptionMessage(), it)
            _products = listOf()
            onComplete?.invoke()
        }, { response ->
            Logger.debug("fetchProducts response: ${response.contentToString()}")

            val filteredList = (response ?: arrayOf())
                .filter {
                    it.platform == ProductPlatform.Android.text &&
                        it.productId.isNullOrBlank().not()
                }.map { it.productId!! }
            Logger.debug("fetchProducts filtered product ids: $filteredList")

            if (filteredList.isNullOrEmpty()) {
                _products = listOf()
                onComplete?.invoke()
                return@getAll
            }

            productInformationProvider
                .queryProductDetails(filteredList, ProductType.Subs) { subscriptions ->
                    productInformationProvider
                        .queryProductDetails(filteredList, ProductType.InApp) { inApps ->
                            val allProducts = subscriptions + inApps
                            Logger.debug("All products: $allProducts")

                            _products = allProducts
                            onComplete?.invoke()
                        }
                }
        })
    }

    /**
     * Gets products with given type
     * @param type Type of the products queried
     * @param onResponse Callback function that will be triggered with products
     */
    override fun getProducts(
        type: ProductType,
        onResponse: (List<RevXProduct>) -> Unit
    ) {
        operationQueue.add { products ->
            val filteredProducts = products.filter { it.type == type }
            onResponse.invoke(filteredProducts)
        }

        // If products are not null, immediately execute operation
        if (_products != null) {
            operationQueue.executePendingOperations(_products!!)
        }
    }

    /**
     * Gets products with given ids
     * @param productIds Product ids that will be queried
     * @param onResponse Callback function that will be triggered with products
     */
    override fun getProducts(
        productIds: List<String>,
        onResponse: (List<RevXProduct>) -> Unit
    ) {
        operationQueue.add { products ->
            val filteredProducts = products.filter { productIds.contains(it.productId) }
            onResponse.invoke(filteredProducts)
        }

        // If products are not null, immediately execute operation
        if (_products != null) {
            operationQueue.executePendingOperations(_products!!)
        }
    }
}
