package tr.com.mobilex.products.network

/**
 * Enum values for product platforms
 */
internal enum class ProductPlatform(val text: String) {
    Android("android"),
    IOS("ios")
}
