package tr.com.mobilex.products.network

import tr.com.mobilex.core.network.Api

/**
 * Products API interface
 */
internal interface ProductsApi : Api {

    /**
     * Gets all products from RevenueX
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when products retrieved
     */
    fun getAll(
        onFailure: (Exception) -> Unit,
        onResponse: (Array<ProductInfoDto>?) -> Unit
    )
}
