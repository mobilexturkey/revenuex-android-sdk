package tr.com.mobilex.products.network

import java.io.Serializable

/**
 * Data model returned by the products api
 * Holds information about a product
 * @param id Id of the product in RevenueX
 * @param productId Id of the product in Play Store
 * @param platform Platform this product is defined for
 * @param application Application id this product is defined for
 */
internal data class ProductInfoDto(
    var id: String? = null,
    var productId: String? = null,
    var platform: String? = null,
    var application: String? = null
) : Serializable
