package tr.com.mobilex

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.annotation.XmlRes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import tr.com.mobilex.applications.network.ApplicationsApi
import tr.com.mobilex.applications.network.ImplApplicationsApi
import tr.com.mobilex.applicationuser.manager.ApplicationUserManager
import tr.com.mobilex.applicationuser.manager.ImplApplicationUserManager
import tr.com.mobilex.applicationuser.network.ImplApplicationUserApi
import tr.com.mobilex.applicationuser.network.UserAttributes
import tr.com.mobilex.config.manager.ImplRemoteConfigManager
import tr.com.mobilex.config.manager.RemoteConfigManager
import tr.com.mobilex.config.manager.getRemoteConfig
import tr.com.mobilex.config.network.ImplRemoteConfigApi
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.billing.BillingClientManager
import tr.com.mobilex.core.billing.ImplBillingClientManager
import tr.com.mobilex.core.billing.ProductType
import tr.com.mobilex.core.billing.RevXOffering
import tr.com.mobilex.core.billing.RevXProduct
import tr.com.mobilex.core.billing.RevXPurchase
import tr.com.mobilex.core.billing.SetupCompletionListener
import tr.com.mobilex.core.coroutine.DispatcherProvider
import tr.com.mobilex.core.db.RevXRoomDatabase
import tr.com.mobilex.core.db.dao.AttributeDao
import tr.com.mobilex.core.db.dao.UserDao
import tr.com.mobilex.core.identity.IdentityManager
import tr.com.mobilex.core.identity.ImplIdentityManager
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.DefaultHttpClient
import tr.com.mobilex.core.network.HttpClient
import tr.com.mobilex.core.sharedpref.EncryptedSharedPrefManager
import tr.com.mobilex.core.utils.application
import tr.com.mobilex.entitlements.manager.EntitlementsManager
import tr.com.mobilex.entitlements.manager.ImplEntitlementsManager
import tr.com.mobilex.entitlements.network.ImplEntitlementsApi
import tr.com.mobilex.offerings.manager.ImplOfferingsManager
import tr.com.mobilex.offerings.manager.OfferingsManager
import tr.com.mobilex.offerings.network.ImplOfferingsApi
import tr.com.mobilex.products.manager.ImplProductsManager
import tr.com.mobilex.products.manager.ProductsManager
import tr.com.mobilex.products.network.ImplProductsApi
import tr.com.mobilex.transactions.manager.ImplTransactionsManager
import tr.com.mobilex.transactions.manager.TransactionsManager
import tr.com.mobilex.transactions.network.ImplTransactionsApi
import java.lang.ref.WeakReference

object Revenuex : SetupCompletionListener, Application.ActivityLifecycleCallbacks {

    /**
     * OkHttpClient recommended to be a singleton object
     * With this way, OkHttp can manage connection and thread pools better
     * And device resources are not wasted on idle pools
     */
    private val client: OkHttpClient = OkHttpClient()
    private val httpClient: HttpClient = DefaultHttpClient(client)

    /**
     * Tells if RevenueX will manage billing operation
     * or just listen for native purchase events
     */
    private var observerMode: Boolean = false

    /**
     * Application context provided by the developer when configuring SDK
     */
    private lateinit var context: WeakReference<Context>

    /**
     * Room database instance
     */
    private lateinit var revXDatabase: RevXRoomDatabase

    /**
     * Billing client manager instance to fetch products
     * and launch purchase flows
     */
    private lateinit var billingManager: BillingClientManager

    /**
     * Identity manager to get user identifier
     */
    private val identityManager: IdentityManager by lazy {
        ImplIdentityManager(EncryptedSharedPrefManager)
    }

    /**
     * User DAO to make CRUD operations on User table
     */
    private val userDao: UserDao by lazy {
        revXDatabase.userDao()
    }

    /**
     * Attribute DAO to make CRUD operations on Attribute table
     */
    private val attributeDao: AttributeDao by lazy {
        revXDatabase.attributeDao()
    }

    /**
     * Implementation of application user manager
     * Manages calls made about the application user
     */
    private val applicationUserManager: ApplicationUserManager by lazy {
        val applicationUserApi = ImplApplicationUserApi(SystemInfo, httpClient)
        ImplApplicationUserManager(SystemInfo, applicationUserApi, userDao, attributeDao)
    }

    /**
     * Implementation of applications api
     * Manages calls made about the applications
     */
    private val applicationsApi: ApplicationsApi by lazy {
        ImplApplicationsApi(SystemInfo, httpClient)
    }

    /**
     * Implementation of products manager
     * Manages calls made about the products
     */
    private val productsManager: ProductsManager by lazy {
        val productsApi = ImplProductsApi(SystemInfo, httpClient)
        ImplProductsManager(productsApi, billingManager)
    }

    /**
     * Implementation of offerings manager
     * Manages calls made about the offerings
     */
    private val offeringsManager: OfferingsManager by lazy {
        val offeringsApi = ImplOfferingsApi(SystemInfo, httpClient)
        ImplOfferingsManager(offeringsApi, productsManager)
    }

    /**
     * Implementation of entitlements manager
     * Manages calls made about the entitlements
     */
    private val entitlementsManager: EntitlementsManager by lazy {
        val entitlementsApi = ImplEntitlementsApi(SystemInfo, httpClient)
        ImplEntitlementsManager(entitlementsApi)
    }

    /**
     * Implementation of transaction manager
     * Manages calls made about purchases
     * For example; Fetching purchase history, buying new products, etc.
     */
    private val transactionsManager: TransactionsManager by lazy {
        val transactionsApi = ImplTransactionsApi(SystemInfo, httpClient)
        ImplTransactionsManager(
            transactionsApi,
            billingManager,
            productsManager,
            entitlementsManager,
            EncryptedSharedPrefManager,
            observerMode
        )
    }

    /**
     * Implementation of remote config manager
     * Manages calls made about the remote configs
     */
    private val configManager: RemoteConfigManager by lazy {
        val configApi = ImplRemoteConfigApi(SystemInfo, httpClient)
        ImplRemoteConfigManager(configApi, context.get()!!)
    }

    /**
     * Configures RevenueX SDK to manage purchases
     * @param context Application context to access necessary information
     * @param clientId Client id from the RevenueX portal
     * @param revenueXUserId User id for the current instance of the application
     *          If not provided, RevenueX will generate id for the current device
     * @param observerMode Tells if RevenueX will manage billing operation
     *          or just listen for native purchase events
     */
    fun init(
        context: Context,
        clientId: String,
        revenueXUserId: String? = null,
        observerMode: Boolean = false
    ) {
        this.context = WeakReference(context)
        this.observerMode = observerMode
        // Initializes room database
        revXDatabase = RevXRoomDatabase.getDatabase(context)
        // Initializes billing client manager
        billingManager = ImplBillingClientManager.getInstance(context.application, this)
        // Creates secure shared preferences
        EncryptedSharedPrefManager.createSecureSharedPreferences(context)

        // Configures system info; some information needs context to be read
        // and client id must be provided by the application developer
        val userId = identityManager.getRevenueXUserId(revenueXUserId) { newUserId, oldUserId ->
            // Create alias between user ids
            createAlias(newUserId, oldUserId)
        }
        SystemInfo.configure(WeakReference(context), clientId, userId)
        Logger.warning(
            "Library is initialized with the following customer id: $clientId. " +
                "Please be sure to add correct consumer id to use our library."
        )

        startRevenueX()
        context.application.registerActivityLifecycleCallbacks(this)
    }

    /**
     * Called when billing client setup is completed
     */
    override fun onSetupComplete() {
        // Fetch products from RevenueX
        fetchProducts {
            // Fetch offerings from RevenueX
            fetchOfferings()
            // Sends old purchases to RevenueX
            sendPurchaseHistory()
            // Sends active purchases to RevenueX
            sendActivePurchases()
        }
    }

    /**
     * Login with a new user id
     * @param newUserId New user id
     */
    fun setRevenueXUserId(newUserId: String) {
        // If new user id is same with old user id, no need to create alias
        if (newUserId == SystemInfo.revenueXUserId) return

        // Create alias between user ids and update SystemInfo
        createAlias(newUserId, SystemInfo.revenueXUserId)
        SystemInfo.revenueXUserId = newUserId
    }

    /**
     * Collects device identifiers and sets on user attributes
     */
    fun collectDeviceIdentifiers() {
        CoroutineScope(DispatcherProvider.IO).launch {
            applicationUserManager.setAttributes(
                mapOf(
                    UserAttributes.AndroidId to SystemInfo.androidId,
                    UserAttributes.GpsAdId to SystemInfo.gpsAdId
                )
            )
        }
    }

    /**
     * Sets user attributes on current user and syncs with RevenueX
     * Unchanged parameters not needed to be sent
     * @param attributes User attributes map
     */
    fun setAttributes(attributes: Map<UserAttributes, String>) {
        applicationUserManager.setAttributes(attributes)
    }

    /**
     * Gets products information from RevenueX
     */
    fun getProducts(type: ProductType, onComplete: (List<RevXProduct>) -> Unit) {
        productsManager.getProducts(type, onComplete)
    }

    /**
     * Gets current offerings from RevenueX
     */
    fun getCurrentOffering(onComplete: (RevXOffering?) -> Unit) {
        offeringsManager.getCurrentOffering(onComplete)
    }

    /**
     * Checks if user is entitled with the given identifier
     * @param identifier Identifier of the entitlement
     *
     * @return If user is entitled with the given identifier
     */
    fun isEntitled(identifier: String): Boolean {
        return entitlementsManager.isEntitled(identifier)
    }

    /**
     * Sets default values from XML resource file
     * @param resourceId Id of the XML file
     */
    fun setConfigDefaultsAsync(@XmlRes resourceId: Int) {
        configManager.setConfigDefaultsAsync(resourceId)
    }

    /**
     * Gets value of the remote config with given key
     * @param key Key of the config
     * @param defaultValue Default value that will be returned if remote config is missing
     *
     * @return Value of the remote config
     */
    fun getStringConfig(
        key: String,
        defaultValue: String? = null
    ): String? {
        return configManager.getRemoteConfig(key, defaultValue)
    }

    /**
     * Gets value of the remote config with given key
     * @param key Key of the config
     * @param defaultValue Default value that will be returned if remote config is missing
     *
     * @return Value of the remote config
     */
    fun getDoubleConfig(
        key: String,
        defaultValue: Double? = null
    ): Double? {
        return configManager.getRemoteConfig(key, defaultValue)
    }

    /**
     * Gets value of the remote config with given key
     * @param key Key of the config
     * @param defaultValue Default value that will be returned if remote config is missing
     *
     * @return Value of the remote config
     */
    fun getBooleanConfig(
        key: String,
        defaultValue: Boolean? = null
    ): Boolean? {
        return configManager.getRemoteConfig(key, defaultValue)
    }

    /**
     * Starts purchase flow for given product
     * @param activity Active activity to launch purchase flow from
     * @param product Product to be purchased
     * @param upgradedProduct Product that the subscription will be upgraded from
     * @param callback Callback function that will be triggered when product is purchased
     */
    fun purchaseProduct(
        activity: Activity,
        product: RevXProduct,
        upgradedProduct: RevXProduct? = null,
        callback: ((RevXPurchase) -> Unit)? = null
    ) {
        transactionsManager.purchaseProduct(activity, product, upgradedProduct, callback)
    }

    /**
     * Completes operation that needs to be done on application startup
     */
    private fun startRevenueX() {
        // Sends open event to RevenueX
        openEvent {
            // Open event is sent
        }
        // Fetch remote configs from RevenueX
        fetchRemoteConfigs()
        // Fetch entitlements from RevenueX
        fetchEntitlements()
    }

    /**
     * Calls open event from RevenueX and gets RevenueX user information
     * @param onComplete Called when open event is send to RevenueX
     */
    private fun openEvent(onComplete: () -> Unit) {
        applicationUserManager.openEvent(onComplete)
    }

    /**
     * Fetches remote configs from RevenueX
     */
    private fun fetchRemoteConfigs() {
        configManager.fetchRemoteConfigs()
    }

    /**
     * Fetches entitlements from RevenueX
     */
    private fun fetchEntitlements() {
        entitlementsManager.fetchEntitlements()
    }

    /**
     * Fetches products from RevenueX
     * @param onComplete Called when products fetched
     */
    private fun fetchProducts(onComplete: () -> Unit) {
        productsManager.fetchProducts(onComplete)
    }

    /**
     * Fetches offerings from RevenueX
     */
    private fun fetchOfferings() {
        offeringsManager.fetchOfferings()
    }

    /**
     * Sends old purchases to RevenueX
     */
    private fun sendPurchaseHistory() {
        transactionsManager.sendPurchaseHistory()
    }

    /**
     * Sends active purchases to RevenueX
     */
    private fun sendActivePurchases() {
        transactionsManager.sendActivePurchases()
    }

    /**
     * Creates alias between the new user id and old user id
     * @param newUserId New user id
     * @param oldUserId Old user id
     */
    private fun createAlias(newUserId: String, oldUserId: String) {
        applicationUserManager.createAlias(newUserId, oldUserId)
    }

    /**
     * Called when the Activity calls super.onCreate()
     */
    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        // Do nothing
    }

    /**
     * Called when the Activity calls super.onStart()
     */
    override fun onActivityStarted(activity: Activity) {
        transactionsManager.executePendingOperations()
    }

    /**
     * Called when the Activity calls super.onResume()
     */
    override fun onActivityResumed(activity: Activity) {
        // Do nothing
    }

    /**
     * Called when the Activity calls super.onPause()
     */
    override fun onActivityPaused(activity: Activity) {
        // Do nothing
    }

    /**
     * Called when the Activity calls super.onStop()
     */
    override fun onActivityStopped(activity: Activity) {
        // Do nothing
    }

    /**
     * Called when the Activity calls super.onSaveInstanceState()
     */
    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
        // Do nothing
    }

    /**
     * Called when the Activity calls super.onDestroy()
     */
    override fun onActivityDestroyed(activity: Activity) {
        // Do nothing
    }
}
