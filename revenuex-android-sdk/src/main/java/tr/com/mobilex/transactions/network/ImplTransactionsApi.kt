package tr.com.mobilex.transactions.network

import com.google.gson.Gson
import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import tr.com.mobilex.core.utils.getConcatenatedUrl
import tr.com.mobilex.core.utils.getExceptionMessage

/**
 * Class that manages calls to transactions API
 */
internal class ImplTransactionsApi(
    override val systemInfo: SystemInfo,
    private val httpClient: HttpClient
) : TransactionsApi {

    /**
     * Constant params
     */
    private val apiUrl = systemInfo.rootUrl
        .getConcatenatedUrl(Constants.TRANSACTIONS_API_URL)

    /**
     * Base HTTP headers used in all requests
     */
    private val baseHeaders: Map<String, String> = mapOf(
        Constants.CLIENT_ID to systemInfo.clientId,
        Constants.USER_REVENUEX_ID to systemInfo.revenueXUserId,
        Constants.PLATFORM to systemInfo.platform,
        Constants.PLATFORM_VERSION to systemInfo.platformVersion,
        Constants.APPLICATION_VERSION to systemInfo.applicationVersion,
        Constants.DEVICE_NAME to systemInfo.deviceName,
        Constants.SANDBOX to "${systemInfo.isSandbox}",
        Constants.REGION to systemInfo.region,
        Constants.CONTENT_TYPE to Constants.JSON_CONTENT_TYPE
    )

    /**
     * Validates purchases in RevenueX
     * @param purchaseInfo Purchase information to validate
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when purchase is validated
     */
    override fun validate(
        purchaseInfo: PurchaseInformation,
        onFailure: (Exception) -> Unit,
        onResponse: (String?) -> Unit
    ) {
        // Prepares endpoint url
        val validateUrl = apiUrl.getConcatenatedUrl(Constants.TRANSACTIONS_API_VALIDATE)

        // Prepares jsonBody with purchase information parameter
        val body = Gson().toJson(purchaseInfo)

        // Makes HTTP call to RevenueX API
        httpClient.post(
            url = validateUrl,
            jsonBody = body,
            headers = baseHeaders,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("TransactionsApi validate server response: $response")
                onResponse.invoke(response)
            },
            classType = String::class.java
        )
    }
}
