package tr.com.mobilex.transactions.manager

import android.app.Activity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.billing.ProductType
import tr.com.mobilex.core.billing.PurchaseInformationProvider
import tr.com.mobilex.core.billing.RevXProduct
import tr.com.mobilex.core.billing.RevXPurchase
import tr.com.mobilex.core.billing.RevXPurchaseHistory
import tr.com.mobilex.core.billing.isSubscription
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.queue.ImplOperationQueue
import tr.com.mobilex.core.queue.OperationQueue
import tr.com.mobilex.core.sharedpref.EncryptedSharedPrefManager
import tr.com.mobilex.core.utils.add
import tr.com.mobilex.core.utils.contains
import tr.com.mobilex.core.utils.getExceptionMessage
import tr.com.mobilex.core.utils.toPrice
import tr.com.mobilex.core.utils.toRevXPeriod
import tr.com.mobilex.entitlements.manager.EntitlementsManager
import tr.com.mobilex.products.manager.ProductsManager
import tr.com.mobilex.transactions.network.ProductInformation
import tr.com.mobilex.transactions.network.PurchaseInformation
import tr.com.mobilex.transactions.network.TransactionsApi

/**
 * Class to manage logics about purchases
 * For example; Fetching purchase history from billing,
 * validating purchases in RevenueX, etc.
 */
internal class ImplTransactionsManager(
    private val transactionsApi: TransactionsApi,
    private val purchaseInfoProvider: PurchaseInformationProvider,
    private val productsManager: ProductsManager,
    private val entitlementsManager: EntitlementsManager,
    private val securePreferences: EncryptedSharedPrefManager,
    private val observerMode: Boolean
) : TransactionsManager {

    init {
        // Sets new purchase listener as itself
        purchaseInfoProvider.newPurchaseListener = this
    }

    /**
     * Operation queue for the not validated purchases
     * This queue will hold only one operation,
     * which will be executed with all not validated purchases
     */
    private val operationQueue: OperationQueue<(RevXPurchase) -> Unit, RevXPurchase> =
        ImplOperationQueue()

    /**
     * Purchase operation is completed in another function's context
     * Callback function is stored in this variable
     * and called when purchase operation is finished
     */
    private var purchaseCallback: ((RevXPurchase) -> Unit)? = null

    /**
     * Mutable live data that holds purchase history
     */
    private val _purchaseHistory: MutableLiveData<List<RevXPurchaseHistory>> = MutableLiveData()

    /**
     * Live data that holds purchase history
     */
    override val purchaseHistory: LiveData<List<RevXPurchaseHistory>> = _purchaseHistory

    /**
     * Mutable live data that holds active purchases
     */
    private val _purchases: MutableLiveData<List<RevXPurchase>> = MutableLiveData()

    /**
     * Live data that holds active purchases
     */
    override val purchases: LiveData<List<RevXPurchase>> = _purchases

    /**
     * Sends purchase history to RevenueX
     */
    override fun sendPurchaseHistory() {
        // Purchase history is queried for old subscriptions and inApp purchases
        purchaseInfoProvider
            .queryPurchaseHistory(ProductType.Subs) { subscriptions ->
                purchaseInfoProvider
                    .queryPurchaseHistory(ProductType.InApp) { inApps ->
                        val totalHistory = subscriptions + inApps
                        Logger.debug("Purchase history: $totalHistory")

                        // All history records are stored in live data and validated in RevenueX
                        _purchaseHistory.postValue(totalHistory)
                        validatePurchaseHistory(totalHistory)
                    }
            }
    }

    /**
     * Sends active purchases to RevenueX
     */
    override fun sendActivePurchases() {
        // Active purchases is queried for subscriptions and inApp purchases
        purchaseInfoProvider
            .queryPurchases(ProductType.Subs) { subscriptions ->
                purchaseInfoProvider
                    .queryPurchases(ProductType.InApp) { inApps ->
                        val totalPurchases = subscriptions + inApps
                        Logger.debug("Active purchases: $totalPurchases")

                        // All active purchases are stored in live data and validated in RevenueX
                        _purchases.postValue(totalPurchases)
                        validatePurchases(totalPurchases)
                    }
            }
    }

    /**
     * Purchases given product
     * @param activity Active activity to launch purchase flow from
     * @param product Product to be purchased
     * @param upgradedProduct Product that the subscription will be upgraded from
     * @param callback Callback function that will be triggered when product is purchased
     */
    override fun purchaseProduct(
        activity: Activity,
        product: RevXProduct,
        upgradedProduct: RevXProduct?,
        callback: ((RevXPurchase) -> Unit)?
    ) {
        // If SDK is not in observer mode, starts purchase flow with billing manager
        if (observerMode.not()) {
            // Callback is stored in a variable to call when purchase is completed
            purchaseCallback = callback
            purchaseInfoProvider.purchaseProduct(activity, product, upgradedProduct)
        } else {
            // This is not expected to be called in observer mode
            Logger.warning("Tried to launch purchase flow in observer mode!")
        }
    }

    /**
     * Called when a new purchase is done
     * @param purchase Information about latest purchase
     */
    override fun onNewPurchaseComplete(purchase: RevXPurchase) {
        // Validates purchase information in RevenueX
        validatePurchase(
            purchase.purchaseToken,
            purchase.type.isSubscription(),
            purchase.productIds
        ) { isSynced ->
            // If a purchase is not validated,
            // tries to add a pending operation
            if (isSynced.not()) addPendingOperation()

            // Adds new purchase to the active purchases
            if (_purchases.contains(purchase.purchaseToken).not()) {
                _purchases.add(purchase)
            }

            // Updating entitlements after a new purchase
            entitlementsManager.fetchEntitlements()
            purchaseCallback?.invoke(purchase)
        }
    }

    /**
     * Executes pending validation requests
     */
    override fun executePendingOperations() {
        // Gets not validated purchases
        val notValidatedPurchases = getNotValidatedPurchases()

        // This loop should be executed only once
        while (operationQueue.isEmpty().not()) {
            val operation = operationQueue.remove()

            // Uses same operation to validate all purchases
            notValidatedPurchases.forEach { purchase ->
                operation.invoke(purchase)
            }
        }
    }

    /**
     * Validates purchase history in RevenueX
     * @param purchaseHistory Purchase history to validate in RevenueX
     */
    private fun validatePurchaseHistory(purchaseHistory: List<RevXPurchaseHistory>) {
        for (history in purchaseHistory) {
            // Validates purchase history record in RevenueX
            validatePurchase(
                history.purchaseToken,
                history.type.isSubscription(),
                history.productIds,
                true
            )
        }
    }

    /**
     * Validates active purchases in RevenueX
     * @param purchases Active purchases to validate in RevenueX
     */
    private fun validatePurchases(purchases: List<RevXPurchase>) {
        for (purchase in purchases) {
            // Validates active purchases in RevenueX
            validatePurchase(
                purchase.purchaseToken,
                purchase.type.isSubscription(),
                purchase.productIds
            )
        }
    }

    /**
     * Validates purchase information in RevenueX
     * @param purchaseToken Purchase token of the purchase
     * @param isSubscription If purchase is subscription
     * @param productIds Products bought with given purchase
     * @param isHistoryRecord If purchase is a historical record
     * @param onComplete Callback function to call when validation is completed
     */
    @Synchronized
    private fun validatePurchase(
        purchaseToken: String,
        isSubscription: Boolean,
        productIds: List<String> = listOf(),
        isHistoryRecord: Boolean = false,
        onComplete: ((Boolean) -> Unit)? = null
    ) {
        // Gets already validated tokens from shared pref
        var syncedTokens = securePreferences
            .read(Constants.VALIDATED_PURCHASE_TOKENS, Array<String>::class.java) ?: arrayOf()

        // If purchase token is in validated tokens, don't send it to RevenueX
        if (syncedTokens.contains(purchaseToken)) {
            onComplete?.invoke(true)
            return
        }

        // Prepares data to call transaction api
        val purchaseInfo = getPurchaseInformation(
            purchaseToken,
            isSubscription,
            productIds,
            isHistoryRecord
        )

        Logger.debug("Purchase information to validate: $purchaseInfo")
        transactionsApi.validate(
            purchaseInfo,
            onFailure = {
                Logger.error(it.getExceptionMessage(), it)
                onComplete?.invoke(false)
            },
            onResponse = { response ->
                Logger.debug("validatePurchase response: $response")
                syncedTokens = syncedTokens.plus(purchaseToken)
                securePreferences.write(Constants.VALIDATED_PURCHASE_TOKENS, syncedTokens)
                onComplete?.invoke(true)
            }
        )
    }

    /**
     * Creates purchase information with given params
     * @param purchaseToken Purchase token of the purchase
     * @param isSubscription If purchase is subscription
     * @param productIds Products bought with given purchase
     * @param isHistoryRecord If purchase is a historical record
     */
    private fun getPurchaseInformation(
        purchaseToken: String,
        isSubscription: Boolean,
        productIds: List<String> = listOf(),
        isHistoryRecord: Boolean = false
    ): PurchaseInformation {
        val purchasedProducts = if (isHistoryRecord) {
            productIds.map { id -> ProductInformation(id) }
        } else {
            // If purchase is an active or new purchase, find product details
            val productList = productsManager.products
                .filter { productIds.contains(it.productId) }
            productList.map {
                ProductInformation(
                    it.productId,
                    it.subscriptionPeriod.toRevXPeriod(),
                    it.priceAmountMicros.toPrice(),
                    it.priceCurrencyCode
                )
            }
        }

        return PurchaseInformation(purchaseToken, isSubscription, purchasedProducts)
    }

    /**
     * If operation queue is empty,
     * adds a pending operation to the queue
     */
    private fun addPendingOperation() {
        // This single operation will be triggered for all not validated purchases
        if (operationQueue.isEmpty()) {
            operationQueue.add { p ->
                validatePurchase(
                    p.purchaseToken,
                    p.type.isSubscription(),
                    p.productIds
                ) { isSynced ->
                    // If a purchase is not validated,
                    // tries to add a pending operation
                    if (isSynced.not()) addPendingOperation()
                }
            }
        }
    }

    /**
     * Gets purchase records which are not validated in the server
     * @return Not validated purchase records
     */
    private fun getNotValidatedPurchases(): List<RevXPurchase> {
        return (_purchases.value ?: listOf()).filter {
            it.isAcknowledged.not()
        }
    }
}
