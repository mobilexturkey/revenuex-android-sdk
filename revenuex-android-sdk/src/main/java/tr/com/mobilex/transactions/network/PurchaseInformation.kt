package tr.com.mobilex.transactions.network

import java.io.Serializable

/**
 * Purchase information data model which will be passed to validation endpoint
 */
internal data class PurchaseInformation(
    val receiptData: String,
    val isSubscription: Boolean,
    val products: List<ProductInformation> = listOf()
) : Serializable

/**
 * Product information data model which will be passed to validation endpoint
 */
internal data class ProductInformation(
    val productId: String,
    val period: String? = null,
    val price: Double? = null,
    val currency: String? = null
) : Serializable
