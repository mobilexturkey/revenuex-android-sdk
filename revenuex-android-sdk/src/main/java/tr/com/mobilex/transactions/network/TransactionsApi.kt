package tr.com.mobilex.transactions.network

import tr.com.mobilex.core.network.Api

/**
 * Transactions API interface
 */
internal interface TransactionsApi : Api {

    /**
     * Validates purchases in RevenueX
     * @param purchaseInfo Purchase information to validate
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when purchase is validated
     */
    fun validate(
        purchaseInfo: PurchaseInformation,
        onFailure: (Exception) -> Unit,
        onResponse: (String?) -> Unit
    )
}
