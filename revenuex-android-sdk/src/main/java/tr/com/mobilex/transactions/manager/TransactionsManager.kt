package tr.com.mobilex.transactions.manager

import android.app.Activity
import androidx.lifecycle.LiveData
import tr.com.mobilex.core.billing.NewPurchaseListener
import tr.com.mobilex.core.billing.RevXProduct
import tr.com.mobilex.core.billing.RevXPurchase
import tr.com.mobilex.core.billing.RevXPurchaseHistory

/**
 * Class to manage logics about purchases
 * For example; Fetching purchase history from billing,
 * validating purchases in RevenueX, etc.
 */
internal interface TransactionsManager : NewPurchaseListener {

    /**
     * Live data that holds purchase history
     */
    val purchaseHistory: LiveData<List<RevXPurchaseHistory>>

    /**
     * Live data that holds active purchases
     */
    val purchases: LiveData<List<RevXPurchase>>

    /**
     * Sends purchase history to RevenueX
     */
    fun sendPurchaseHistory()

    /**
     * Sends active purchases to RevenueX
     */
    fun sendActivePurchases()

    /**
     * Purchases given product
     * @param activity Active activity to launch purchase flow from
     * @param product Product to be purchased
     * @param upgradedProduct Product that the subscription will be upgraded from
     * @param callback Callback function that will be triggered when product is purchased
     */
    fun purchaseProduct(
        activity: Activity,
        product: RevXProduct,
        upgradedProduct: RevXProduct? = null,
        callback: ((RevXPurchase) -> Unit)? = null
    )

    /**
     * Executes pending validation requests
     */
    fun executePendingOperations()
}
