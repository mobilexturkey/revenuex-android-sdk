package tr.com.mobilex.config.network

import tr.com.mobilex.core.Constants
import tr.com.mobilex.core.SystemInfo
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.network.HttpClient
import tr.com.mobilex.core.utils.getConcatenatedUrl
import tr.com.mobilex.core.utils.getExceptionMessage
import tr.com.mobilex.core.utils.typeToken

/**
 * Class that manages calls to config API
 */
internal class ImplRemoteConfigApi(
    override val systemInfo: SystemInfo,
    private val httpClient: HttpClient
) : RemoteConfigApi {

    /**
     * Constant params
     */
    private val apiUrl = systemInfo.rootUrl
        .getConcatenatedUrl(Constants.CONFIG_API_URL)
    private val baseHeaders: Map<String, String> = mapOf(
        Constants.CLIENT_ID to systemInfo.clientId
    )

    /**
     * Gets remote configs from RevenueX
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when remote configs retrieved
     */
    override fun getRemoteConfigs(
        onFailure: (Exception) -> Unit,
        onResponse: (Map<String, Any?>?) -> Unit
    ) {
        // Prepares endpoint url and headers for the request
        val remoteConfigUrl = apiUrl.getConcatenatedUrl(Constants.CONFIG_API_REMOTE_CONFIG)
        val headers = baseHeaders.toMutableMap().also { newMap ->
            newMap[Constants.USER_REVENUEX_ID] = systemInfo.revenueXUserId
            newMap[Constants.SANDBOX] = "${systemInfo.isSandbox}"
        }

        // Makes HTTP call to RevenueX API
        httpClient.get(
            url = remoteConfigUrl,
            headers = headers,
            onFailure = { exception ->
                Logger.error(exception.getExceptionMessage(), exception)
                onFailure.invoke(exception)
            },
            onResponse = { response ->
                Logger.debug("RemoteConfigApi getRemoteConfigs server response: $response")
                onResponse.invoke(response)
            },
            typeToken = typeToken<HashMap<String, Any?>>()
        )
    }
}
