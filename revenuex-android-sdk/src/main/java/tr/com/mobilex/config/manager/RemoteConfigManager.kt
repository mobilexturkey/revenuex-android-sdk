package tr.com.mobilex.config.manager

import androidx.annotation.XmlRes

/**
 * Class to manage logics about remote configs
 */
internal interface RemoteConfigManager {

    /**
     * Map that holds remote configs
     */
    val remoteConfigs: Map<String, Any?>

    /**
     * Fetches remote configs from RevenueX
     * @param onComplete Called when remote configs fetched
     */
    fun fetchRemoteConfigs(onComplete: (() -> Unit)? = null)

    /**
     * Sets default values from XML resource file
     * @param resourceId Id of the XML file
     */
    fun setConfigDefaultsAsync(@XmlRes resourceId: Int)

    /**
     * Gets value of the remote config with given key
     * @param key Key of the config
     * @param defaultValue Default value that will be returned if remote config is missing
     *
     * @return Value of the remote config
     */
    @Deprecated(
        "This function is used for internal logics.",
        ReplaceWith("getRemoteConfig(key: String, defaultValue: T?)"),
        DeprecationLevel.WARNING
    )
    fun get(
        key: String,
        defaultValue: Any? = null
    ): Any?
}

/**
 * Gets value of the remote config with given key
 * @param key Key of the config
 * @param defaultValue Default value that will be returned if remote config is missing
 *
 * @return Value of the remote config
 */
@Suppress("deprecation")
internal inline fun <reified T> RemoteConfigManager.getRemoteConfig(
    key: String,
    defaultValue: T? = null
): T? {
    val value = get(key, defaultValue)
    return if (value != null && value is T) value else defaultValue
}
