package tr.com.mobilex.config.manager

import android.content.Context
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.xmlpull.v1.XmlPullParser
import tr.com.mobilex.config.network.RemoteConfigApi
import tr.com.mobilex.core.coroutine.DispatcherProvider
import tr.com.mobilex.core.logging.Logger
import tr.com.mobilex.core.utils.getExceptionMessage

/**
 * Class to manage logics about remote configs
 */
internal class ImplRemoteConfigManager(
    private val remoteConfigApi: RemoteConfigApi,
    private val context: Context
) : RemoteConfigManager {

    /**
     * Mutable map that holds remote configs
     */
    private var _remoteConfigs: MutableMap<String, Any?>? = null

    /**
     * Map that holds remote configs
     */
    override val remoteConfigs: Map<String, Any?>
        get() = _remoteConfigs ?: mapOf()

    companion object {
        // Constants for XML property names
        private const val XML_KEY_ENTRY = "entry"
        private const val XML_KEY_KEY = "key"
        private const val XML_KEY_VALUE = "value"
    }

    /**
     * Fetches remote configs from RevenueX
     * @param onComplete Called when remote configs fetched
     */
    override fun fetchRemoteConfigs(onComplete: (() -> Unit)?) {
        remoteConfigApi.getRemoteConfigs({
            Logger.error(it.getExceptionMessage(), it)
            _remoteConfigs = mutableMapOf()
            onComplete?.invoke()
        }, { response ->
            Logger.debug("fetchRemoteConfigs response: $response")
            _remoteConfigs = response?.toMutableMap() ?: mutableMapOf()
            onComplete?.invoke()
        })
    }

    /**
     * Sets default values from XML resource file
     * @param resourceId Id of the XML file
     */
    override fun setConfigDefaultsAsync(resourceId: Int) {
        CoroutineScope(DispatcherProvider.IO).launch {
            try {
                val tempConfigs = mutableMapOf<String, Any?>()
                val defaultValues = parseXmlFile(resourceId)

                defaultValues.forEach { entry ->
                    tempConfigs[entry.key] = entry.value
                }
                _remoteConfigs = tempConfigs
            } catch (exception: Exception) {
                Logger.error("Could not set default config values", exception)
            }
        }
    }

    /**
     * Gets value of the remote config with given key
     * @param key Key of the config
     * @param defaultValue Default value that will be returned if remote config is missing
     *
     * @return Value of the remote config
     */
    @Suppress("UNCHECKED_CAST", "OverridingDeprecatedMember")
    override fun get(
        key: String,
        defaultValue: Any?
    ): Any? {
        return try {
            // Trying to cast config value to T
            (_remoteConfigs?.get(key) ?: defaultValue)
        } catch (exception: ClassCastException) {
            Logger.error(exception.getExceptionMessage(), exception)
            defaultValue
        }
    }

    /**
     * Parses XML resource file and reads default values for remote configs
     * @param resourceId Id of the XML file
     *
     * @return Map that holds default values
     */
    private fun parseXmlFile(resourceId: Int): Map<String, Any?> {
        val configs = mutableMapOf<String, Any?>()
        lateinit var key: String
        lateinit var value: String

        val parser = context.resources.getXml(resourceId)
        // Loop while not at the end of the file
        while (parser.eventType != XmlPullParser.END_DOCUMENT) {
            if (parser.eventType == XmlPullParser.START_TAG) {
                when (parser.name) {
                    XML_KEY_ENTRY -> {
                        // When a new entry starts, reset key and value
                        key = ""
                        value = ""
                    }
                    XML_KEY_KEY -> {
                        // When key tag is found, read key from xml
                        key = parser.nextText()
                    }
                    XML_KEY_VALUE -> {
                        // When value tag is found, read value from xml
                        value = parser.nextText()
                    }
                }
            } else if (parser.eventType == XmlPullParser.END_TAG) {
                if (parser.name == XML_KEY_ENTRY) {
                    // When entry tag is closed, add key and value to map
                    val newEntry = getMapEntry(key, value)
                    configs[newEntry.first] = newEntry.second
                }
            }

            // Load next element
            parser.next()
        }

        return configs
    }

    /**
     * Casts value to appropriate type and returns key - value to Pair
     * @param key Key of the config
     * @param value Value of the config
     *
     * @return Key - value pair
     */
    private fun getMapEntry(key: String, value: String): Pair<String, Any?> {
        val convertedValue: Any? = when {
            value.toBooleanStrictOrNull() != null -> {
                value.toBooleanStrictOrNull()
            }
            value.toDoubleOrNull() != null -> {
                value.toDoubleOrNull()
            }
            else -> {
                value
            }
        }

        return Pair(key, convertedValue)
    }
}
