package tr.com.mobilex.config.network

import tr.com.mobilex.core.network.Api

/**
 * Config API interface
 */
internal interface RemoteConfigApi : Api {

    /**
     * Gets remote configs from RevenueX
     * @param onFailure Callback function that will be triggered when an error occurs
     * @param onResponse Callback function that will be triggered when remote configs retrieved
     */
    fun getRemoteConfigs(
        onFailure: (Exception) -> Unit,
        onResponse: (Map<String, Any?>?) -> Unit
    )
}
