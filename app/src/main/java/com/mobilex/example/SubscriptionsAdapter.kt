package com.mobilex.example

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobilex.example.databinding.SubscriptionRowBinding
import tr.com.mobilex.core.billing.RevXProduct

/**
 * RecyclerView adapter to show available subscription products
 * @param subscriptions List of available subscriptions
 * @param purchaseRequestListener Purchase request listener
 *          which triggers when clicked on product price
 */
class SubscriptionsAdapter(
    private val subscriptions: List<RevXProduct>,
    private val purchaseRequestListener: (RevXProduct) -> Unit
) : RecyclerView.Adapter<SubscriptionVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubscriptionVH {
        // Creates view binding for view holder
        val itemBinding = SubscriptionRowBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return SubscriptionVH(itemBinding)
    }

    override fun onBindViewHolder(holder: SubscriptionVH, position: Int) {
        // Finds current product and binds to view holder
        val subscription: RevXProduct = subscriptions[position]
        holder.onBind(subscription) {
            purchaseRequestListener.invoke(it)
        }
    }

    override fun getItemCount(): Int {
        return subscriptions.count()
    }
}

/**
 * View holder model for subscription item
 * @param binding View binding model for row
 */
class SubscriptionVH(
    private val binding: SubscriptionRowBinding
) : RecyclerView.ViewHolder(binding.root) {

    /**
     * Binds data with view
     */
    fun onBind(subscription: RevXProduct, onButtonClick: (RevXProduct) -> Unit) {
        binding.subscriptionTitle.text = subscription.title
        binding.subscriptionTrialPeriod.text = subscription.freeTrialPeriod.toTrialPeriod()
        binding.subscriptionPeriod.text = subscription.subscriptionPeriod.toPeriod()
        binding.subscriptionPrice.text = subscription.price
        binding.subscriptionPrice.setOnClickListener {
            onButtonClick.invoke(subscription)
        }
    }
}

/**
 * Prepares label for trial period
 */
private fun String.toTrialPeriod(): String {
    return if (this.isBlank()) {
        "No trial period"
    } else {
        "Trial period: $this"
    }
}

/**
 * Prepares label for subscription period
 */
private fun String.toPeriod(): String {
    return if (this.isBlank()) {
        "No subscription period"
    } else {
        "Subscription period: $this"
    }
}
