package com.mobilex.example

import android.util.Log
import androidx.multidex.MultiDexApplication
import tr.com.mobilex.Revenuex
import tr.com.mobilex.applicationuser.network.UserAttributes

/**
 * Test application class to test configurations of RevenueX SDK
 */
class ExampleApplication : MultiDexApplication() {

    companion object {
        private const val TAG = "RevenueXApp"
    }

    override fun onCreate() {
        super.onCreate()

        // Init library
        val clientId = "BFEDF734E4BE9F702D7A6EEA"
        val userId = "0d3b8fc66eeb45728f2a281fddb0ec99"
        Revenuex.init(applicationContext, clientId, userId)
        Revenuex.setAttributes(mapOf(UserAttributes.Email to "revenuex@mobilex.com.tr"))
        Revenuex.setConfigDefaultsAsync(R.xml.revenuex_remote_config_defaults)

        getRemoteConfigValues()
    }

    private fun getRemoteConfigValues() {
        val appName = Revenuex.getStringConfig("app_name")
        val version = Revenuex.getDoubleConfig("version")
        Log.w(TAG, "Remote config value is app_name : $appName - version : $version")
    }
}
