package com.mobilex.example

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageButton
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textview.MaterialTextView
import com.mobilex.example.databinding.ActivityPaywallBinding
import tr.com.mobilex.Revenuex
import tr.com.mobilex.core.billing.RevXPackage
import tr.com.mobilex.core.billing.RevXProduct

class PaywallActivity : AppCompatActivity() {

    /**
     * View binding adapter for paywall activity
     */
    private lateinit var binding: ActivityPaywallBinding

    /**
     * RecyclerView adapter for subscriptions
     */
    private lateinit var adapter: SubscriptionsAdapter

    /**
     * View model for paywall activity
     */
    private val viewModel: PaywallViewModel by lazy {
        ViewModelProvider(this)[PaywallViewModel::class.java]
    }

    companion object {
        private const val TAG = "RevenueXApp"
        private const val FIRST_PRODUCT = 0
        private const val SECOND_PRODUCT = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Creates view binding to access layout items
        binding = ActivityPaywallBinding.inflate(layoutInflater)
        setContentView(binding.root)

        observeInAppProducts()
        observeSubscriptions()
        observeOfferingProducts()
        setClickListeners()
    }

    /**
     * Sets observer for in-app purchase products
     */
    private fun observeInAppProducts() {
        viewModel.inAppProducts.observe(this) { inApps ->
            // When inApp products updated,
            // updates top ui elements with new inApp products
            Log.i(TAG, "In app products observed. Count: ${inApps.count()}")
            updateInAppButton(
                binding.firstInappName,
                binding.firstInappPrice,
                FIRST_PRODUCT,
                inApps
            )
            updateInAppButton(
                binding.secondInappName,
                binding.secondInappPrice,
                SECOND_PRODUCT,
                inApps
            )
        }
    }

    /**
     * Updates UI elements with new inApp products
     */
    private fun updateInAppButton(
        name: MaterialTextView,
        price: MaterialTextView,
        index: Int,
        inApps: List<RevXProduct>
    ) {
        if (inApps.count() > index) {
            name.text = inApps[index].title
            price.text = inApps[index].price
        } else {
            name.text = ""
            price.text = getString(R.string.no_product)
        }
    }

    /**
     * Sets observer for subscription products
     */
    private fun observeSubscriptions() {
        viewModel.subscriptions.observe(this) { subs ->
            Log.i(TAG, "Subscriptions observed. Count: ${subs.count()}")

            setupAdapter(subs)
        }
    }

    /**
     * Creates new adapter and sets on RecyclerView
     */
    private fun setupAdapter(subscriptions: List<RevXProduct>) {
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        val dividerItemDecoration = DividerItemDecoration(
            binding.subscriptionsRecycler.context,
            layoutManager.orientation
        )
        adapter = SubscriptionsAdapter(subscriptions, newPurchaseListener)

        binding.subscriptionsRecycler.layoutManager = layoutManager
        binding.subscriptionsRecycler.addItemDecoration(dividerItemDecoration)
        binding.subscriptionsRecycler.adapter = adapter
    }

    /**
     * Sets observer for offerings
     */
    private fun observeOfferingProducts() {
        viewModel.offering.observe(this) { offering ->
            // When offerings updated,
            // updates bottom ui elements with new offering information
            Log.i(TAG, "Offering observed. Package count: ${offering.packages.count()}")
            updateOfferingButton(
                binding.firstOfferingPrice,
                binding.firstOfferingPeriod,
                FIRST_PRODUCT,
                offering.packages
            )
            updateOfferingButton(
                binding.secondOfferingPrice,
                binding.secondOfferingPeriod,
                SECOND_PRODUCT,
                offering.packages
            )
        }
    }

    /**
     * Updates UI elements with new offerings
     */
    private fun updateOfferingButton(
        price: MaterialTextView,
        period: MaterialTextView,
        index: Int,
        packages: List<RevXPackage>
    ) {
        if (packages.count() > index) {
            price.text = packages[index].product?.price
            period.text = packages[index].product?.subscriptionPeriod
        } else {
            price.text = getString(R.string.no_offering)
            period.text = ""
        }
    }

    /**
     * Sets on click listeners for inApps and offerings
     */
    private fun setClickListeners() {
        setInAppClickListener(binding.firstInappProduct, FIRST_PRODUCT)
        setInAppClickListener(binding.secondInappProduct, SECOND_PRODUCT)
        setOfferingClickListener(binding.firstOfferingProduct, FIRST_PRODUCT)
        setOfferingClickListener(binding.secondOfferingProduct, SECOND_PRODUCT)
    }

    /**
     * Sets on click listeners for inApps
     */
    private fun setInAppClickListener(button: AppCompatImageButton, index: Int) {
        button.setOnClickListener {
            // If clicked item index is found in live data,
            // starts purchase flow for selected product
            viewModel.inAppProducts.value?.let { inApps ->
                if (inApps.count() > index) {
                    Revenuex.purchaseProduct(this, inApps[index])
                    return@setOnClickListener
                }
            }

            // If products contains requested inApp, this code will not execute
            Log.w(TAG, "There is no product to purchase")
        }
    }

    /**
     * Sets on click listeners for offerings
     */
    private fun setOfferingClickListener(button: AppCompatImageButton, index: Int) {
        button.setOnClickListener {
            // If clicked item index is found in live data,
            // starts purchase flow for selected package
            viewModel.offering.value?.let { offering ->
                if (offering.packages.count() > index) {
                    val product = offering.packages[index].product ?: return@let
                    Revenuex.purchaseProduct(this, product)
                    return@setOnClickListener
                }
            }

            // If offering contains requested package, this code will not execute
            Log.w(TAG, "There is no product to purchase")
        }
    }

    /**
     * Starts purchase flow for clicked offering
     */
    private val newPurchaseListener: (RevXProduct) -> Unit = {
        Revenuex.purchaseProduct(this, it)
    }
}
