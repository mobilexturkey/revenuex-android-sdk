package com.mobilex.example

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import tr.com.mobilex.Revenuex
import tr.com.mobilex.core.billing.ProductType
import tr.com.mobilex.core.billing.RevXOffering
import tr.com.mobilex.core.billing.RevXProduct

/**
 * View model to manage business logics in paywall activity
 */
class PaywallViewModel : ViewModel() {

    companion object {
        private const val TAG = "RevenueXApp"
    }

    /**
     * LiveData property to hold in-app purchase products
     */
    private val _inAppProducts: MutableLiveData<List<RevXProduct>> by lazy {
        getProducts(ProductType.InApp)
        MutableLiveData()
    }
    val inAppProducts: LiveData<List<RevXProduct>> = _inAppProducts

    /**
     * LiveData property to hold subscriptions
     */
    private val _subscriptions: MutableLiveData<List<RevXProduct>> by lazy {
        getProducts(ProductType.Subs)
        MutableLiveData()
    }
    val subscriptions: LiveData<List<RevXProduct>> = _subscriptions

    /**
     * LiveData property to hold current offering
     */
    private val _offering: MutableLiveData<RevXOffering> by lazy {
        getOffering()
        MutableLiveData()
    }
    val offering: LiveData<RevXOffering> = _offering

    /**
     * Gets products with given type from RevenueX SDK
     * @param type Type of the products that requested
     */
    private fun getProducts(type: ProductType) {
        Log.i(TAG, "Getting products. Type: $type")
        Revenuex.getProducts(type) { products ->
            Log.i(TAG, "Products fetched. Count: ${products.count()}")
            when (type) {
                ProductType.InApp -> _inAppProducts.postValue(products)
                ProductType.Subs -> _subscriptions.postValue(products)
            }
        }
    }

    /**
     * Gets current offering from RevenueX SDK
     */
    private fun getOffering() {
        Revenuex.getCurrentOffering { currentOffering ->
            currentOffering?.let {
                _offering.postValue(it)
            }
        }
    }
}
