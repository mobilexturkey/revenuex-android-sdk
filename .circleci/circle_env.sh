#!/usr/bin/env bash

function copyEnvVarsToGradleProperties {
    DIR="$(pwd)"
    LOCAL_PROPERTIES=${DIR}"/local.properties"
    export LOCAL_PROPERTIES
    echo "Keystore Properties should exist at $LOCAL_PROPERTIES"

    if [[ -f "$LOCAL_PROPERTIES" ]]; then
      echo "Local properties is exist"
      echo "Writing keys to local.properties..."
      echo "ossrhUsername=$OSSRH_USERNAME" >> ${LOCAL_PROPERTIES}
      echo "ossrhPassword=$OSSRH_PASSWORD" >> ${LOCAL_PROPERTIES}
      echo "sonatypeStagingProfileId=$SONATYPE_STAGING_PROFILE_ID" >> ${LOCAL_PROPERTIES}
      echo "signing.keyId=$SIGNING_KEY_ID" >> ${LOCAL_PROPERTIES}
      echo "signing.password=$SIGNING_PASSWORD" >> ${LOCAL_PROPERTIES}
      echo "signing.secretKeyRingFile=$DIR/gpg-keys/$SIGNING_SECRET_KEY_RING_FILE" >> ${LOCAL_PROPERTIES}
    fi


    if [[ ! -f "$KEYSTORE_PROPERTIES" ]]; then
        echo "Local properties is exist"
      echo "Writing keys to local.properties..."
      echo "ossrhUsername=$OSSRH_USERNAME" >> ${LOCAL_PROPERTIES}
      echo "ossrhPassword=$OSSRH_PASSWORD" >> ${LOCAL_PROPERTIES}
      echo "sonatypeStagingProfileId=$SONATYPE_STAGING_PROFILE_ID" >> ${LOCAL_PROPERTIES}
      echo "signing.keyId=$SIGNING_KEY_ID" >> ${LOCAL_PROPERTIES}
      echo "signing.password=$SIGNING_PASSWORD" >> ${LOCAL_PROPERTIES}
      echo "signing.secretKeyRingFile=$DIR/gpg-keys/$SIGNING_SECRET_KEY_RING_FILE" >> ${LOCAL_PROPERTIES}
    fi
}

function fetchKeystore {
    DIR="$(pwd)"
    echo "Base Path is $DIR"
    sudo gpg --passphrase ${KEYSTORE_ENCRYPTION_KEY} --pinentry-mode loopback -o "$DIR/$KEYSTORE_FILE" -d "$DIR/$KEYSTORE_FILE.gpg"
}

function fetchLibGPGKey {
    DIR="$(pwd)"
    LIB_DIR=${DIR}"/revenuex-android-sdk/"
    echo "Base path is $LIB_DIR"
    sudo bash -c "echo '$GPG_KEY_CONTENTS' | base64 -d > '${LIB_DIR}/revenuex-android.gpg'"
}